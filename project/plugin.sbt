
addSbtPlugin ("org.scalariform" % "sbt-scalariform" % "1.6.0")

addSbtPlugin ("org.bitbucket.inkytonik.sbt-rats" % "sbt-rats" % "2.3.0")

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.0")

addSbtPlugin("de.heikoseeberger" % "sbt-header" % "1.7.0")
