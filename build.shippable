name := "Scala SMTLIB2 MQ"

organization := "au.edu.mq.comp"

version := "1.0-SNAPSHOT"

// Scala compiler settings

scalaVersion := "2.11.8"

crossScalaVersions := Seq("2.10.4", "2.11.2", "2.11.3", "2.11.4", "2.11.5", "2.11.6")

scalacOptions :=
    Seq (
        "-deprecation",
        "-feature",
        "-sourcepath", baseDirectory.value.getAbsolutePath,
        "-unchecked",
        "-Xlint",
        "-Xcheckinit"
    )

javaOptions in IntegrationTest ++= Seq("-Xss200M")

// test options for generating formatted test results (and use with CI)
testOptions in Test <+= (target in Test) map {
  t => Tests.Argument(TestFrameworks.ScalaTest, "-u", "%s" format (t / "../shippable/testresults"))
}

fork in IntegrationTest := true

libraryDependencies ++=
    Seq (
        "org.bitbucket.inkytonik.kiama" %% "kiama" % "2.0.0",
        "org.bitbucket.inkytonik.kiama" %% "kiama" % "2.0.0" % "test" classifier ("tests"),
        "org.scalatest" %% "scalatest" % "2.2.6" % "test",
        "org.scalacheck" %% "scalacheck" % "1.13.0" % "test",
        "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
        "com.jsuereth" %% "scala-arm" % "1.4",
        "ch.qos.logback" % "logback-classic" % "1.1.6"
    )


//  scoverage
coverageMinimum := 80
coverageFailOnMinimum := false  //  false is safer as otherwise the build breaks
coverageHighlighting := true    //  enable highlighting of covered/non-covered
//  exclude some package from coverage
coverageExcludedPackages := ".*sbtrats.*"

logBuffered in IntegrationTest := false

parallelExecution in Test := true

logLevel := Level.Info

shellPrompt <<= (name, version) { (n, v) =>
     _ => n + " " + v + "> "
}

scalacOptions in (Compile,doc) ++= Seq(
    "-groups",
    "-implicits",
    "-diagrams",
    "-diagrams-dot-restart", "80",
    "-diagrams-dot-path", "/usr/local/bin/dot"
    // ,"-diagrams-debug"
)

// Dependencies

// Rats! setup

sbtRatsSettings

ratsScalaRepetitionType := Some (ListType)

ratsUseScalaOptions := true

ratsUseScalaPositions := true

ratsDefineASTClasses := true

ratsDefinePrettyPrinter := true

// ratsUseDefaultLayout := true

ratsUseDefaultComments := false

// ratsVerboseOutput := true

ratsUseKiama := 2


// ScalariForm

import scalariform.formatter.preferences._
import com.typesafe.sbt.SbtScalariform
import com.typesafe.sbt.SbtScalariform.ScalariformKeys

scalariformSettings
//scalariformSettingsWithIt
//
ScalariformKeys.preferences := ScalariformKeys.preferences.value
    .setPreference (AlignSingleLineCaseStatements, true)
    .setPreference (IndentSpaces, 4)
    .setPreference (SpaceBeforeColon, true)
    .setPreference (SpaceInsideBrackets, true)
    .setPreference (SpaceInsideParentheses, true)
    .setPreference (SpacesWithinPatternBinders, true)
    .setPreference(SpacesAroundMultiImports, true)
    .setPreference (SpacesAroundMultiImports, true)
    .setPreference (PreserveSpaceBeforeArguments, true)
    .setPreference (SpacesWithinPatternBinders, true)
    .setPreference (RewriteArrowSymbols, true)
   .setPreference (AlignParameters, true)
   .setPreference(AlignArguments, true)
   // .setPreference(doubleIndentMethodDeclaration, true)
