#!/bin/bash

# echo "starting test terminal :-)"

#   the test terminal simulates the behaviour of an SMT solver.
#   it reads a line on its input stream composed of:
#   a list of pairs r!::d1 r2::d2 ... rk::dk
#   and output r1.r2...rk followed by the prompt.
#
#   prompt should be given on the command line as the first argument
#   if one of the ri is (exit) the terminal exits
#   the ouput is produdec with a delay of ri before producing ri

#   usage is:
#       test-terminal.sh "<EOC>"
#   and then enter re:1 ad::2 -> produces read (after 3 time units)
#   followed by the prompt
#   to stop the terminal, use (exit)::n in one of the pairs


if [ "$#" -ne 1 ];
    then
    echo "illegal number of parameters: need to specify the prompt"
    exit
fi

#   get the prompt
prompt=$1

while read a; do
    for i in $a; do
        # check is $i has a :: and if yes split
        if [[ $i == *"::"* ]]
            then
                # split with awk to get cmd/duration
                resp=`echo $i | awk -F '::' '{print $1}'`
                dur=`echo $i | awk -F '::' '{print $2}'`

                if [ "$resp" = "(exit)" ]
                then
                    exit 0
                else
                    sleep $dur
                    printf  "$resp\\n"
                fi
        fi
        # otherwise read the last two fields
    done
    #    this is the expected result
    printf  "$prompt\\n\\t\\n"
done
exit 1
