#!/bin/bash
#   this terminal when sent w::k on its input,
#   this terminal outputs w on the output after k seconds

while read a; do
    for i in $a; do
        # check is $i has a :: and if yes split
        if [[ $i == *"::"* ]]
            then
                # split with awk to get cmd/duration
                resp=`echo $i | awk -F '::' '{print $1}'`
                dur=`echo $i | awk -F '::' '{print $2}'`

                if [ "$resp" = "(exit)" ]
                then
                    exit
                else
                    sleep $dur
                    # printf "$resp\\n\\t" >log.txt
                    printf  "\\t\\n$resp\\n\\t"
                fi
        fi
        # otherwise read the last two fields
    done
done
exit
