/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package documentation
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import au.edu.mq.comp.smtlib.theories.{ Core, IntegerArithmetics, RealArithmetics }
import au.edu.mq.comp.smtlib.typedterms.Commands
import au.edu.mq.comp.smtlib.interpreters.Resources
import au.edu.mq.comp.smtlib.solvers._
import au.edu.mq.comp.smtlib._
import scala.util.{ Try, Success, Failure }
import parser.SMTLIB2Syntax._
import parser.SMTLIB2Parser.ParseErrorException
import theories.PredefinedLogics._
import configurations.Configurations._

/**
 * Propositional logic simple examples
 */
class PropLogic
        extends FunSuite
        with Matchers
        with TableDrivenPropertyChecks
        with Core
        with Commands
        with Resources {

    override def suiteName =
        """| ===============================================================
           | Examples from http://rise4fun.com/Z3/tutorial/guide
           | Propositional logic example -- QF_UF
           |    (declare-const p Bool)
           |    (declare-const q Bool)
           |    (declare-const r Bool)
           |    (define-fun conjecture () Bool
           |           (=>
           |                (and (=> p q) (=> q r))
           |                (=> p r)
           |            )
           |    )
           |    (assert (not conjecture))
           |    (check-sat)
           |    )
           |//  should be unsat""".stripMargin

    //  format: OFF
    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3          with QF_UF,
        new SMTInterpol with QF_UF,
        new CVC4        with QF_UF,
        new MathSAT     with QF_UF,
        new Yices       with QF_UF
    )
    //  format: ON

    for ( s ← theSolvers ) {
        test( s"Checking with [$s] -- unsat" ) {

            //  define 3 propositional variables, p, q, r
            val p = Bools( "p" )
            val q = Bools( "q" )
            val r = Bools( "r" )

            //  define a conjecture
            val c1 = ( p imply q ) & ( q imply r )
            val c2 = ( p imply r )

            val response = using( s ) {
                implicit withSolver ⇒
                    // check that not(c1 => c2) is Sat
                    isSat ( !( c1 imply c2 ) )
            }
            response shouldBe Success( UnSat() )
        }
    }
}

/**
 * More propositional logic
 */
class DeMorgan extends FunSuite
        with Matchers
        with TableDrivenPropertyChecks with Core
        with Commands
        with Resources {

    override def suiteName =
        """| ===============================================================
           | Examples from http://rise4fun.com/Z3/tutorial/guide
           | deMorgan's law -- QF_UF
           |    (declare-const a Bool)
           |    (declare-const b Bool)
           |    (declare-const r Bool)
           |    (define-fun demorgan () Bool
           |        (=
           |            (and a b)
           |            (not (or (not a) (not b)))
           |        )
           |    )
           |    (assert (not demorgan))
           |    (check-sat)
           |    )
           |//  should be unsat""".stripMargin

    import theories.PredefinedLogics.QF_UF

    //  format: OFF
    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3          with QF_UF,
        new SMTInterpol with QF_UF,
        new CVC4        with QF_UF,
        new MathSAT     with QF_UF,
        new Yices       with QF_UF
    )
    //  format: ON

    for ( s ← theSolvers ) {
        test( s"Checking with [$s] -- unsat" ) {

            //  define 2 propositional variables a and b
            val a = Bools( "a" )
            val b = Bools( "b" )

            //  define a conjecture
            val dml = ( a & b ) === !( !a | !b )

            val response = using( s ) {
                implicit withSolver ⇒
                    isSat ( !dml )
            }
            response shouldBe Success( UnSat() )
        }
    }
}

/**
 * Linear Arithmetics (integer and real)
 */
class Arithmetic1 extends FunSuite
        with Matchers
        with TableDrivenPropertyChecks
        with Core
        with IntegerArithmetics
        with RealArithmetics
        with Commands
        with Resources {

    override def suiteName =
        """| ===============================================================
           | Examples from http://rise4fun.com/Z3/tutorial/guide
           | Arithmetic -- QF_LRA
           |    (declare-const a Int)
           |    (declare-const b Int)
           |    (declare-const c Int)
           |    (declare-const d Real)
           |    (declare-const e Real)
           |
           |    (assert (> a (+ b 2)))
           |    (assert (= a (+ (* 2 c) 10)))
           |    (assert (<= (+ c b) 1000))
           |    (assert (>= d e))
           |
           |    (check-sat)
           |    (get-model)
           |    )
           |//  should be sat""".stripMargin

    //  format: OFF
    val theSolvers = Table[ Solver, Try[ SatResponses ] ](
        ( "Solver"                                   , "Expected Response" )                           ,
        ( new Z3 with AUFNIRA with Models            , Success( Sat() ) )                              ,
        ( new SMTInterpol with QF_UFLIRA with Models , Success( Sat() ) )                              ,
        ( new Yices with QF_LIRA with Models , Success( Sat() ) )                              ,
        ( new CVC4 with AUFNIRA with Models          , Failure( ParseErrorException( "..." ) ) ) ,
        ( new MathSAT with AUFNIRA with Models       , Success( Sat() ) )
    )
    //  format: ON

    for ( ( s, r ) ← theSolvers ) {
        test( s"Checking with [$s] -- should be $r" ) {

            //  define 3 propositional variables, p, q, r
            val a = Ints( "a" )
            val b = Ints( "b" )
            val c = Ints( "c" )
            val d = Reals( "d" )
            val e = Reals( "e" )

            val response = using( s ) {
                implicit withSolver ⇒
                    //  assert
                    |= ( a > b + 2 ) flatMap
                        { _ ⇒ |= ( a === ( Ints( 2 ) * c ) + 10 ) } flatMap
                        { _ ⇒ |= ( c + b <= 1000 ) } flatMap
                        { _ ⇒ |= ( d >= e ) } flatMap
                        { _ ⇒ checkSat() }
            }
            response match {
                case Success( x ) if ( r.isSuccess && r.get == x )      ⇒ assert( true )
                case Failure( ParseErrorException( _ ) ) if r.isFailure ⇒
            }
        }
    }
}

/**
 * Non-linear Arithmetics (integer and real) -- checksat and getmodel, getvalue
 */
class NonLinearArithmetic1 extends FunSuite
        with Matchers
        with TableDrivenPropertyChecks with Core
        with IntegerArithmetics
        with RealArithmetics
        with Commands
        with Resources {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    override def suiteName =
        """| ===============================================================
           | Examples from http://rise4fun.com/Z3/tutorial/guide
           | Non Linear Arithmetic -- AUFNIRA
           |    (declare-const a Int)
           |    (assert (> (* a a) 3))
           |
           |    (check-sat)
           |    (get-model)
           |    )
           |//  should be sat""".stripMargin

    //  format: OFF
    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with AUFNIRA with Models
    )
    //  format: ON

    val theTests = Table[ Int ](
        "value",
        3,
        9,
        81,
        230,
        -4
    )

    for ( s ← theSolvers; t ← theTests ) {
        test( s"Checking a * a > [$t] with [$s] -- value should v satisfy v * v > $t" ) {

            //  define int var a
            val a = Ints( "a" )

            val model = using( s ) {
                implicit withSolver ⇒
                    //  assert
                    |= ( a * a > Ints( t ) ) flatMap
                        { _ ⇒ checkSat() } flatMap
                        { _ ⇒ getModel() }
            }

            model match {
                case Success( m ) ⇒
                    //  get the value as an Int
                    val z = m.valueOf( a ).map( _.show ).get.toInt
                    logger.info( s"Value of a for target $t is: $z " )
                    z * z should be > t

                case Failure( e ) ⇒ fail( e )
            }
        }
    }
}

/**
 * Non-linear Arithmetics (real) -- check-sat
 */
class NonLinearArithmetic2 extends FunSuite
        with Matchers
        with TableDrivenPropertyChecks with Core
        with RealArithmetics
        with Commands
        with Resources {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    override def suiteName =
        """| ===============================================================
           | Examples from http://rise4fun.com/Z3/tutorial/guide
           | Non Linear Arithmetic -- AUFNIRA
           | (echo "Z3 does not always find solutions to non-linear problems")
           | (declare-const b Real)
           | (declare-const c Real)
           |    (assert ( =
           |        (+ (* b b b) (* b c) )
           |        3.0)
           |    )
           |
           |    (check-sat)
           |    (get-model)
           |    )
           |//  should be sat""".stripMargin

    //  format: OFF
    val theSolvers = Table[ Solver, Try[SatResponses] ](
        ("Solver"                        , "Expected answer")  ,

        (new Z3 with AUFNIRA with Models , Success(UnKnown())) ,

        //  "When presented only non-linear constraints over reals,
        //  Z3 uses a specialized complete solver"
        //  {@link http://rise4fun.com/Z3/tutorial/guide}
        (new Z3 with QF_NRA with Models , Success(Sat())) ,

        //  if Z3 is configured is auto-configured), it can find Sat
        (new Z3 with Models              , Success(Sat()))
    )
    //  format: ON

    val theTests = Table[ Double ](
        "value",
        3.0
    )

    for ( ( s, r ) ← theSolvers; t ← theTests ) {
        test(
            """|
               | Checking ( b * b * b ) + ( b * c ) === 3.0  with
               |""".stripMargin + s"with [$s] configured with ${s.configScript} -- check-sat should give $r"
        ) {

                //  define int var b
                val b = Reals( "b" )
                val c = Reals( "c" )

                using( s ) {
                    implicit withSolver ⇒
                        //  assert
                        |= ( ( b * b * b ) + ( b * c ) === 3.0 ) flatMap
                            { _ ⇒ checkSat() }
                } shouldBe r

            }
    }
}

/**
 * Non-linear Arithmetics (real) -- checksat, getmodel, getvalue
 */
class NonLinearArithmetic3 extends FunSuite
        with Matchers
        with TableDrivenPropertyChecks with Core
        with RealArithmetics
        with Commands
        with Resources {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    override def suiteName =
        """| ===============================================================
           | Examples from http://rise4fun.com/Z3/tutorial/guide
           | Non Linear Arithmetic
           | (echo "When presented only non-linear constraints over reals,
           | Z3 uses a specialized complete solver")
           | (declare-const b Real)
           | (declare-const c Real)
           |    (assert ( =
           |        (+ (* b b b) (* b c) )
           |        3.0)
           |    )
           |
           |    (check-sat)
           |    (get-model)
           |    )
           |//  should be sat""".stripMargin

    //  format: OFF
    val theSolvers = Table[ Solver, Try[SatResponses] ](
        ("Solver"                        , "Expected answer")  ,

        //  "When presented only non-linear constraints over reals,
        //  Z3 uses a specialized complete solver"
        //  {@link http://rise4fun.com/Z3/tutorial/guide}
        (new Z3 with QF_NRA with Models , Success(Sat())) ,

        //  if Z3 is configured is auto-configured), it can find Sat
        (new Z3 with Models              , Success(Sat()))
    )
    //  format: ON

    val theTests = Table[ Double ](
        "value",
        3.0
    )

    for ( ( s, r ) ← theSolvers; t ← theTests ) {
        test(
            """|
               | Checking ( b * b * b ) + ( b * c ) === 3.0  with
               |""".stripMargin + s"with [$s] configured with ${s.configScript} -- check-sat should give $r"
        ) {

                //  define int var b
                val b = Reals( "b" )
                val c = Reals( "c" )

                val model = using( s ) {
                    implicit withSolver ⇒
                        //  assert
                        |= ( ( b * b * b ) + ( b * c ) === 3.0 ) flatMap
                            { _ ⇒ checkSat() } flatMap
                            { _ ⇒ getModel() }
                }

                model match {
                    case Success( m ) ⇒

                        //  get the value as a pair alpha/beta
                        val z1 = m.valueOf( b ).map( _.show ).get.split( "/" )
                        val z2 = m.valueOf( c ).map( _.show ).get.split( "/" )
                        val z1R = z1( 0 ).toDouble / z1( 1 ).toDouble
                        val z2R = z2( 0 ).toDouble / z2( 1 ).toDouble
                        logger.info( s"Value of (b,c) for target $t is: ($z1R,$z2R) " )
                        ( z1R * z1R * z1R ) + ( z1R * z2R ) shouldBe 3.0

                    case Failure( e ) ⇒ fail( e )
                }
            }
    }
}

/**
 *
 */
class NonLinearArithmetic4 extends FunSuite
        with Matchers
        with TableDrivenPropertyChecks with Core
        with IntegerArithmetics
        with Commands
        with Resources {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    override def suiteName =
        """| ===============================================================
           | Examples from http://rise4fun.com/Z3/tutorial/guide
           | Non Linear Arithmetic -- QF_LIA
           | (declare-const a Int)
           | (declare-const r1 Int)
           | (declare-const r2 Int)
           | (declare-const r3 Int)
           | (declare-const r4 Int)
           | (declare-const r5 Int)
           | (declare-const r6 Int)
           | (assert (= a 10))
           | (assert (= r1 (div a 4))) ; integer division
           | (assert (= r2 (mod a 4))) ; mod
           | ;;(assert (= r3 (rem a 4))) ; remainder only Z3
           | (assert (= r4 (div a (- 4)))) ; integer division
           | (assert (= r5 (mod a (- 4)))) ; mod
           | ;;(assert (= r6 (rem a (- 4)))) ; remainder only Z3
           | (check-sat)
           | (get-model)
           |    )
           |//  should be sat""".stripMargin

    //  format: OFF
    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_LIA with Models,
        new SMTInterpol with QF_LIA with Models,
        new CVC4 with QF_LIA with Models
        //  MathSAT does not generate (model ...)
    )
    //  format: ON

    for ( s ← theSolvers ) {
        test( s"Checking (div, mod and rem  with [$s] -- check-sat" ) {

            //  define int var a, b, c
            val a = Ints( "a" )
            val b = Ints( "b" )
            val c = Ints( "c" )
            //  define 7 variables r0-r6
            val r = Range( 0, 7 ).map( "r" + _ ).map( Ints( _ ) )

            val model = using( s ) {
                implicit withSolver ⇒
                    //  assert
                    |= ( a === 10 ) flatMap
                        { _ ⇒ |= ( r( 1 ) === ( a / 4 ) ) } flatMap
                        { _ ⇒ |= ( r( 2 ) === ( a % 4 ) ) } flatMap
                        // { _ ⇒ |= ( r( 3 ) === ( a rem 4 ) ) } flatMap
                        { _ ⇒ |= ( r( 4 ) === ( a / -4 ) ) } flatMap
                        { _ ⇒ |= ( r( 5 ) === ( a % -4 ) ) } flatMap
                        // { _ ⇒ |= ( r( 6 ) === ( a rem -4 ) ) } flatMap
                        { _ ⇒ checkSat() } flatMap
                        { _ ⇒ getModel() }
            }
            model match {
                case Success( m ) ⇒
                    val za = m.valueOf( a ).map( _.show ).get.toInt
                    val r1 = m.valueOf( r( 1 ) ).map( _.show ).get.toInt
                    val r2 = m.valueOf( r( 2 ) ).map( _.show ).get.toInt
                    // val r3 = m.valueOf( r( 3 ) ).map( _.show ).get.toInt
                    val r4 = m.valueOf( r( 4 ) ).map( _.show ).get.toInt
                    val r5 = m.valueOf( r( 5 ) ).map( _.show ).get.toInt
                    // val r6 = m.valueOf( r( 6 ) ).map( _.show ).get.toInt

                    r1 shouldBe za / 4
                    r2 shouldBe za % 4
                    // r3 shouldBe za % 4
                    //
                    r4 shouldBe za / -4
                    r5 shouldBe za % -4
                // r6 shouldBe -( za % 4 )

                case Failure( e ) ⇒ fail( e )
            }
        }
    }
}
