/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package parser
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import Implicits._

//  get interpolants (not in SMTLIB2 )

/**
 * Check that solvers without trait SupportInterpoalnts cannot be
 * instantiated with Interpolants option
 */
class InterpolantUsage extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Usage of Interpolants option test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    import solvers._
    import configurations.Configurations.Interpolants

    test( "Z3 is declared to support interpolants" ) {
        "val s = new Z3 with Interpolants" should compile
    }

    test( "SMTInterpol is declared to support interpolants" ) {
        "val s = new SMTInterpol with Interpolants" should compile
    }

    test( "CVC4 is **NOT** declared to support interpolants" ) {
        "val s = new CVC4 with Interpolants" shouldNot compile
    }

    test( "MathSAT is **NOT** declared to support interpolants" ) {
        "val s = new MathSAT with Interpolants" shouldNot compile
    }
}

/**
 *  Check PPrinter for getInterpolants. This command is not standardised and
 * we need the solver to define the actual command. get-interpolant for Z3,
 * get-interpolants for SMTInterpol, and for MathSAT??
 */
class GetInterpolantPPrinterTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Pprinting get-interpolant(s) with different strings test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    import parser.SMTLIB2PrettyPrinter.format

    val getITPString = Table(
        "get-interpolants",
        "get-interpolant"
    )

    val theNames = Table[ List[ String ] ](
        "List of names",
        List( "P1", "P2" ),
        List( "(and P1 P2)", "P3" )
    )

    def makeTerm( xs : String ) = QIdTerm( SimpleQId( SymbolId( SSymbol( xs ) ) ) )

    for ( itpCmd ← getITPString; names ← theNames ) {
        test( s"Test get-interpolants command $itpCmd syntax with names ${names.mkString( " " )}" ) {

            val pprintedCommand = format(
                GetInterpolantCmd(
                    itpCmd,
                    names map makeTerm
                )
            ).layout

            logger.debug(
                "Solver {} - Pretty printed get-interpolant command {}",
                itpCmd,
                pprintedCommand
            )

            pprintedCommand shouldBe
                s"(${itpCmd} ${names.mkString( " " )} )"
        }
    }

}

class GetInterpolantResponsesTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "get-interpolants responses for different solvers test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    import parser.SMTLIB2PrettyPrinter.format

    val responses = Table[ String ](
        "Interpolant responses",

        """ |
            | ((<= (* 2 (div (+ y 1) 2)) y))
            |
            """.stripMargin,

        """ |
            | (<= x 2)
            |
            """.stripMargin,

        """ |
            |   (<= x 2)
            |   (>= y (+ z 1))
            |
            """.stripMargin,

        """ |
            |   (
            |     (<= x 2)
            |
            |     (>= y (+ z 1))
            |   )
            """.stripMargin,

        """ |
            |     (= x 2)
            |
            |     (< (select a i) x)
            |
            """.stripMargin,

        """ |
            |  (
            |     (= x 2)
            |
            |     (< (select a i) x)
            |   )
            """.stripMargin

    )

    val parseInterpolantResponses = SMTLIB2Parser[ GetInterpolantResponses ]

    for ( r ← responses ) {

        test( s"Parse GetInterpolant response:  $r" ) {

            logger.info( "GetInterpolant responses: {}", parseInterpolantResponses( r ) )

            parseInterpolantResponses( r ) should matchPattern {
                case Success(
                    GetInterpolantResponsesSuccess(
                        InterpolantResponse( _ : List[ Term ] )
                        )
                    ) ⇒
            }
        }
    }
}
