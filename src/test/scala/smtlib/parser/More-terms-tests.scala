/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package parser
import Implicits._

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

//  Terms in SMTLIB2 v2.5,

/**
 *  Parsing rule <term> :=
 * <spec_constant>
 * | <qual_identifier>
 * | (<qual_identifier> <term+>)
 */
class TermsTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing Term a <term> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Term

    val parseTerm = SMTLIB2Parser[ TermTester ]

    //  be careful not to write the same pair twice as this is used
    //  to generate the test name and duplicated names are not allowed.

    //  in the tables of type Table[String, Bool], (x, true)  means x should
    //  be correctly parsed as a Term, and (x, false)  means it should not

    //   test strings
    //  format: OFF
    val strings = Table[ String, Boolean ] (
        ( "input string is a good <spec_constant>?" , "yes/no" ) ,
        ( "x10"                                     , true )     ,
        ( " #x10A"                                  , true )     ,
        ( " #b10"                                   , true )     ,
        ( " _#r"                                    , false )    ,
        ( "x"                                       , true )     ,
        ( "( +  x   1)"                             , true )     ,
        ( " ( < 2 (- x 1))"                         , true )
    )
    //  format: ON

    forAll ( strings ) {
        ( s : String, ok : Boolean ) ⇒

            test( s"Trying to parse $s as a <term>  -- ${if ( ok ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseTerm( s ) )

                parseTerm( s ) should matchPattern {
                    case Failure( _ ) if ( !ok ) ⇒
                    case Success( x ) if ( ok )  ⇒

                }
            }
    }
}
