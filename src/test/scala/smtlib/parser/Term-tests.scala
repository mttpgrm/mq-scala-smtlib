/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package parser
import Implicits._

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

//  Terms in SMTLIB2 v2.5,

/**
 *  Parsing rule <term> := <spec_constant>
 */
class TermRule1Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing Term <term> := <spec_constant> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Term

    val parseTerm = SMTLIB2Parser[ TermTester ]

    //  be careful not to write the same pair twice as this is used
    //  to generate the test name and duplicated names are not allowed.
    //  in the tables of type Table[String, T], (x, Some(t:T)) means x should
    //  be correctly parsed as t:T, and None means it should not

    //  <spec_constant> test strings
    //  format: OFF
    val specConstants = Table[ String, Option[ SpecConstant ] ](
        ( "input string is a good <spec_constant>?", "yes/no" ),

        //  <numeral>

        ( "2"        , Some( NumLit( "2" ) ) )    ,
        ( "12"       , Some( NumLit( "12" ) ) )   ,
        ( "23"       , Some( NumLit( "23" ) ) )   ,
        ( "0"        , Some( NumLit( "0" ) ) )    ,
        ( "3455"     , Some( NumLit( "3455" ) ) ) ,
        ( "00"       , None )                     ,
        ( "2 3"      , None )                     ,
        ( "3455."    , None )                     ,
        ( "2301980_" , None )                     ,

        // <decimal> i.e.  <numeral>.0*<numeral>

        ( "0.0001"        , Some( DecLit( "0.0001" ) ) )       ,
        ( "0.1"           , Some( DecLit( "0.1" ) ) )          ,
        ( "0.0"           , Some( DecLit( "0.0" ) ) )          ,
        ( "0.01"          , Some( DecLit( "0.01" ) ) )         ,
        ( "23.000000004"  , Some( DecLit( "23.000000004" ) ) ) ,
        ( "3455.1"        , Some( DecLit( "3455.1" ) ) )       ,
        ( "2.0"           , Some( DecLit( "2.0" ) ) )          ,
        ( "0.000"         , Some( DecLit( "0.000" ) ) )        ,
        ( "123378.0"      , Some( DecLit( "123378.0" ) ) )     ,
        ( "23.000000 004" , None )                             ,
        ( "34489."        , None )                             ,

        //  <hexadecimal>: '#x' followed by non-empty sequence of
        //  digits and letters from a to F (case insensitive)

        ( "#x1"        , Some( HexaLit( ( "1" ) ) ) )        ,
        ( "#x"         , None )                              ,
        ( "# x1"       , None )                              ,
        ( "#xf"        , Some( HexaLit( ( "f" ) ) ) )        ,

        ( "#xAFe0"     , Some( HexaLit( ( "AFe0" ) ) ) )     ,
        ( "#x45dE2137" , Some( HexaLit( ( "45dE2137" ) ) ) ) ,
        ( "#x 345F"    , None )                              ,

        // <binary>:  '#b' followed by non-empty sequence of 0 and 1

        ( "#b1"        , Some( BinLit( ( "1" ) ) ) )        ,
        ( "#b"         , None )                             ,
        ( "#b01"       , Some( BinLit( ( "01" ) ) ) )       ,
        ( "#b01110010" , Some( BinLit( ( "01110010" ) ) ) ) ,
        ( "#b 1"       , None )                             ,
        ( "# b11"      , None )                             ,
        ( "#xi0"       , None )                             ,
        ( "#b 345F"    , None )                             ,

        //  <string>: sequence of white spaces and printabel characters in double quote
        //  with escape sequence ""
        ( " \"this is it!\""         , Some( StringLit( StringLiteral( "this is it!" ) ) ) ),
        ( " \" @rt why> 01235\""     , Some( StringLit( StringLiteral( " @rt why> 01235" ) ) ) ),
        ( " \"&^$%12 ttr%\""         , Some( StringLit( StringLiteral( "&^$%12 ttr%" ) ) ) ),
        ( " \" le ts b reA l1st1c\"" , Some( StringLit( StringLiteral( " le ts b reA l1st1c" ) ) ) ),
        ( " \"^\\ is a good one\""   , Some( StringLit( StringLiteral( "^\\ is a good one" ) ) ) )
    )
    //  format: ON

    forAll ( specConstants ) {
        ( s : String, answer : Option[ SpecConstant ] ) ⇒

            test( s"Trying to parse $s as a <term> ::= <spec_constant> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseTerm( s ) )

                parseTerm( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                                ⇒
                    case Success( x ) if ( TermTester( ConstantTerm( answer.get ) ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule <term> := <qual_identifier>
 */
class TermRule2Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing Term <term> := <qual_identifier> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Term

    val parseTerm = SMTLIB2Parser[ TermTester ]

    //  <qual_identifier> test strings

    val qualIdentifiers = Table[ String, Option[ QualifiedId ] ](
        ( "input string is a good <qual_identifier>", "yes/no" ),

        //  <identifier>

        //  <symbol>
        //  format: OFF
        ( "x"    , Some( SimpleQId( SymbolId( SSymbol( "x" ) ) ) ) )    ,
        ( "_x"   , Some( SimpleQId( SymbolId( SSymbol( "_x" ) ) ) ) )   ,
        ( "fa0"  , Some( SimpleQId( SymbolId( SSymbol( "fa0" ) ) ) ) )  ,
        ( "_x00" , Some( SimpleQId( SymbolId( SSymbol( "_x00" ) ) ) ) ) ,
        //  format: ON

        //  ( _ <symbol> <index>+)

        (
            "(_ x 1)",
            Some(
                SimpleQId(
                    IndexedId (
                        SSymbol( "x" ),
                        List( NumeralIdx( "1" ) )
                    )
                )
            )
        ),

        (
            "( _  @s anIndex )",
            Some(
                SimpleQId(
                    IndexedId (
                        SSymbol( "@s" ),
                        List( SymbolIdx( SSymbol( "anIndex" ) ) )
                    )
                )
            )
        ),

        //  (as <identifier> <sort>)

        (
            " (  as   x  Int )",
            Some(
                SortedQId (
                    SymbolId( SSymbol( "x" ) ),
                    IntSort()
                )
            )
        ),

        (
            "( as    y ( Int    Bool  )) ",
            Some(
                SortedQId (
                    SymbolId( SSymbol( "y" ) ),
                    SortIdList( IntSort(), List( BoolSort() ) )
                )
            )
        ),

        (
            "( as    y ( Int    Bool  ) ", None
        )
    )

    forAll ( qualIdentifiers ) {
        ( s : String, answer : Option[ QualifiedId ] ) ⇒

            test( s"Trying to parse $s as a <term> ::= <qual_identifier> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseTerm( s ) )

                parseTerm( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                           ⇒
                    case Success( x ) if ( TermTester( QIdTerm( answer.get ) ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule <term> := (<qual_identifier> <term>+)
 */
class TermRule3Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing Term <term> := (<qual_identifier> <term>+)  test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Term

    val parseTerm = SMTLIB2Parser[ TermTester ]

    //  (<qual_identifier> <term>+)  test strings

    val qualIdentifiersAndTerms = Table[ String, Option[ ( QualifiedId, List[ Term ] ) ] ](
        ( "input string is a good (<qual_identifier> <term>+)?", "yes/no" ),

        (
            "( x   1 )",
            Some(
                (
                    SimpleQId( SymbolId( SSymbol( "x" ) ) ),
                    List(
                        ConstantTerm(
                            NumLit( "1" )
                        )
                    )
                )
            )
        ),

        (
            "( x 12  y  )",
            Some(
                (
                    SimpleQId( SymbolId( SSymbol( "x" ) ) ),
                    List(
                        ConstantTerm( NumLit( "12" ) ),
                        QIdTerm( SimpleQId ( SymbolId( SSymbol( "y" ) ) ) )
                    )
                )
            )
        )
    )

    forAll ( qualIdentifiersAndTerms ) {
        ( s : String, answer : Option[ ( QualifiedId, List[ Term ] ) ] ) ⇒

            test( s"Trying to parse $s as a <term> ::= (<qual_identifier> <term>+) -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseTerm( s ) )

                parseTerm( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty ) ⇒
                    case Success( x ) if ( TermTester( QIdAndTermsTerm( answer.get._1, answer.get._2 ) ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule <term> := (! <term> <attribute+)
 */
class TermRule7Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing Term <term> := (! <term> <attribute+)  test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Term

    val parseTerm = SMTLIB2Parser[ TermTester ]

    //  (<qual_identifier> <term>+)  test strings
    //  format: OFF
    val attributedterms = Table[ String, Boolean ](
        ( "input string is a good (<qual_identifier> <term>+)?" , "yes/no" ) ,

        ( "( !   2   :new)"                                     , true )     ,
        ( "( !   x )"                                           , false )    ,
        ( "( !   x   xx )"                                      , false )    ,
        ( "( !   x   :xx )"                                     , true )     ,
        ( "( !   x   :kw1 val1 :kw2 )"                          , true )     ,
        ( "( !   _w   :kw1 val1 :kw2 2)"                        , true )
    // ( "( x 1)", true )

    )
    //  format: ON

    forAll ( attributedterms ) {
        ( s : String, ok : Boolean ) ⇒

            test( s"Trying to parse $s as a <term> ::= (<qual_identifier> <term>+) -- ${if ( ok ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseTerm( s ) )

                parseTerm( s ) should matchPattern {
                    case Failure( _ ) if ( !ok )                                ⇒
                    case Success( TermTester( AttributedTerm( _ ) ) ) if ( ok ) ⇒

                }
            }
    }
}
