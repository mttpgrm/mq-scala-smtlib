/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package parser

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import Implicits._

//  get Models terms in SMTLIB2 v2.5,

/**
 *  Parsing rule <get_info>
 */
class GetInfoParserTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing get-info test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for SpecConstant

    val parseGetInfoCmd = SMTLIB2Parser[ Command ]

    //  <get-info> test strings
    //  format: OFF
    val getInfoTests = Table[ String, Boolean](
        ( "input string?"                       , "is get-info : yes/no" ) ,
        ( "(get-info :all-statistics)"          , true )                    ,
        ( "(get-info :assertions-stack-levels)" , true )                    ,
        ( "(get-info :authors)"                 , true )                    ,
        ( "(get-info :error-behaviour)"         , true )                    ,
        ( "(get-info :reason-unknown)"          , true )                    ,
        ( "(get-info :name )"                   , true )                    ,
        ( "(get-info :version)"                 , true )                    ,

        ( "(get-info :mykwd)"                   , true )                    ,

        ( """|
             | (
             |  get-info
             |    :authors
             |      )
             |
             |""".stripMargin     , true ),
         ( """|
              | (
              |  get-inf
              |    :authors
              |      )
              |
              |""".stripMargin     , false ),
          ( """|
               | (
               |  get-info
               |    x
               |      )
               |
               |""".stripMargin     , false )
    )
    //  format: ON

    forAll ( getInfoTests ) {
        ( s : String, answer : Boolean ) ⇒

            test( s"Trying to parse $s as a <command> := <get_info> -- ${if ( answer ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseGetInfoCmd( s ) )

                parseGetInfoCmd( s ) should matchPattern {

                    case Failure( _ ) if ( !answer ) ⇒
                    case Success( x ) if ( answer )  ⇒

                }
            }
    }
}

/**
 *  Parsing rule <get_info>
 */
class GetInfoPrettyPrinterTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Pretty-printing get-info test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for SpecConstant

    val parseGetInfoCmd = SMTLIB2Parser[ Command ]

    //  <get-info> test strings
    //  format: OFF
    val getInfoTests = Table[ String, String](
        ( "input string?"                        , "pretty-printed" )                     ,
        ("  (   get-info :all-statistics ) "     , "(get-info :all-statistics)")          ,
        ("(get-info :assertions-stack-levels ) " , "(get-info :assertions-stack-levels)") ,
        ("(get-info   :authors )  "              , "(get-info :authors)")                 ,
        ("(  get-info   :error-behaviour )"      , "(get-info :error-behaviour)")         ,
        ("  (get-info :reason-unknown)  "        , "(get-info :reason-unknown)")          ,
        ("  (get-info     :name  )"              , "(get-info :name)")                    ,
        ("(get-info :version )"                  , "(get-info :version)")                 ,

        ("(get-info :mykwd)"                     , "(get-info :mykwd)")                   ,

        ( """|
             | (
             |  get-info
             |    :authors
             |      )
             |
             |""".stripMargin     , "(get-info :authors)" ),
         ( """|
              | (
              |             get-info
              |    :version
              |
              |   )
              |
              |""".stripMargin     , "(get-info :version)" )
    )
    //  format: ON

    import parser.SMTLIB2PrettyPrinter.format

    forAll ( getInfoTests ) {
        ( s : String, r : String ) ⇒

            test( s"Trying to pretty-print $s as a <get_info>" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseGetInfoCmd( s ) )

                parseGetInfoCmd( s ) match {

                    case Success( x ) ⇒ format( x ).layout shouldBe r
                    case Failure( _ ) ⇒ fail

                }
            }
    }
}

/**
 *  Parsing rule <get_info_response>
 */
class GetInfoResponseParserTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing get-info test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for SpecConstant

    val parseGetInfoResponse = SMTLIB2Parser[ GetInfoResponses ]

    //  <get-info> test strings
    //  format: OFF
    val getInfoResponseTests = Table[ String, Boolean](
        ( "input string?"                      , "info response : yes/no" ) ,
        (   "(:all-statistics )"          , true),
        ( "(:all-statistics ())"          , true),
        ( "(:authors \"Me and You\")" , true),
        ( "(:name \"Super Solver\")" , true),

        ( """|
             | ( :all-statistics
             |        (
             |             ( :Core
             |                  (
             |                      (Conflicts 0)
             |
             |                      (Clauses 0)
             |                  )
             |              )
             |
             |
             |      )
             |)""".stripMargin, true),

        ( """|
              | ( :all-statistics
              |             ( :Core
              |                 0
              |              )
              |)""".stripMargin, true
        )
    )

    //  format: ON

    forAll ( getInfoResponseTests ) {
        ( s : String, answer : Boolean ) ⇒

            test( s"Trying to parse $s <get_info_response> -- ${if ( answer ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseGetInfoResponse( s ) )

                parseGetInfoResponse( s ) should matchPattern {

                    case Failure( _ ) if ( !answer )                          ⇒
                    case Success( GetInfoResponseSuccess( x ) ) if ( answer ) ⇒
                    case Success( GetInfoResponseError( x ) ) if ( !answer )  ⇒

                }
            }
    }
}
