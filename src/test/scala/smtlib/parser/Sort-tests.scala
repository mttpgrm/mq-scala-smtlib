/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package parser
import Implicits._

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

//  Attributes in SMTLIB2 v2.5,

/**
 *  Parsing rule  <sort> ::= <identifier>
 */
class SortRule1Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing <sort> := <identifier> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Sort

    val parseSort = SMTLIB2Parser[ SortTester ]

    //  <identifier> test strings
    //  format: OFF
    val identifiers = Table[ String, Option[ Sort ] ](
        ( "input string is a good Sort?" , "yes/no" )                                             ,
        ( "Int"                          , Some( IntSort() ) )                                    ,
        ( "Bool"                         , Some( BoolSort() ) )                                   ,
        ( "A"                            , Some( SortId ( SymbolId ( SSymbol ( "A" ) ) ) ) )      ,
        ( "_Qsort"                       , Some( SortId ( SymbolId ( SSymbol ( "_Qsort" ) ) ) ) ) ,
        ( "_12"                          , Some( SortId ( SymbolId ( SSymbol ( "_12" ) ) ) ) )    ,
        ( "12"                           , None )
    )
    //  format: ON

    forAll ( identifiers ) {
        ( s : String, answer : Option[ Sort ] ) ⇒

            test( s"Trying to parse $s as a <sort> ::= <identifier> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseSort( s ) )

                parseSort( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                                    ⇒
                    case Success( x ) if ( answer.isDefined && SortTester( answer.get ) == x ) ⇒
                    // case Success( x ) if ( SortTester( answer.get ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule  <sort> ::= <identifier> <sort>+
 */
class SortRule2Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing <sort> := ( <identifier> <sort>+ )  test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Sort

    val parseSort = SMTLIB2Parser[ SortTester ]

    //  <identifier> <sort>+ test strings

    val identifiersSort = Table[ String, Option[ Sort ] ](
        ( "input string is a good Sort?", "yes/no" ),

        (
            "(  Int  Bool )",
            Some(
                SortIdList(
                    IntSort(),
                    List( BoolSort() )
                )
            )
        ),

        (
            "( Int (Bool Int))",
            Some(
                SortIdList(
                    IntSort(),
                    List(
                        SortIdList(
                            BoolSort(),
                            List ( IntSort() )
                        )
                    )
                )
            )
        ),

        (
            "( A (_Int C))",
            Some(
                SortIdList(
                    SortId( SymbolId( SSymbol( "A" ) ) ),
                    List(
                        SortIdList(
                            SortId( SymbolId ( SSymbol( "_Int" ) ) ),
                            List( SortId( SymbolId ( SSymbol( "C" ) ) ) )
                        )
                    )
                )
            )
        ),

        (
            "(Array (Int (Array Int)))",
            Some(
                SortIdList(
                    SortId( SymbolId( SSymbol( "Array" ) ) ),
                    List(
                        SortIdList(
                            IntSort(),
                            List (
                                SortIdList(
                                    SortId( SymbolId( SSymbol( "Array" ) ) ),
                                    List( IntSort() )
                                )
                            )
                        )
                    )
                )
            )
        ),

        ( " ( (A B) C) ", None )

    )

    forAll ( identifiersSort ) {
        ( s : String, answer : Option[ Sort ] ) ⇒

            test( s"Trying to parse $s as a <sort> ::= (<identifier> <sort>+) -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseSort( s ) )

                parseSort( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                ⇒
                    case Success( x ) if ( SortTester( answer.get ) == x ) ⇒

                }
            }
    }
}
