/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package parser

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import Implicits._

//  get Models terms in SMTLIB2 v2.5,

/**
 *  Parsing rule <get_model_response>
 */
class GetModelParserTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing get-model-responses test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for SpecConstant

    val parseGetModel = SMTLIB2Parser[ ModelResponseTester ]

    //  <models> test strings
    //  format: OFF
    val models = Table[ String, Boolean](
        ( "input string?"           , "Model response: yes/no" ) ,
        ( """|
             | (
             |  model
             |    (define-fun x
             |      () Int 1)
             | )
             |""".stripMargin     , true ),
         ( """|
              | (
              |  model
              |    (define-fun x
              |      () Int 1)
              |
              |     (define-fun y
              |      () Bool true)
              | )
              |""".stripMargin     , true ),
          ( """|
               | (
               |  model
               |    (define- x
               |      () Int 1)
               |
               |     (define-fun y
               |      () Bool true)
               | )
               |""".stripMargin     , false ),
           ( """|
                | (
                |  model
                |    (define-fun x
                |      ( (f V) (h U) ) Int 1)
                |
                |     (define-fun y
                |      () Bool true)
                | )
                |""".stripMargin     , true ),
            ( """|
                 | (
                 |  model
                 |    (define-fun x
                 |      ( (f V) (h U) ) (Array U Int) 1)
                 |
                 |     (define-fun y
                 |        (
                 |          ( a (Array Int Real) )
                 |        ) Bool true)
                 | )
                 |""".stripMargin     , true ),

            ( """|
                 | (model
                 | (define-fun z () (Array Int Int)
                 |  (_ as-array k!0)
                 | )
                 |   (define-fun y () Int
                 |    0)
                 | (define-fun k!0 ((x!1 Int)) Int
                 |  (ite (= x!1 2) (- 1)
                 |  (ite (= x!1 1) 0
                 |   (- 1))))
                 | )
                 |""".stripMargin       , true ),


            ( """|
                 |  ;; returned by MathSAT when get-model command issued.
                 |  ;; Not conformant to SMTLIB2
                 | (
                 |   ( z
                 |     (
                 |     (as const (Array Int Int) )
                 |     0
                 |     )
                 |   )
                 |
                 |  (y 0)
                 |)
                 |""".stripMargin       , false )


    )
    //  format: ON

    forAll ( models ) {
        ( s : String, answer : Boolean ) ⇒

            test( s"Trying to parse $s as a <gen_response> -- ${if ( answer ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseGetModel( s ) )

                parseGetModel( s ) should matchPattern {

                    case Failure( _ ) if ( !answer ) ⇒
                    case Success(
                        ModelResponseTester( GetModelResponseSuccess( _ ) )
                        ) if ( answer ) ⇒
                    case Success(
                        ModelResponseTester( GetModelResponseError( _ ) )
                        ) if ( !answer ) ⇒

                }
            }
    }
}
