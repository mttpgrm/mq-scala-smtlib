/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package typedterms
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ IntegerArithmetics, Core }

import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

class LetTermTermDefTest extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with QuantifiedTerm {

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    override def suiteName = "LeTerm parsing test suite"

    import parser.SMTLIB2Syntax.{ LetTerm }
    import parser.Implicits._
    import parser.SMTLIB2PrettyPrinter.format
    import theories.IntTerm

    //  useful variables
    val x = Ints( "x" )
    val y = Ints( "y" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[IntTerm, LetTerm], String ](

        ( "Scala def", "Term" , "Expected SMTLIB2 term" ) ,
        (
            """ |
                | let {
                |   val x1 = BoundedVar( "x1", x + 1)
                |   x1 + 2
                | }
                | """.stripMargin,
            let {
                val x1 = BoundedVar( "x1", x + 1)
                x1 + 2
            },
            "(let ( (x1 (+ x 1 ) ) ) (+ x1 2 ) ) "
        ),

        (
            """ |
                | let {
                |    val x1 = BoundedVar( "x1", x + 1)
                |    val x2 = BoundedVar( "x2", y + x)
                |    x1 + x2
                | }
                | """.stripMargin,
            let {
                val x1 = BoundedVar( "x1", x + 1)
                val x2 = BoundedVar( "x2", y + x)
                x1 + x2
            },
            "(let ( (x1 (+ x 1 ) ) (x2 (+ y x ) ) ) (+ x1 x2 ) ) "
        ),

        //  nested let
        (
            """ |
                | let {
                |   val x1 = BoundedVar( "x1", x + 1)
                |       let {
                |           val x2 = BoundedVar( "x2", x1 + y + x)
                |           x2
                |       }
                | }
                | """.stripMargin,
            let {
                val x1 = BoundedVar( "x1", x + 1)

                let {
                    val x2 = BoundedVar( "x2", x1 + y + x)
                    x2
                }
            },
            "(let ( (x1 (+ x 1 ) ) ) (let ( (x2 (+ (+ x1 y ) x ) ) (x1 (+ x 1 ) ) ) x2 ) ) "
        )


    )
    // format: ON

    for ( ( txt, xt, r ) ← theTerms ) {

        test( s"Check letTerm termDef for $txt -- should be $r" ) {

            logger.info( "formated term: {}", format( xt.termDef ).layout )
            format( xt.termDef ).layout shouldBe r
        }
    }
}

class LetTermTypeDefsTest extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with QuantifiedTerm {

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    override def suiteName = "LeTerm typedDefs test suite"

    import parser.SMTLIB2Syntax.{ QualifiedId, LetTerm, SortedQId }
    import parser.Implicits._
    import parser.SMTLIB2PrettyPrinter.format
    import theories.IntTerm
    import parser.SMTLIB2Parser

    //  get a SortedQId parser
    //  as SotedQId is a rule of QualifiedId we get QualifiedId parser first
    val p1 = SMTLIB2Parser[ QualifiedId ]
    val sortedIdParser : String ⇒ SortedQId = { x ⇒ p1( x ).get.asInstanceOf[ SortedQId ] }

    //  create a solver SortedQId, type Ints, from a string
    def toSortedQId( name : String ) : SortedQId = sortedIdParser( s"(as $name Int)" )

    val x = Ints( "x" )
    val y = Ints( "y" )

    //  format: OFF
    val theTerms = Table[ TypedTerm[IntTerm, LetTerm], Set[SortedQId] ](

        ( "Terms" , "Expected set of typedefs" ) ,
        (
            let {
                val x1 = BoundedVar( "x1", x + 1)
                x1 + 2
            },
            Set("x") map toSortedQId
        ),

        (
            let {
                val x1 = BoundedVar( "x1", x + 1)
                val x2 = BoundedVar( "x2", y + x)
                x1 + x2
            },
            Set("x", "y") map toSortedQId
        ),

        //  nested let
        (
            let {
                val x1 = BoundedVar( "x1", x + 1)

                let {
                    val x2 = BoundedVar( "x2", x1 + y + x)
                    x2
                }

            },
            Set("x", "y") map toSortedQId
        )


    )
    // format: ON

    for ( ( xt, r ) ← theTerms ) {

        test( s"Check letTerm typeDefs  for ${format( xt.termDef ).layout} -- should be $r" ) {

            logger.info( "formated term: {}", format( xt.termDef ).layout )
            xt.typeDefs shouldBe r
        }
    }
}
