/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package parser
package tests

import Implicits._
import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import au.edu.mq.comp.smtlib.parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

object NumeralIdxSamples extends TableDrivenPropertyChecks {

    //  format: OFF
    val numeralIndices = Table[ String, Option[ Idx ] ] (
        ( "input string is a good index?" , "yes/no" )                     ,
        ( "2"                             , Some( NumeralIdx( "2" ) ) )    ,
        ( "12"                            , Some( NumeralIdx( "12" ) ) )   ,
        ( "23"                            , Some( NumeralIdx( "23" ) ) )   ,
        ( "0"                             , Some( NumeralIdx( "0" ) ) )    ,
        ( "3455"                          , Some( NumeralIdx( "3455" ) ) ) ,
        ( " x dv"                         , None )
    )
    //  format: ON
}

//  Identifiers in SMTLIB2 v2.5,

/**
 *  Parsing rule  <index> ::= <numeral>
 */
class IndexRule1Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing  <index> ::= <numeral> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Index

    val parseIndex = SMTLIB2Parser[ IdxTester ]

    //  <numeral> test strings
    import NumeralIdxSamples.numeralIndices

    forAll ( numeralIndices ) {
        ( s : String, answer : Option[ Idx ] ) ⇒

            test( s"Trying to parse $s as a <index> ::= <numeral> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseIndex( s ) )

                parseIndex( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )               ⇒
                    case Success( x ) if ( IdxTester( answer.get ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule  <index> ::= <symbol>
 */
class IndexRule2Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing  <index> ::= <numeral> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Index

    val parseIndex = SMTLIB2Parser[ IdxTester ]

    //  import the sample symbols

    import SMTLIB2SymbolsSamples.{ sSymbols, delSymbols }
    import Implicits._

    forAll ( sSymbols ++ delSymbols ) {
        ( s : String, answer : Option[ SMTLIB2Symbol ] ) ⇒

            test( s"Trying to parse $s as a <index> ::= <symbol> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {
                logger.debug( "checking {} - parse command returned:{}", s, parseIndex( s ) )

                parseIndex( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                            ⇒
                    case Success( x ) if ( IdxTester( SymbolIdx( answer.get ) ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule  <identifier> ::= <symbol>
 */
class IdentifierRule1Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing  <identifier> ::= <symbol> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Identifier

    val parseId = SMTLIB2Parser[ IdTester ]

    //  import the sample symbols

    import SMTLIB2SymbolsSamples.{ sSymbols, delSymbols }

    forAll ( sSymbols ++ delSymbols ) {
        ( s : String, answer : Option[ SMTLIB2Symbol ] ) ⇒

            test( s"Trying to parse $s as a <index> ::= <symbol> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {
                logger.debug( "checking {} - parse command returned:{}", s, parseId( s ) )

                parseId( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                          ⇒
                    case Success( x ) if ( IdTester( SymbolId( answer.get ) ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule  <identifier> ::= (_ <symbol> <index>+)
 */
class IdentifierRule2Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing  <identifier> ::= (_ <symbol> <index>+) test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Identifier

    val parseId = SMTLIB2Parser[ IdTester ]

    //  import the sample symbols

    import SMTLIB2SymbolsSamples.{ sSymbols, delSymbols }
    import NumeralIdxSamples.numeralIndices

    for ( v1 ← sSymbols ++ delSymbols; v2 ← numeralIndices ) {

        //  string to parse
        val s = s"(_ ${v1._1} ${v2._1})"

        //  parse the index (v2) to  check if it is a good <index>+
        //  the table numeralIndices contains unique index
        val parseIndexPlus = SMTLIB2Parser[ IdxPlusTester ]
        val indexPlus = parseIndexPlus( v2._1 ) match {
            case Success( IdxPlusTester( x ) ) ⇒ Success( x )
            case Failure( e )                  ⇒ Failure( e )
        }

        val answer =
            if ( v1._2.nonEmpty && indexPlus.isSuccess )
                Some( IndexedId( v1._2.get, indexPlus.get ) )
            else
                None

        test( s"Trying to parse $s as a <index> ::= (_ <symbol> <index>+) -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {
            logger.debug( "checking {} - parse command returned:{}", s, parseId( s ) )

            parseId( s ) should matchPattern {
                case Failure( _ ) if ( answer.isEmpty )              ⇒
                case Success( x ) if ( IdTester( answer.get ) == x ) ⇒

            }
        }
    }
}
