/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package parser
import Implicits._

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

object SMTLIB2SymbolsSamples extends TableDrivenPropertyChecks {
    //  a non-empty sequence of letters, digits, and the characters
    //  +, -, /, *, =, %, ?, ., $, ~, &, ^, <, >, @, that does start with
    //  a digit

    //  format: OFF
    val sSymbols = Table[ String, Option[ SMTLIB2Symbol ] ] (
        ( "input string is SSymbol?"     , "yes/no" )                    ,

        //  SSymbol
        ( "a"                            , Some( SSymbol( "a" ) ) )      ,
        ( "bv1"                          , Some( SSymbol( "bv1" ) ) )    ,
        ( "+x"                           , Some( SSymbol( "+x" ) ) )     ,
        ( "-trois"                       , Some( SSymbol( "-trois" ) ) ) ,
        ( "-12"                          , Some( SSymbol( "-12" ) ) )    ,
        ( "!1"                           , Some( SSymbol( "!1" ) ) )     ,
        ( ".2x"                          , Some( SSymbol( ".2x" ) ) )    ,
        ( "$x"                           , Some( SSymbol( "$x" ) ) )     ,
        ( "$2"                           , Some( SSymbol( "$2" ) ) )     ,
        ( "$_1"                          , Some( SSymbol( "$_1" ) ) )    ,
        ( "&1"                           , Some( SSymbol( "&1" ) ) )     ,
        ( "&x"                           , Some( SSymbol( "&x" ) ) )     ,
        ( "~1"                           , Some( SSymbol( "~1" ) ) )     ,
        ( "^xa"                          , Some( SSymbol( "^xa" ) ) )    ,
        ( "^0"                           , Some( SSymbol( "^0" ) ) )     ,
        ( "<0"                           , Some( SSymbol( "<0" ) ) )     ,
        ( "<x"                           , Some( SSymbol( "<x" ) ) )     ,
        ( "><"                           , Some( SSymbol( "><" ) ) )     ,
        ( "a1@i"                         , Some( SSymbol( "a1@i" ) ) )     ,

        //  IndexedSymbol
        ( "x_12@2"                       , Some( ISymbol( "x_12",2 ) ) ) ,
        ( "%x_12@2"                      , Some( ISymbol( "%x_12" ,2 ) ) ) ,
        ( "a1@2"                         , Some( ISymbol( "a1", 2 ) ) ) ,
        ( "a1_i@34"                      , Some( ISymbol( "a1_i", 34 ) ) ) ,
        ( "a1_@0"                        , Some( ISymbol( "a1_", 0 ) ) ) ,
        ( "a1@10"                        , Some( ISymbol( "a1", 10 ) ) )     ,

        //  not SSymbol nor ISymbol nor DelimtedSymbol
        ( "1xs"                          , None )                        ,
        ( "012"                          , None )                        ,
        //  note: the following test may break tests that use the string concatenated
        //  with another one. An example is _ <symbol> <index>+. "@ xd@" is not a symbol
        //  but "_ @ xd" is _ <symbol> <index>+
        // ( "@ xd", None ),
        ( "8\""    , None ) ,
        ( "exists" , None ) ,
        ( "forall" , None ) ,
        ( "par"    , None ) ,
        ( "!"      , None ) ,
        ( "_"      , None ) ,
        ( "let"    , None )
    )
    //  format: ON

    //  or a sequence of whitespace and printable characters that starts and
    //  ends with | and does not otherwise include | or \

    //  format: OFF
    val delSymbols = Table[ String, Option[ SMTLIB2Symbol ] ] (
        ( "input string is a good Sort?" , "yes/no" )                                         ,
        ( "|this is the input|"          , Some( DelimitedSeq( "this is the input" ) ) )      ,
        ( "|@^*4kjl(*(*)(*)*__)_)_|"     , Some( DelimitedSeq( "@^*4kjl(*(*)(*)*__)_)_" ) ) ) ,
        ( "|234|"                        , Some( DelimitedSeq( "234" ) ) )                    ,
        ( "| missing matching"           , None )
    )
    //  format: ON
}

//  Symbol in SMTLIB2 v2.5,

/**
 *  Parsing rule  <symbol> ::= <simple_symbol>
 */
class SMTLIB2SymbolRule1Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    override def suiteName = "Parsing <symbol> ::= <simple_symbol> test suite"

    import SMTLIB2SymbolsSamples.sSymbols

    //  SMTLIB2Symbol parser

    val parseSymbol = SMTLIB2Parser[ SMTLIB2SymbolTester ]

    forAll ( sSymbols ) {
        ( s : String, answer : Option[ SMTLIB2Symbol ] ) ⇒

            test( s"Trying to parse $s as a <symbol> ::= <simple_symbol> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {
                logger.debug( "checking {} - parse command returned:{}", s, parseSymbol( s ) )

                parseSymbol( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                         ⇒
                    case Success( x ) if ( SMTLIB2SymbolTester( answer.get ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule  <symbol> ::= | characters |
 */
class SMTLIB2SymbolRule2Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    override def suiteName = "Parsing <symbol> ::= | characters | test suite"

    import SMTLIB2SymbolsSamples.delSymbols

    //  SMTLIB2Symbol parser

    val parseSymbol = SMTLIB2Parser[ SMTLIB2SymbolTester ]

    forAll ( delSymbols ) {
        ( s : String, answer : Option[ SMTLIB2Symbol ] ) ⇒

            test( s"Trying to parse $s as a <symbol> ::= | characters | -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseSymbol( s ) )

                parseSymbol( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                         ⇒
                    case Success( x ) if ( SMTLIB2SymbolTester( answer.get ) == x ) ⇒

                }
            }
    }
}
