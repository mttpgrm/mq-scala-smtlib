/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package typedterms
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ IntegerArithmetics, Core }

import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

/**
 * Check the pretty printed term of ExistsTerm
 */
class ExistsTermTermDefsTest extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with QuantifiedTerm {

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    override def suiteName = "ExistsTerm termDef test suite"

    import parser.SMTLIB2Syntax.{ Term, ExistsTerm }
    import parser.Implicits._
    import parser.SMTLIB2PrettyPrinter.format
    import theories.BoolTerm
    import parser.SMTLIB2Parser

    val x = Ints( "x" )
    val y = Ints( "y" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[BoolTerm, Term], String ](

        ( "SMTLIB version" , "typed term version", "Expected pretty printed term" ) ,
        (
            """|
               | ( exists
               |      (
               |          (x Int)
               |      )
               |      ( <=
               |           ( + x 2 )
               |           1
               |      )
               | )
               |""".stripMargin,
            (exists (Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= 1
            }),
            "(exists ((x1 Int )) (<= (+ x1 2 ) 1 ) ) "
        ),

        //  one var not bounded
        (
            """|
               | ( exists
               |      (
               |          (x1 Int)
               |      )
               |      (and
               |        ( <=
               |           ( + x1 2 )
               |           1
               |        )
               |        ( = x 3)
               |      )
               | )
               |""".stripMargin,
            (exists(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= 1
            }) & (x === 3),
            "(and (exists ((x1 Int )) (<= (+ x1 2 ) 1 ) ) (= x 3 ) ) "
        ),

        //  one var not bounded
        (
            """|
               | ( and
               |   ( exists
               |     (
               |        (x1 Int )
               |     )
               |    ( <=
               |        ( + x1 2 )
               |        y
               |    )
               |   )
               |   ( = x 3 )
               | )
               |""".stripMargin,
            (exists(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= y
            }) & (x === 3),
            "(and (exists ((x1 Int )) (<= (+ x1 2 ) y ) ) (= x 3 ) ) "
        ),

        //  indexed variable
        (
            """|
               | ( exists
               |   (
               |    (x1@1 Int )
               |   )
               |   ( <=
               |    (+ x1@1 2 )
               |    1
               |   )
               | )
               |""".stripMargin,
            exists(Ints("x1").indexed(1).symbol) {
                val x1 = Ints("x1").indexed(1)
                x1 + 2 <= 1
            },
            "(exists ((x1@1 Int )) (<= (+ x1@1 2 ) 1 ) ) "
        ),

        //  nested exists
        (
            """|
               | ( exists
               |   (
               |    (x1 Int )
               |   )
               |   ( =>
               |    ( <= x1 0 )
               |    ( exists
               |        (
               |         (y1 Int )
               |        )
               |        ( =>
               |          ( >= y1 0 )
               |          ( >= y1 x1 )
               |        )
               |    )
               |   )
               | )
               |""".stripMargin,
            exists(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                exists(Ints("y1").symbol) {
                    val y1 = Ints("y1")
                    y1 >= 0 imply y1 >= x1
                }
            },
            "(exists ((x1 Int )) (=> (<= x1 0 ) (exists ((y1 Int )) (=> (>= y1 0 ) (>= y1 x1 ) ) ) ) ) "
        )
    )
    // format: ON

    for ( ( txt, xt, r ) ← theTerms ) {

        test( s"Check ExistsTerm termDef for $txt -- should be $r" ) {

            logger.info( "formated term: {}", format( xt.termDef ).layout )
            format( xt.termDef ).layout shouldBe r
        }
    }
}

/**
 * Check the typedDefs of ExistsTerm
 */
class ExistsTermTypeDefsTest extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with QuantifiedTerm {

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    override def suiteName = "ExistsTerm typedDefs test suite"

    import parser.SMTLIB2Syntax.{ Term, QualifiedId, ExistsTerm, SortedQId }
    import parser.Implicits._
    import parser.SMTLIB2PrettyPrinter.format
    import theories.BoolTerm
    import parser.SMTLIB2Parser

    //  get a SortedQId parser
    //  as SotedQId is a rule of QualifiedId we get QualifiedId parser first
    val p1 = SMTLIB2Parser[ QualifiedId ]
    val sortedIdParser : String ⇒ SortedQId = { x ⇒ p1( x ).get.asInstanceOf[ SortedQId ] }

    //  create a solver SortedQId, type Ints, from a string
    def toSortedQId( name : String ) : SortedQId = sortedIdParser( s"(as $name Int)" )

    val x = Ints( "x" )
    val y = Ints( "y" )

    //  format: OFF
    val theTerms = Table[ TypedTerm[BoolTerm, Term], Set[SortedQId] ](

        ( "Terms" , "Expected set of typedefs" ) ,
        (
            (exists (Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= 1
            }),
            Set() map toSortedQId
        ),

        //  one var not bounded
        (
            (exists(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= 1
            }) & (x === 3),
            Set("x") map toSortedQId
        ),

        //  one var not bounded
        (
            (exists(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= y
            }) & (x === 3),
            Set("x", "y") map toSortedQId
        ),

        //  indexed variable
        (
            exists(Ints("x1").indexed(1).symbol) {
                val x1 = Ints("x1").indexed(1)
                x1 + 2 <= 1
            },
            Set() map toSortedQId
        ),

        //  nested exists
        (
            exists(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                exists(Ints("y1").symbol) {
                    val y1 = Ints("y1")
                    y1 >= 0 imply y1 >= x1
                }
            },
            Set() map toSortedQId
        )
    )
    // format: ON

    for ( ( xt, r ) ← theTerms ) {

        test( s"Check ExistTerm typeDefs for term  ${format( xt.termDef ).layout} -- should be $r" ) {

            logger.info( "formated term: {}", format( xt.termDef ).layout )
            xt.typeDefs shouldBe r
        }
    }
}
