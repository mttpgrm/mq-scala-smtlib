/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package parser

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import Implicits._

//  Attributes in SMTLIB2 v2.5,

/**
 *  Parsing rule <attribute> := <keyword> <attribute_value>
 */
class AttributeRule1Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing Attributes <attribute> ::= <keyword> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for SpecConstant

    val parseAttribute = SMTLIB2Parser[ AttributeTester ]

    //  <keyword> test strings
    //  format: OFF
    val keywords = Table[ String, Option[ Attribute ] ](
        ( "input string?"         , "Good attribute: yes/no" ),

        ( ":key"                  , Some( AttributeKeyword( Keyword( "key" ) ) ) ),
        ( ":thisIsAnotherKeyword" , Some( AttributeKeyword( Keyword( "thisIsAnotherKeyword" ) ) ) ),
        ( ": notAGoodOne"         , None ),

        //  :named should be parsed as it is an AttributedTerm
        //  as a Term it should not be an AttributedTerm but a NamedTerm
        ( ":named"                , Some( AttributeKeyword( Keyword( "named" ) ) ) )
    )
    //  format: ON

    forAll ( keywords ) {
        ( s : String, answer : Option[ Attribute ] ) ⇒

            test( s"Trying to parse $s as a <attribute> ::= <keyword> <attribute_value>-- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseAttribute( s ) )

                parseAttribute( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                     ⇒
                    case Success( x ) if ( AttributeTester( answer.get ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule <attribute> := <keyword> <attribute_value>
 */
class AttributeRule2Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing Attributes <attribute> ::= <keyword> <attribute_value> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Attribute

    val parseAttribute = SMTLIB2Parser[ AttributeTester ]

    //  <keyword> <attribute_value> test strings

    val keywordsAttVal = Table[ String, Option[ Attribute ] ](
        ( "input string is a good attribute?", "yes/no" ),

        (
            ":key  11",
            Some(
                AttributeKeywordAndValue(
                    Keyword( "key" ),
                    AttValueSpecConstant( NumLit( "11" ) )
                )
            )
        ),

        (
            ":anotherKey keyVal",
            Some(
                AttributeKeywordAndValue(
                    Keyword( "anotherKey" ),
                    AttValueSymbol( SSymbol( "keyVal" ) )
                )
            )
        )
    )

    forAll ( keywordsAttVal ) {
        ( s : String, answer : Option[ Attribute ] ) ⇒

            test( s"Trying to parse $s as a <attribute> ::= <keyword> <attribute_value> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseAttribute( s ) )

                parseAttribute( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                     ⇒
                    case Success( x ) if ( AttributeTester( answer.get ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule <attribute> := <keyword> <attribute_value>
 */
class NamedTermTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing Attributes <term> ::= <namedTerm> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Attribute

    val parseTerm = SMTLIB2Parser[ TermTester ]

    //  <keyword> <attribute_value> test strings
    //  format: OFF
    val theTerms = Table[ String, Term  ](
        ( "input string?"         , "named term: yes/no" ),

        ( "( ! y :key val)",

            AttributedTerm(
                TermPlusAttributes(
                    QIdTerm(
                        SimpleQId(
                            SymbolId(SSymbol("y"))
                        )
                    ),
                    List(
                        AttributeKeywordAndValue(
                            Keyword("key"),
                            AttValueSymbol(SSymbol("val"))
                        )
                    )
                )
            )
        ),

        ( "(! 1 :notAGoodOne 9)",

            AttributedTerm(
                TermPlusAttributes(
                    ConstantTerm(NumLit("1")),
                    List(
                        AttributeKeywordAndValue(
                            Keyword("notAGoodOne"),
                            AttValueSpecConstant(NumLit("9"))
                        )
                    )
                )
            )
        ),

        //  :named should be parsed as it is an AttributedTerm
        //  as a Term it should not be an AttributedTerm but a NamedTerm
        ( "(! x :named theName)",

            NamedTerm(
                QIdTerm(SimpleQId(SymbolId(SSymbol("x")))),
                SSymbol("theName")
            )
        )

    )
    //  format: ON

    forAll ( theTerms ) {
        ( s : String, answer : Term ) ⇒

            test( s"Trying to parse $s as a <term> ::= <named_term -- should be $answer" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseTerm( s ) )
                parseTerm( s ) shouldBe Success( TermTester( answer ) )

            }
    }
}
