/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package parser

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import Implicits._
import interpreters.Resources
import org.bitbucket.inkytonik.kiama.util.FileSource

class ParseFileTests extends FunSuite with Matchers with Resources {

    override def suiteName = "Parsing simple files test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    ignore( "Read .smt2 file -- file is missing in repository. Check it in" ) {

        //  path where the files are
        val path = "src/test/scala/smtlib/parser/resources/"

        //  Get  a parser for SMTLIB2 script
        val parser = SMTLIB2Parser[ Script ]

        //  Parse the file
        val t = parser( FileSource( path + "ex1.smt2" ) )
        t should matchPattern {
            case Success( Script( _ ) ) ⇒
        }

        //  get list of commands
        val Script( cmds ) = t.get
        val status = cmds find { x ⇒
            x match {
                case SetInfo(
                    AttributeKeywordAndValue(
                        Keyword( "status" ),
                        AttValueSymbol( SSymbol( _ ) )
                        )
                    ) ⇒ true
                case _ ⇒ false

            }
        }

        //  assume status is Some!
        assert( status.isDefined )

        //  make a SatStatus with status
        // val satStat = SMTLIB2Parser[ CheckSatResponse ]
        import interpreters.SMTSolver
        import solvers._
        val s = UnSat()
        using ( SMTSolver( new Z3 ) ) {
            implicit s ⇒
                val testval = eval( cmds.init )
            // println( testval )
        }
    }

    //
}
