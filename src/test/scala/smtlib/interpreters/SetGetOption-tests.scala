/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package interpreters
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ Core, IntegerArithmetics }
import interpreters.Resources

/**
 *  Send a get-option command to the solvers
 */
class GetOptionCmdTests
        extends FunSuite
        with TableDrivenPropertyChecks
        with Matchers
        with Resources {

    override def suiteName = "get-option command test suite"

    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ Raw, GetOptionResponseSuccess, GetOptionResponses, GetOptionResponseError }
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.format
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import theories.PredefinedLogics.QF_LIA
    import theories.tests.PPrinter._
    import parser.SMTLIB2Parser
    import parser.Implicits._

    val parserGetOptionResponse = SMTLIB2Parser[ GetOptionResponses ]

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_LIA,
        new SMTInterpol with QF_LIA,
        new CVC4 with QF_LIA,
        new MathSAT with QF_LIA,
        new Yices with QF_LIA
    )

    //  <get-option> test strings
    //  format: OFF
    val getOptionTests = Table[ String, Solver => Boolean](
        ("input string" , "supported by solver?"),

        (  """  |
                | (
                |    get-option :diagnostic-output-channel
                | )
                | """.stripMargin,          { x  =>
                    if (x.isInstanceOf[MathSAT] || x.isInstanceOf[CVC4]) false else true }),

        ( """|
             | (
             |  get-option
             |    :global-declarations
             | )
             |
             |""".stripMargin     , {
                 x => if (x.isInstanceOf[Z3]) true else false
             } ),


         (  """  |
                 | (
                 |    get-option :interactive-mode
                 | )
                 | """.stripMargin,         { x  => if (x.isInstanceOf[MathSAT] || x.isInstanceOf[Yices]) false else true }),


        (  """  |
                | (
                |    get-option :print-success
                | )
                | """.stripMargin,          { x  => if (x.isInstanceOf[MathSAT]) false else true }),

        (  """  |
                | (
                |    get-option :produce-assertions
                | )
                | """.stripMargin,         { x  => if (x.isInstanceOf[Z3])  true else false }),

        (  """  |
                | (
                |    get-option :produce-assignments
                | )
                | """.stripMargin,     { x  => if (x.isInstanceOf[MathSAT]) false else true }),

        (  """  |
                | (
                |    get-option :produce-models
                | )
                | """.stripMargin,          { x  => if (x.isInstanceOf[MathSAT]) false else true }),

        (  """  |
                | (
                |    get-option :produce-proofs
                | )
                | """.stripMargin,          { x  => if (x.isInstanceOf[MathSAT] || x.isInstanceOf[Yices]) false else true }),

        (  """  |
                | (
                |    get-option :produce-unsat-assumptions
                | )
                | """.stripMargin,         _ => false ),

        (  """  |
                | (
                |    get-option :produce-unsat-cores
                | )
                | """.stripMargin,          { x  => if (x.isInstanceOf[MathSAT] || x.isInstanceOf[Yices]) false else true }),

        (  """  |
                | (
                |    get-option :random-seed
                | )
                | """.stripMargin,          { x  => if (x.isInstanceOf[MathSAT]) false else true }),

        (  """  |
                | (
                |    get-option :regular-output-channel
                | )
                | """.stripMargin,          { x  =>
                    if (x.isInstanceOf[MathSAT] || x.isInstanceOf[CVC4]) false else true }),

        (  """  |
                | (
                |    get-option :reproducible-resource-limit
                | )
                | """.stripMargin,        _ => false ),

        (  """  |
                | (
                |    get-option :verbosity
                | )
                | """.stripMargin,          { x  =>
                    if (x.isInstanceOf[MathSAT]) false else true })

    )
    //  format: ON

    import org.bitbucket.inkytonik.kiama.util.{ Source, StringSource, FileSource }

    for ( s ← theSolvers; ( cmd, r ) ← getOptionTests ) {

        test( s"Using solver $s configured with ${pprintConfig( s )} eval $cmd -- ${if ( r( s ) ) "Should succeed" else "Not supported, Should fail"}" ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        eval ( Seq( Raw( cmd ) ) ) match {

                            case Success( response ) ⇒

                                parserGetOptionResponse( response ) should matchPattern {

                                    case Success( GetOptionResponseSuccess( _ ) ) if r( s ) ⇒
                                    case Success( GetOptionResponseError( _ ) ) if !r( s )  ⇒
                                    case Failure( _ ) if !r( s )                            ⇒
                                }

                            case Failure( e ) ⇒ fail( e )
                        }
                    }
            }
        }
    }
}
