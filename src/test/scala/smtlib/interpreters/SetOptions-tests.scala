/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package interpreters
package tests

import configurations.Configurations._
import solvers._
import org.scalatest.{ FunSuite, Matchers }
import scala.util.{ Try, Success, Failure }
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import parser.SMTLIB2Parser
import parser.PredefinedParsers
import scala.concurrent.TimeoutException
import resource._
import SMTSolver.SolverResource

/**
 * Check set options for the solvers
 */
class SetOptionTests extends FunSuite with TableDrivenPropertyChecks with Matchers with PredefinedParsers {

    //  use a table to range over for Boolean as if you use b: Boolean,
    //  and GeneratorDrivenPropertyChecks, same tests may be performed many times
    //  and some values may be ommitted!
    val theBools = Table[ Boolean ]( "Bool", true, false )

    val theSolvers = Table(
        "Solver",
        new Z3,
        new SMTInterpol,
        new CVC4,
        new MathSAT,
        new Yices
    )

    for ( theSolver ← theSolvers ) {
        test( s"${theSolver} (set-option :diagnostic-output-channel <string>) -- Option supported but effect not guaranteed" ) {

            for ( trySolver ← managed ( SMTSolver( theSolver ) ) ) {
                trySolver.isSuccess shouldBe true

                val solver = trySolver.get
                import solver.eval
                val r1 = eval ( SetOption( DiagnosticOutputChannel ( StringLiteral( "stderr" ) ) ) )
                r1.flatMap( parseAsGeneralResponse ) shouldBe Success( SuccessResponse() )

                val r2 = eval ( SetOption( DiagnosticOutputChannel ( StringLiteral( "stdout" ) ) ) )
                r2.flatMap( parseAsGeneralResponse ) shouldBe Success( SuccessResponse() )
            }
        }

        test( s"${theSolver} (set-option :global-declarations <bool>) -- Should yield error" ) {

            forAll( theBools ) {
                ( b : Boolean ) ⇒
                    for ( trySolver ← managed ( SMTSolver( theSolver ) ) ) {
                        trySolver.isSuccess shouldBe true

                        val solver = trySolver.get
                        import solver.eval
                        val r1 = eval ( SetOption( GlobalDeclarations ( b ) ) ) flatMap
                            parseAsGeneralResponse

                        r1 shouldBe Success( theSolver match {
                            case _ : Z3 ⇒ SuccessResponse()
                            case _      ⇒ UnsupportedResponse()
                        } )
                    }
            }
        }

        test( s"${theSolver} (set-option :interactive-mode <bool>) -- Should always succeed" ) {

            forAll( theBools ) {
                ( b : Boolean ) ⇒
                    for ( trySolver ← managed ( SMTSolver( theSolver ) ) ) {
                        trySolver.isSuccess shouldBe true

                        val solver = trySolver.get
                        import solver.eval
                        val r1 = eval ( SetOption( InteractiveMode ( b ) ) ) flatMap
                            parseAsGeneralResponse
                        r1 shouldBe Success( theSolver match {
                            case _ : MathSAT ⇒ UnsupportedResponse()
                            case _ : Yices   ⇒ UnsupportedResponse()
                            case _           ⇒ SuccessResponse()
                        } )
                    }
            }
        }

        test( s"${theSolver} (set-option :print-success true) -- Should always succeed" ) {

            forAll( theBools ) {
                ( b : Boolean ) ⇒
                    for ( trySolver ← managed ( SMTSolver( theSolver ) ) ) {
                        trySolver.isSuccess shouldBe true

                        val solver = trySolver.get
                        import solver.eval
                        import scala.concurrent.duration._

                        val r1 = eval ( SetOption( PrintSuccess ( b ) ), Some( 1.seconds ) ) flatMap
                            parseAsGeneralResponse

                        if ( b )
                            r1 shouldBe Success( SuccessResponse() )
                        else
                            r1 should matchPattern {
                                case Failure( e ) if e.isInstanceOf[ TimeoutException ] ⇒
                            }
                    }
            }
        }

        test( s"${theSolver} (set-option :produce-assertions <bool>) -- Should yield error" ) {

            forAll( theBools ) {
                ( b : Boolean ) ⇒
                    for ( trySolver ← managed ( SMTSolver( theSolver ) ) ) {
                        trySolver.isSuccess shouldBe true

                        val solver = trySolver.get
                        import solver.eval
                        val r1 = eval ( SetOption( ProduceAssertions ( b ) ) ) flatMap
                            parseAsGeneralResponse

                        r1 shouldBe Success( theSolver match {
                            case _ : Z3 ⇒ SuccessResponse()
                            case _      ⇒ UnsupportedResponse()
                        } )
                    }
            }
        }

        test( s"${theSolver} (set-option :produce-assignments <bool>) -- Should always succeed" ) {

            forAll( theBools ) {
                ( b : Boolean ) ⇒
                    for ( trySolver ← managed ( SMTSolver( theSolver ) ) ) {
                        trySolver.isSuccess shouldBe true

                        val solver = trySolver.get
                        import solver.eval
                        val r1 = eval ( SetOption( ProduceAssignments ( b ) ) ) flatMap
                            parseAsGeneralResponse

                        r1 shouldBe Success( SuccessResponse() )
                    }
            }
        }

        test( s"${theSolver} (set-option :produce-models <bool>) -- Should always succeed" ) {
            val theBools = Table[ Boolean ]( "Bool", true, false )

            forAll( theBools ) {
                ( b : Boolean ) ⇒
                    for ( trySolver ← managed ( SMTSolver( theSolver ) ) ) {
                        trySolver.isSuccess shouldBe true

                        val solver = trySolver.get
                        import solver.eval
                        val r1 = eval ( SetOption( ProduceModels ( b ) ) ) flatMap
                            parseAsGeneralResponse

                        r1 shouldBe Success( SuccessResponse() )
                    }
            }
        }

        test( s"${theSolver} (set-option :produce-unsat-assumptions <bool>) -- Should yield error" ) {

            forAll( theBools ) {
                ( b : Boolean ) ⇒
                    for ( trySolver ← managed ( SMTSolver( theSolver ) ) ) {
                        trySolver.isSuccess shouldBe true

                        val solver = trySolver.get
                        import solver.eval
                        val r1 = eval ( SetOption( ProduceUnsatAssumptions ( b ) ) )

                        r1.flatMap( parseAsGeneralResponse ) shouldBe
                            Success( theSolver match {
                                case _ : Z3 ⇒ SuccessResponse()
                                case _      ⇒ UnsupportedResponse()
                            } )
                    }
            }
        }

        test( s"${theSolver} (set-option :produce-unsat-cores <bool>) -- Should alsways succeed" ) {

            forAll( theBools ) {
                ( b : Boolean ) ⇒
                    for ( trySolver ← managed ( SMTSolver( theSolver ) ) ) {
                        trySolver.isSuccess shouldBe true

                        val solver = trySolver.get
                        import solver.eval
                        val r1 = eval ( SetOption( ProduceUnsatCores ( b ) ) )
                        r1.flatMap( parseAsGeneralResponse ) shouldBe
                            Success( theSolver match {
                                case _ : CVC4 if ( b ) ⇒ UnsupportedResponse()
                                case _ : Yices         ⇒ UnsupportedResponse()
                                case _                 ⇒ SuccessResponse()
                            } )
                    }
            }
        }

        // TODO: add max int tests ? Beyond Maxint should fail
        test( s"${theSolver} (set-option :random-seed <number>) -- Should always succeed with > 0 numbers" ) {

            val someInts = Table[ Int ]( "n", -1, -25, 12, 245, 102340, 34565566 )

            forAll( someInts ) {
                ( n : Int ) ⇒
                    for ( trySolver ← managed ( SMTSolver( theSolver ) ) ) {
                        trySolver.isSuccess shouldBe true

                        val solver = trySolver.get
                        import solver.eval
                        val r1 = eval ( SetOption( RandomSeed ( n ) ) ) flatMap
                            parseAsGeneralResponse

                        if ( n > 0 )
                            r1 shouldBe Success( theSolver match {
                                case _ : MathSAT ⇒ UnsupportedResponse()
                                case _           ⇒ SuccessResponse()

                            } )
                        else
                            r1 should matchPattern {
                                case Failure( _ ) if ( theSolver.isInstanceOf[ CVC4 ] ) ⇒
                                case Success( UnsupportedResponse() ) if ( theSolver.isInstanceOf[ MathSAT ] ) ⇒
                                case Success( ErrorResponse( _ ) ) ⇒
                            }
                    }
            }
        }

        test( s"${theSolver} (set-option :regular-output-channel <string>) -- tests pass but  we should not let the user redefine this! This feature is not provided." ) {

            for ( trySolver ← managed ( SMTSolver( theSolver ) ) ) {
                trySolver.isSuccess shouldBe true

                val solver = trySolver.get
                import solver.eval
                val r1 = eval ( SetOption( DiagnosticOutputChannel ( StringLiteral( "stderr" ) ) ) )
                r1.flatMap( parseAsGeneralResponse ) shouldBe Success( SuccessResponse() )
                val r2 = eval ( SetOption( RegularOutputChannel ( StringLiteral( "stdout" ) ) ) )
                r2.flatMap( parseAsGeneralResponse ) shouldBe Success( SuccessResponse() )
            }
        }

        test( s"${theSolver} (set-option :reproducible-resource-limit <number>) -- Should yield error" ) {

            val someInts = Table[ Int ]( "n", 12, 245, 102340, 34565566 )

            forAll( someInts ) {
                ( n : Int ) ⇒
                    for ( trySolver ← managed ( SMTSolver( theSolver ) ) ) {
                        trySolver.isSuccess shouldBe true

                        val solver = trySolver.get
                        import solver.eval
                        val r1 = eval ( SetOption( ReproducibleResourceLimit ( n ) ) ) flatMap
                            parseAsGeneralResponse
                        r1 shouldBe Success( theSolver match {
                            case _ : Z3 ⇒ SuccessResponse()
                            case _      ⇒ UnsupportedResponse()
                        } )
                    }
            }
        }

        test( s"${theSolver} (set-option :verbosity <number>) -- Should always succeed with > 0 numbers" ) {

            val someInts = Table[ Int ]( "n", 12, 245, 102340, 34565566 )

            forAll( someInts ) {
                ( n : Int ) ⇒
                    for ( trySolver ← managed ( SMTSolver( theSolver ) ) ) {
                        trySolver.isSuccess shouldBe true

                        val solver = trySolver.get
                        import solver.eval
                        val r1 = eval ( SetOption( Verbosity ( n ) ) ) flatMap
                            parseAsGeneralResponse

                        r1 shouldBe Success( SuccessResponse() )
                    }
            }
        }

        ignore( s"${theSolver} (set-option  <attribute>) -- not implemented in grammar yet" ) {

        }

    }
}
