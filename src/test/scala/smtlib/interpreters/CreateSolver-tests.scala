/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package interpreters
package tests

import solvers._
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import resource._
import SMTSolver.SolverResource

/**
 * Create a solver
 */
class CreateSolver extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Create running solver"

    val theSolvers = Table(
        "Solver",
        new Z3,
        new SMTInterpol,
        new CVC4,
        new MathSAT,
        new Yices
    )

    for ( solver ← theSolvers )

        test( s"Create $solver solver process" ) {

            for ( trySolver ← managed ( SMTSolver( solver ) ) ) {

                trySolver.isSuccess shouldBe true

            }
        }
}
