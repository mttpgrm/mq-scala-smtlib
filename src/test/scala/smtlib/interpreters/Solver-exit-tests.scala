/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package interpreters
package tests

import solvers._
import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import resource._
import SMTSolver.SolverResource
import configurations.Configurations._
import interpreters.Resources
import parser.PredefinedParsers

/**
 * Send ExitCmd() to solver. Should result in Success("Solver exit")
 *
 */
class ExitCmdTests
        extends FunSuite
        with Matchers
        with TableDrivenPropertyChecks
        with Resources
        with PredefinedParsers {

    //  logger

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    import theories.PredefinedLogics.QF_LIA

    val theSolvers = Table[ Solver ](
        "Solver", new Z3 with QF_LIA,
        new SMTInterpol with QF_LIA,
        new CVC4 with QF_LIA,
        new MathSAT with QF_LIA,
        new Yices with QF_LIA
    )

    forAll( theSolvers ) {
        ( s : Solver ) ⇒
            test( s"Send ExitCmd() to solver -- Using solver $s with using/implicit" ) {

                //  with using on solvers and flatMap
                using( s )(
                    implicit withSolver ⇒
                        eval ( Seq( ExitCmd() ) )
                ).flatMap( parseAsExitCmdResponse ) shouldBe Success ( ExitCmdResponse() )
            }
    }
}
