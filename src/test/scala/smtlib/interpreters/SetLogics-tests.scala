/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package interpreters
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import parser.SMTLIB2Parser.ParseErrorException
import parser.PredefinedParsers
import SMTSolver.SolverResource
import resource._
import solvers._

class SetLogicTests extends FunSuite with TableDrivenPropertyChecks with Matchers with PredefinedParsers {

    val theSolvers = Table(
        "Solver",
        new Z3,
        new SMTInterpol,
        new CVC4,
        new MathSAT,
        new Yices
    )

    //  test supported logics
    for ( theSolver ← theSolvers; l ← theSolver.supportedLogics ) {

        test( s"${theSolver} (set-logic $l) -- supported logic -- Should always succeed" ) {

            for ( trySolver ← managed( SMTSolver( theSolver ) ) ) {
                trySolver.isSuccess shouldBe true

                val solver = trySolver.get
                import solver.eval
                eval ( SetLogic( SSymbol ( l ) ) ) flatMap
                    parseAsGeneralResponse shouldBe
                    Success( SuccessResponse() )
            }
        }
    }

    val badLogic1 = "QFF_LIA"

    for ( theSolver ← theSolvers ) {

        test( s"${theSolver} (set-logic  $badLogic1) -- Uses an unsupported logic -- Should yield an error" ) {
            val l = badLogic1

            for ( trySolver ← managed( SMTSolver( theSolver ) ) ) {
                trySolver.isSuccess shouldBe true

                val solver = trySolver.get
                import solver.eval
                eval ( SetLogic( SSymbol ( l ) ) ) flatMap
                    parseAsGeneralResponse should matchPattern {
                        case Success( SuccessResponse() ) if theSolver.isInstanceOf[ MathSAT ] ⇒
                        case Success( ErrorResponse( _ ) ) if theSolver.isInstanceOf[ Yices ]  ⇒
                        case Failure( e ) if theSolver.isInstanceOf[ CVC4 ]                    ⇒
                        case Success( UnsupportedResponse() )                                  ⇒
                    }

                //  send another set-logic
                eval ( SetLogic( SSymbol ( l ) ) ) flatMap
                    parseAsGeneralResponse should
                    matchPattern {
                        case Success( ErrorResponse( _ ) ) if theSolver.isInstanceOf[ MathSAT ] ⇒
                        case Success( ErrorResponse( _ ) ) if theSolver.isInstanceOf[ Yices ]   ⇒
                        case Failure( e ) if theSolver.isInstanceOf[ CVC4 ]                     ⇒
                        case Success( UnsupportedResponse() )                                   ⇒
                    }

                //  send first supported logic in solver
                eval ( SetLogic( SSymbol ( theSolver.supportedLogics.head ) ) ) flatMap
                    parseAsGeneralResponse should
                    matchPattern {
                        case Success( ErrorResponse( _ ) ) if theSolver.isInstanceOf[ MathSAT ] ⇒
                        case Failure( e ) if theSolver.isInstanceOf[ CVC4 ]                     ⇒
                        case Success( SuccessResponse() )                                       ⇒
                    }
            }
        }
    }

    val badLogic2 = "123"

    for ( theSolver ← theSolvers ) {

        test( s"${theSolver} (set-logic  $badLogic2) -- Uses an unsupported logic -- Should yield an error" ) {
            val l = badLogic2

            for ( trySolver ← managed( SMTSolver( theSolver ) ) ) {
                trySolver.isSuccess shouldBe true

                val solver = trySolver.get
                import solver.eval
                val r1 = eval ( SetLogic( SSymbol ( l ) ) ) flatMap parseAsGeneralResponse

                r1 should matchPattern {
                    case Failure( _ ) if theSolver.isInstanceOf[ CVC4 ]    ⇒
                    case Failure( _ ) if theSolver.isInstanceOf[ MathSAT ] ⇒
                    case Failure( _ ) if theSolver.isInstanceOf[ Yices ]   ⇒
                    case Success( ErrorResponse( _ ) )                     ⇒
                }

                //  send another set-logic
                val r2 = eval ( SetLogic( SSymbol ( l ) ) ) flatMap parseAsGeneralResponse

                r2 should matchPattern {
                    case Failure( _ ) if theSolver.isInstanceOf[ CVC4 ]    ⇒
                    case Failure( _ ) if theSolver.isInstanceOf[ MathSAT ] ⇒
                    case Failure( _ ) if theSolver.isInstanceOf[ Yices ]   ⇒
                    case Success( ErrorResponse( _ ) )                     ⇒
                }

                //  send first supported logic in solver
                val r3 = eval ( SetLogic( SSymbol ( theSolver.supportedLogics.head ) ) )

                r3.flatMap( parseAsGeneralResponse ) should
                    matchPattern {
                        case Failure( _ ) if theSolver.isInstanceOf[ MathSAT ] ⇒
                        case Failure( _ ) if theSolver.isInstanceOf[ CVC4 ]    ⇒
                        case Failure( _ ) if theSolver.isInstanceOf[ Yices ]   ⇒
                        case Success( SuccessResponse() )                      ⇒
                    }
            }
        }
    }
}
