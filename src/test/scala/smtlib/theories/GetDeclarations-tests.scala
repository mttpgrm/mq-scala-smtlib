/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package theories
package tests

import parser.Implicits._
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

class GetDeclarationsCmdLIATests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Resources {

    override def suiteName = "Push some LIA + Arrays terms on the solver and check GetDeclCmd results"
    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Parser
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.{
        Script,
        IdTester,
        SortedQId,
        QualifiedId,
        Sort,
        SymbolId,
        SSymbol,
        IntSort,
        RealSort,
        BoolSort,
        Array1Sort,
        Array2Sort,
        Array1,
        Array2
    }
    import typedterms.TypedTerm
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  build a SortedQId term (as name type) where x._1 is the name and ._2 the sort

    val parseId = SMTLIB2Parser[ IdTester ]

    def toSortedQId( x : ( String, Sort ) ) = parseId( x._1 ) match {

        case Success( IdTester( a ) ) ⇒ SortedQId ( a, x._2 )

        case Failure( e )             ⇒ fail( e )

    }

    val pScript = SMTLIB2Parser[ Script ]

    //  format: OFF
    val theScripts = Table[ String , Map[ String, Sort ] ](
        ( "Expression" ,  "Expected declarations" ),

        ("""|
            |  (declare-fun x () Int)
         """.stripMargin,
            Map (
                    "x" -> IntSort( )
                )
        ),

        ("""    |
                |   (declare-fun   x   ()   Int)
                |   (declare-fun   y   ()   Int)
                |   (declare-fun   z   ()   Bool)
                |
         """.stripMargin,
                    Map (
                        "x" -> IntSort( ),
                        "y" -> IntSort( ),
                        "z" -> BoolSort( )
                        )
        ),

        ("""    |
                |   (declare-fun   x@1   ()   Int)
                |   (declare-fun   y_@0   ()   Int)
                |   (declare-fun   z1@20   ()   Bool)
                |
         """.stripMargin,
                    Map (
                        "x@1" -> IntSort( ),
                        "y_@0" -> IntSort( ),
                        "z1@20" -> BoolSort( )
                        )
        ),

        ("""    |
                |   (declare-fun   x    ()    Int)
                |   (declare-fun   y1   ()    Int)
                |   (declare-fun   y2   ()    Int)
                |   (declare-fun   z    ()    Int)
                |
         """.stripMargin,
                Map (
                    "x"  -> IntSort( ),
                    "y1" -> IntSort( ),
                    "y2" -> IntSort( ),
                    "z"  -> IntSort( )
                    )
        ),

        ("""    |
                |  (declare-fun x () (Array Int Int))
                |
         """.stripMargin,
                Map (
                    "x" ->
                        Array1(
                            Array1Sort ( IntSort( ) )
                        )
                    )
        ),

        ("""    |
                |  (declare-fun y () (Array Int Int))
                |
         """.stripMargin,
                Map (
                    "y" ->
                        Array1(
                                Array1Sort ( IntSort( ) )
                            )
                        )
        ),

        ("""    |
                |  (declare-fun y () (Array Int (Array Int Int) ) )
                |
         """.stripMargin,
                Map ( "y" ->
                        Array2(
                            Array2Sort (
                                Array1Sort ( IntSort() )
                            )
                        )
                    )
        ),

        ("""    |
                |  (assert true)
                |
         """.stripMargin,
                Map()
        )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ GetDeclCmd, GetDeclCmdResponse }
    import PPrinter._
    import theories.PredefinedLogics.QF_AUFLIA

    //  configure solvers with all types are arrays
    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_AUFLIA,
        new SMTInterpol with QF_AUFLIA,
        new CVC4 with QF_AUFLIA,
        new MathSAT with QF_AUFLIA,
        new Yices with QF_AUFLIA
    )

    val parserDeclCmdResponse = SMTLIB2Parser[ GetDeclCmdResponse ]

    for ( s ← theSolvers; ( x, t ) ← theScripts ) {

        test( s"Using solver $s configured with ${pprintConfig( s )} check the getDeclResponse for $x" ) {

            // parse the SMTLIB2 program to build a Script
            pScript( x ) match {

                case Failure( e ) ⇒ fail( e )

                case Success( xs ) ⇒

                    val Script( seqCmds ) = xs

                    using( SMTSolver( s ) ) {
                        implicit solver ⇒
                            {
                                //  smtlib package eval is used
                                eval( seqCmds ) match {

                                    case Success( _ ) ⇒
                                        //  check the declarations
                                        eval( Seq( GetDeclCmd() ) ) match {

                                            case Failure( f ) ⇒ fail( f )

                                            //  getDecl returns a list so we convert it to a set
                                            case Success( solverResponse ) ⇒
                                                //  parse the GteDeclResponse
                                                parserDeclCmdResponse( solverResponse ) match {
                                                    case Success(
                                                        GetDeclCmdResponse( r )
                                                        ) ⇒ r.toSet shouldBe ( t map toSortedQId ).toSet

                                                    case Failure( e ) ⇒ fail( e )
                                                }

                                            case _ ⇒ fail()
                                        }

                                    case Failure( e ) ⇒ fail( e )
                                }
                            }
                    }
            }
        }
    }
}

class GetDeclarationsCmdAUFLIATests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Resources {

    override def suiteName = "Push some terms Arrays of Bools and check GetDeclCmd results"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.{
        Script,
        SortedQId,
        QualifiedId,
        Sort,
        SymbolId,
        SSymbol,
        IntSort,
        RealSort,
        BoolSort,
        Array1Sort,
        Array2Sort,
        Array1,
        Array2
    }
    import typedterms.TypedTerm
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  build a SortedQId term (as name type) where x._1 is the name and ._2 the sort
    def toSortedQId( x : ( String, Sort ) ) = SortedQId ( SymbolId( SSymbol( x._1 ) ), x._2 )

    val pScript = SMTLIB2Parser[ Script ]

    //  format: OFF
    val theScripts = Table[ String , Map[ String, Sort ] ](
        ( "Expression" ,  "Expected declarations" ),

        ("""    |
                |  (declare-fun y () (Array Int Bool ) )
                |
         """.stripMargin,
                Map ( "y" ->
                        Array1(
                                Array1Sort ( BoolSort() )
                        )
                    )
        ),

        ("""    |
                |  (declare-fun y () (Array Int (Array Int Bool) ) )
                |
         """.stripMargin,
                Map ( "y" ->
                        Array2(
                            Array2Sort (
                                Array1Sort ( BoolSort() )
                            )
                        )
                    )
        )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ GetDeclCmd, GetDeclCmdResponse }
    import theories.tests.PPrinter._
    import theories.PredefinedLogics.AUFNIRA

    //  configure solvers with all types are arrays
    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with AUFNIRA,
        new CVC4 with AUFNIRA
    )

    val parserDeclCmdResponse = SMTLIB2Parser[ GetDeclCmdResponse ]

    for ( s ← theSolvers; ( x, t ) ← theScripts ) {

        test( s"Using solver $s configured with ${pprintConfig( s )} check the getDeclResponse for $x" ) {

            // parse the SMTLIB2 program to build a Script
            pScript( x ) match {

                case Failure( e ) ⇒ fail( e )

                case Success( xs ) ⇒
                    val Script( seqCmds ) = xs

                    using( SMTSolver( s ) ) {
                        implicit solver ⇒
                            {
                                //  smtlib package eval is used
                                eval( seqCmds ) match {

                                    case Success( _ ) ⇒
                                        //  check the declarations
                                        eval( Seq( GetDeclCmd() ) ) match {

                                            case Failure( f ) ⇒ fail( f )

                                            //  getDecl returns a list so we convert it to a set
                                            case Success( solverResponse ) ⇒
                                                //  parse the GteDeclResponse
                                                parserDeclCmdResponse( solverResponse ) match {
                                                    case Success(
                                                        GetDeclCmdResponse( r )
                                                        ) ⇒ r.toSet shouldBe ( t map toSortedQId ).toSet

                                                    case Failure( e ) ⇒ fail( e )
                                                }

                                            case _ ⇒ fail()
                                        }

                                    case Failure( e ) ⇒ fail( e )
                                }
                            }
                    }
            }
        }
    }
}

class GetDeclarationsCmdArrayBoolRealTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Resources {

    override def suiteName = "Push some terms Arrays of Realsr and check GetDeclCmd results"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.{
        Script,
        SortedQId,
        QualifiedId,
        Sort,
        SymbolId,
        SSymbol,
        IntSort,
        RealSort,
        BoolSort,
        Array1Sort,
        Array2Sort,
        Array1,
        Array2
    }
    import typedterms.TypedTerm
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  build a SortedQId term (as name type) where x._1 is the name and ._2 the sort
    def toSortedQId( x : ( String, Sort ) ) = SortedQId ( SymbolId( SSymbol( x._1 ) ), x._2 )

    val pScript = SMTLIB2Parser[ Script ]

    //  format: OFF
    val theScripts = Table[ String , Map[ String, Sort ] ](
        ( "Expression" ,  "Expected declarations" ),

        ("""    |
                |  (declare-fun y () (Array Int Real ) )
                |
        """.stripMargin,
            Map ( "y" ->
                    Array1(
                            Array1Sort ( RealSort() )
                    )
                )
        ),

        ("""    |
                |  (declare-fun y () (Array Int (Array Int Real) ) )
                |
         """.stripMargin,
                Map ( "y" ->
                        Array2(
                            Array2Sort (
                                Array1Sort ( RealSort() )
                            )
                        )
                    )
        )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ GetDeclCmd, GetDeclCmdResponse }
    import theories.tests.PPrinter._
    import theories.PredefinedLogics.AUFNIRA

    //  configure solvers with all types are arrays
    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with AUFNIRA,
        new CVC4 with AUFNIRA,
        new MathSAT with AUFNIRA
    )

    val parserDeclCmdResponse = SMTLIB2Parser[ GetDeclCmdResponse ]

    for ( s ← theSolvers; ( x, t ) ← theScripts ) {

        test( s"Using solver $s configured with ${pprintConfig( s )} check the getDeclResponse for $x" ) {

            // parse the SMTLIB2 program to build a Script
            pScript( x ) match {

                case Failure( e ) ⇒ fail( e )

                case Success( xs ) ⇒
                    val Script( seqCmds ) = xs
                    //  with using
                    using( SMTSolver( s ) ) {
                        implicit solver ⇒
                            {
                                //  smtlib package eval is used
                                eval( seqCmds ) match {

                                    case Success( _ ) ⇒
                                        //  check the declarations
                                        eval( Seq( GetDeclCmd() ) ) match {

                                            case Failure( f ) ⇒ fail( f )

                                            //  getDecl returns a list so we convert it to a set
                                            case Success( solverResponse ) ⇒
                                                //  parse the GteDeclResponse
                                                parserDeclCmdResponse( solverResponse ) match {
                                                    case Success(
                                                        GetDeclCmdResponse( r )
                                                        ) ⇒ r.toSet shouldBe ( t map toSortedQId ).toSet

                                                    case Failure( e ) ⇒ fail( e )
                                                }

                                            case _ ⇒ fail()
                                        }

                                    case Failure( e ) ⇒ fail( e )
                                }
                            }
                    }
            }
        }
    }
}
