/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package theories
package tests

import parser.Implicits._
import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

object PPrinter {
    import solvers._
    import parser.SMTLIB2PrettyPrinter.format

    def pprintConfig( s : Solver ) =
        s.configScript map ( l ⇒ format( l ).layout ) mkString ( "\n\t", "\n\t", "\n" )

}

/**
 * test the assert command on typed terms
 *
 * This command should allow assertions of typedterms and manage variable declarations
 * on-the-fly.
 */
class AssertCommandTests extends FunSuite with TableDrivenPropertyChecks with Matchers
        with Core with IntegerArithmetics with Commands with Resources {

    override def suiteName = "Send assert command and check Declared variables stack"

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import scala.util.Success
    import parser.SMTLIB2Syntax.{
        Sat,
        UnSat,
        UnKnown,
        SatResponses
    }
    import PPrinter._
    import theories.PredefinedLogics.QF_LIA

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_LIA,
        new SMTInterpol with QF_LIA,
        new CVC4 with QF_LIA,
        new MathSAT with QF_LIA,
        new Yices with QF_LIA
    )

    for ( s ← theSolvers ) {

        test( s"Using solver $s configured with ${pprintConfig( s )} send simple assert script with multiple check-sat commands" ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        val x = Ints( "x" )
                        val y = Ints( "y" )

                        //  assert first term
                        val res1 = |= ( x <= 4 )
                        //  assert another term
                        val res2 = |= ( x >= 1 )

                        //  checkSat
                        isSat() shouldBe Success( Sat() )

                        isSat() shouldBe Success( Sat() )

                        // assert a new term
                        val res3 = |= ( y < x + 1 )

                        checkSat() shouldBe Success( Sat() )
                        checkSat() shouldBe Success( Sat() )

                        // assert a new term that makes the solver stack UnSat
                        isSat( y >= 6 ) shouldBe Success( UnSat() )
                        checkSat() shouldBe Success( UnSat() )
                    }
            }
        }
    }
}
