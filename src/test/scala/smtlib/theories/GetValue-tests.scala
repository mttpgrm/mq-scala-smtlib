/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package theories
package tests

import parser.Implicits._
import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

class IntsGetValueTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Commands with Resources {

    override def suiteName = "Check sat and get interpolants for  LIA terms"

    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.Term
    import typedterms.{ TypedTerm, Value }
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ], List[TypedTerm[IntTerm, Term]] ](
        ( "expression"         , "TypedTerm"       , "Values needed") ,
        ( "integer variable x" , x  === y          , List ( x ) )         ,
        ( "-x"                 , -x  === 1         , List ( x ) )         ,
        ( "x + y"              , x + y  === 0      , List (x , y ) )      ,
        ( "x + y = z"          , x + y  === z      , List (x , y , z ) )  ,
        ( "2 * x = z"          , x * 2  === z      , List (x , z ) )      ,
        ( "1 + 2 + x"          , x + 1 + 2  === -4 , List ( x ) )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2Syntax.{ Sat, UnSat }
    import theories.PredefinedLogics.QF_LIA
    import PPrinter._

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_LIA with Models,
        new SMTInterpol with QF_LIA with Models,
        new CVC4 with QF_LIA with Models,
        new MathSAT with QF_LIA with Models,
        new Yices with QF_LIA with Models
    )

    for ( s ← theSolvers; ( _, t, xv ) ← theTerms ) {

        test( s"Using solver $s configured with ${pprintConfig( s )} to get values for term ${format( t.termDef ).layout} === 1 " ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( t ) should matchPattern {
                            case Success( Sat() ) ⇒
                        }

                        val r : List[ Try[ Value ] ] = xv map getValue
                        logger.info( "Get-model returned: {}", r )

                        r.foreach( _ should matchPattern {
                            case Success( Value( _ ) ) ⇒
                        } )

                    }
            }
        }
    }
}

class RealsGetValueTests extends FunSuite with TableDrivenPropertyChecks with Matchers with RealArithmetics with Core with Commands with Resources {

    override def suiteName = "Check sat and get value for some LRA terms"

    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.Term
    import typedterms.{ TypedTerm, Value }
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    val x = Reals( "x" )
    val y = Reals( "y" )
    val z = Reals( "z" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ], List[TypedTerm[RealTerm, Term]] ](
        ( "expression"         , "TypedTerm"              , "Values needed")     ,
        ( "integer variable x" , x  === y                 , List ( x , y ) )     ,
        ( "-x"                 , -x  === 1                , List ( x ) )         ,
        ( "x + y"              , x + y  === 0             , List ( x , y ) )     ,
        ( "x + y = z"          , x + y  === z             , List ( x , y , z ) ) ,
        ( "2 * x = z"          , x * 2.0  === z           , List ( x , z ) )     ,
        ( "1 + 2 + x"          , x + 1.0 + 2.4  === -4.66 , List(x))
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2Syntax.{ Sat, UnSat }
    import theories.PredefinedLogics.QF_LRA
    import PPrinter._

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_LRA with Models,
        new SMTInterpol with QF_LRA with Models,
        new CVC4 with QF_LRA with Models,
        new MathSAT with QF_LRA with Models,
        new Yices with QF_LRA with Models
    )

    for ( s ← theSolvers; ( _, t, xv ) ← theTerms ) {

        test( s"Using solver $s configured with ${pprintConfig( s )} to get values for term ${format( t.termDef ).layout} === 1 " ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( t ) should matchPattern {
                            case Success( Sat() ) ⇒
                        }

                        val r : List[ Try[ Value ] ] = xv map getValue
                        logger.info( "Get-model returned: {}", r )

                        r.foreach( _ should matchPattern {
                            case Success( Value( _ ) ) ⇒
                        } )
                    }
            }
        }
    }
}

class ArrayGetValueTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with ArrayExInt with ArrayExBool with ArrayExOperators with Core with Commands with Resources {

    override def suiteName = "Check sat and get value for some Array terms"

    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.Term
    import typedterms.{ TypedTerm, Value, VarTerm }
    import parser.SMTLIB2Syntax.CheckSatResponses
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import scala.util.Success
    import parser.SMTLIB2Syntax.{ Sat, UnSat, QIdTerm }
    import PPrinter._
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val a1 = ArrayInt1( "a1" )

    //  format: OFF
    val theIntTerms = Table[ String, TypedTerm[ BoolTerm, Term ], List[TypedTerm[IntTerm, Term]] ](
        ( "expression"         , "TypedTerm",                       "terms needed")           ,
        ( "integer variable x" , a1(1) === y                        , List ( a1(1) ) )        ,
        ( "integer variable x" , (a1(1) === y) & (a1(2) === y - 1)  , List ( a1(1), a1(2)  ) )
    )
    //  format: ON

    //  format: OFF
    val theArrayTerms = Table[ String, TypedTerm[ BoolTerm, Term ], List[TypedTerm[ArrayTerm[IntTerm], Term]]] (
        ( "expression"         , "TypedTerm",                       "terms needed")           ,
        ( "integer variable x" , (a1(1) === y) & (a1(2) === x - 1)  , List ( a1 ) )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2Syntax.{ Sat, UnSat }
    import theories.PredefinedLogics.QF_AUFLIA
    import PPrinter._

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_AUFLIA with Models,
        new SMTInterpol with QF_AUFLIA with Models,
        new CVC4 with QF_AUFLIA with Models,
        new MathSAT with QF_AUFLIA with Models,
        new Yices with QF_AUFLIA with Models
    )

    for ( s ← theSolvers; ( _, t, xv ) ← theIntTerms ) {

        test( s"Using solver $s configured with ${pprintConfig( s )} to get to get values for term ${format( t.termDef ).layout} === 1" ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( t ) should matchPattern {
                            case Success( Sat() ) ⇒
                        }

                        val r : List[ Try[ Value ] ] = xv map getValue
                        logger.info( "Get-model returned: {}", r )

                        r.foreach( _ should matchPattern {
                            case Success( Value( _ ) ) ⇒
                        } )

                        logger.info(
                            "values: {}",
                            ( r map {
                                case Success( Value( v ) ) ⇒ format( v ).layout
                                case Failure( e )          ⇒ fail( e )
                            } ).mkString( "\n" )
                        )
                    }
            }
        }
    }

    for ( s ← theSolvers; ( _, t, xv ) ← theArrayTerms ) {

        test( s"Using solver $s configured with ${pprintConfig( s )} to get to get values for term ${format( t.termDef ).layout} === 1" ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( t ) should matchPattern {
                            case Success( Sat() ) ⇒
                        }

                        val r : List[ Try[ Value ] ] = xv map getValue
                        logger.info( "Get-model returned: {}", r )

                        r.foreach( _ should matchPattern {
                            case Success( Value( _ ) ) ⇒
                        } )

                        logger.info(
                            "values: {}",
                            ( r map {
                                case Success( Value( v ) ) ⇒ format( v ).layout
                                case Failure( e )          ⇒ fail( e )
                            } ).mkString( "\n" )
                        )
                    }
            }
        }
    }
}
