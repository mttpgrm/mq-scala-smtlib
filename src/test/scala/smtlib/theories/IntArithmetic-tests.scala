/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package theories
package tests

import parser.Implicits._
import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

class IntsComparisonTests extends FunSuite with TableDrivenPropertyChecks with Matchers
        with Core with IntegerArithmetics with Resources {

    override def suiteName = "Check the termDef of integer arithmetic comparison"

    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm

    val x = Ints( "x" )
    val y = Ints( "y" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ], String ](
        ( "expression" , "TypedTerm"           , "Solver string" )    ,
        ( "x <= y"     , x <= y                , "(<= x y ) " )       ,
        ( "1 <= y"     , Ints(1) <= y          , "(<= 1 y ) " )       ,
        ( "x <= 1"     , x <= 1                , "(<= x 1 ) " )       ,
        ( "x < y"      , x < y                 , "(< x y ) " )        ,
        ( "x > y"      , x > y                 , "(> x y ) " )        ,
        ( "x >= y"     , x >= y                , "(>= x y ) " )       ,
        ( "x == y"     , x === y               , "(= x y ) " )        ,
        ( "x != y"     , x =/= y               , "(distinct x y ) " ) ,
        ( "1 == 0"     , Ints(1) === Ints(0)   , "(= 1 0 ) " )        ,
        ( "1 != 0"     , Ints(1) =/= Ints(0)   , "(distinct 1 0 ) " ) ,
        ( "1 == x"     , Ints(1) === x         , "(= 1 x ) " )        ,
        ( "1 != x"     , Ints(1) =/= x         , "(distinct 1 x ) " ) ,
        ( "0 < 1"      , Ints( 0 ) < Ints( 1 ) , "(< 0 1 ) " )
    )
    //  format: ON

    for ( ( x, t, s ) ← theTerms ) {
        test( s"Check string produced by: $x -- Should be $s" ) {
            format( t.termDef ).layout shouldBe s
        }
    }
}

class LinearIntsArithmeticTests extends FunSuite with TableDrivenPropertyChecks with Matchers
        with Core with IntegerArithmetics with Commands with Resources {

    override def suiteName = "Check the termDef and solvers' compatibility for LIA terms"

    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    //  format: OFF
    val theLIATerms = Table[ String, TypedTerm[ IntTerm, Term ], String ](
        ( "expression"          , "TypedTerm"           , "Solver string" )             ,
        ( "integer 0"           , Ints( 0 )             , "0 " )                        ,
        ( "1 + 2"               , Ints( 1 ) + Ints( 2 ) , "(+ 1 2 ) " )                 ,
        ( "integer variable x"  , x                     , "x " )                        ,
        ( "indexed variable x"  , x indexed 1           , "x@1 " )                      ,
        ( "-x"                  , -x                    , "(- x ) " )                   ,
        ( "x + y"               , x + y                 , "(+ x y ) " )                 ,
        ( "x - y + z"           , x - y + z             , "(+ (- x y ) z ) " )          ,
        ( "x + y + z"           , x + y + z             , "(+ (+ x y ) z ) " )          ,
        ( "x + y + z + 1"       , x + y + z + 1         , "(+ (+ (+ x y ) z ) 1 ) " )   ,
        ( "(x + y) + (z + 1)"   , ( x + y ) + ( z + 1 ) , "(+ (+ x y ) (+ z 1 ) ) " )   ,
        ( "x - ( x + z )"       , x - ( y + z )         , "(- x (+ y z ) ) " )          ,
        ( "abs(x)"              , absI(x)               , "(abs x ) " )
    )

    //  modulus and div partially supported (CVC4 does not support them)
    val theModDivTerms = Table[ String, TypedTerm[ IntTerm, Term ], String ](
        ( "expression"          , "TypedTerm"           , "Solver string" )      ,
        ( "x % 3"               , x % 3                 , "(mod x 3 ) " )        ,
        ( "x_3 % 3"             , (x indexed 3) % 3     , "(mod x@3 3 ) " )      ,
        ( "x / 5"               , x / 5                 , "(div x 5 ) " )
    )
    //  format: ON

    //  check the strings produced by the prettyPrinter
    for ( ( x, t, s ) ← theLIATerms ++ theModDivTerms ) {
        test( s"Check string produced by: $x -- Should be $s" ) {
            format( t.termDef ).layout shouldBe s
        }
    }

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import scala.util.Success
    import parser.SMTLIB2Syntax.{ Sat, UnSat, UnKnown }
    import PPrinter._
    import theories.PredefinedLogics.QF_LIA

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_LIA,
        new SMTInterpol with QF_LIA,
        new CVC4 with QF_LIA,
        new MathSAT with QF_LIA,
        new Yices with QF_LIA
    )

    for ( s ← theSolvers; ( x, t, _ ) ← theLIATerms ) {

        test( s"Using solver $s configured with ${pprintConfig( s )} to check that check-sat for term ${format( t.termDef ).layout} === 1 can be established" ) {

            //  with using (monadic)
            using( s ) {
                implicit withSolver ⇒
                    isSat( t === 1 )
            } should matchPattern {
                case Success( Sat() )   ⇒
                case Success( UnSat() ) ⇒
            }
        }
    }

    //  except CVC4 for Mod/Div terms
    for ( s ← theSolvers; ( x, t, _ ) ← theModDivTerms ) {

        if ( s.isInstanceOf[ CVC4 ] )
            ignore( s"Using solver CVC4configured with QF_LIA to check that check-sat for term ${format( t.termDef ).layout} === 1 -- operator not supported by CVC4" ) {

                //  with using
                using( s ) {
                    implicit withSolver ⇒
                        isSat( t === 1 )
                } should matchPattern {
                    case Success( Sat() )   ⇒
                    case Success( UnSat() ) ⇒
                }
            }
        else
            test( s"Using solver $s configured with ${pprintConfig( s )} to check that check-sat for term ${format( t.termDef ).layout} === 1 can be established" ) {

                //  with using
                using( s ) {
                    implicit withSolver ⇒
                        isSat( t === 1 )
                } should matchPattern {
                    case Success( Sat() )   ⇒
                    case Success( UnSat() ) ⇒
                }
            }
    }

    test( s"Using solver Z3 configured with QF_LIA to check that check-sat for term x rem 6 === 1 can be established" ) {

        //  with using
        using( new Z3 with QF_LIA ) {
            implicit withSolver ⇒
                isSat( ( x rem 6 ) === 1 )
        } should matchPattern {
            case Success( Sat() ) ⇒
        }
    }

}

class IntsFactoryTypeTests extends FunSuite with TableDrivenPropertyChecks with Matchers
        with Core with IntegerArithmetics {

    override def suiteName = "Check the typeDefs of Integer Arithmetics terms"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax.{ QualifiedId, SortedQId }
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm

    //  get a SortedQId parser
    //  as SotedQId is a rule of QualifiedId we get QualifiedId parser first
    val p1 = SMTLIB2Parser[ QualifiedId ]
    val sortedIdParser : String ⇒ SortedQId = { x ⇒ p1( x ).get.asInstanceOf[ SortedQId ] }

    //  create a solver SortedQId, type Ints, from a string
    def toSortedQId( name : String ) : SortedQId = sortedIdParser( s"(as $name Int)" )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val y1 = y indexed 1

    //  format: OFF
    val theTerms1 = Table[ String, TypedTerm[ IntTerm, Term ], Set[ SortedQId ] ](
        ( "Expression" , "TypedTerm"       , "typeDefs" )                          ,
        ( "0 - 1"      , Ints( 0 ) - 1     , Set() )                               ,
        ( "- 1"        , -Ints( 1 )        , Set() )                               ,
        ( "2 * 1"      , Ints( 2 ) * 1     , Set() )                               ,
        ( "2 / 1"      , Ints( 2 ) / 1     , Set() )                               ,
        ( "0 + 2"      , Ints( 0 ) + 2     , Set() )                               ,
        ( "x"          , x                 , Set( "x" ).map( toSortedQId ) )       ,
        ( "y1"         , y1                , Set( "y@1" ).map( toSortedQId ) )     ,
        ( "x + y"      , x + y             , Set( "x", "y" ).map( toSortedQId ) )  ,
        ( "x - y"      , x - y             , Set( "x", "y" ).map( toSortedQId ) )  ,
        ( "x + y + 0"  , x + y + Ints( 0 ) , Set( "x", "y" ).map( toSortedQId ) )  ,
        ( "0 + x + y"  , Ints( 0 ) + x + y , Set( "x", "y" ).map( toSortedQId ) )  ,
        ( "x / 2"      , x / 2             , Set( "x" ).map( toSortedQId ) )       ,
        ( "x * y"      , x * y1            , Set( "x", "y@1" ).map( toSortedQId ) ),
        ( "x + y / 2"  , x + y / 2         , Set( "x", "y" ).map( toSortedQId ) )  ,
        ( "x + 2 / y"  , (x + 2) / y       , Set( "x", "y" ).map( toSortedQId ) )  ,
        ( "x + 2 % y"  , (x + 2) % y       , Set( "x", "y" ).map( toSortedQId ) )  ,
        ( "x + y % 2"  , x + y % 2         , Set( "x", "y" ).map( toSortedQId ) )
    )
    //  format: ON

    //  format: OFF
    val theTerms2 = Table[ String, TypedTerm[ BoolTerm, Term ], Set[ SortedQId ] ](
        ( "Expression" , "TypedTerm"     , "typeDefs" )                    ,

        ( "1 < y"      , Ints( 1 ) < y   , Set( "y" ).map( toSortedQId ) ) ,
        ( "1 <= y"     , Ints( 1 ) <= y  , Set( "y" ).map( toSortedQId ) ) ,
        ( "1 > y"      , Ints( 1 ) > y   , Set( "y" ).map( toSortedQId ) ) ,
        ( "1 >= y"     , Ints( 1 ) >= y  , Set( "y" ).map( toSortedQId ) ) ,
        ( "1 === y"    , Ints( 1 ) === y , Set( "y" ).map( toSortedQId ) ) ,

        ( "y < 1"      , y < Ints( 1 )   , Set( "y" ).map( toSortedQId ) ) ,
        ( "y <= 1"     , y <= Ints( 1 )  , Set( "y" ).map( toSortedQId ) ) ,
        ( "y > 1"      , y > Ints( 1 )   , Set( "y" ).map( toSortedQId ) ) ,
        ( "y >= 1"     , y >= Ints( 1 )  , Set( "y" ).map( toSortedQId ) ) ,
        ( "y === 1"    , y === Ints( 1 ) , Set( "y" ).map( toSortedQId ) ) ,
        ( "y =/= 1"    , y =/= Ints( 1 ) , Set( "y" ).map( toSortedQId ) ) ,

        ( "x < y"      , x < y           , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x <= y"     , x <= y          , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x > y"      , x > y           , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x >= y"     , x >= y          , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x === y"    , x === y         , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x =/= y"    , x =/= y         , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "0 < 1"      , Ints( 0 ) < 1   , Set() )
    )
    //  format: ON

    for ( ( e, t, tdef ) ← theTerms1 ++ theTerms2 ) {
        val result = tdef.map( x ⇒ format( x ).layout )
        test( s"Check the typeDefs built with for term $e -- Should be $result" ) {
            t.typeDefs shouldBe tdef
        }
    }
}
