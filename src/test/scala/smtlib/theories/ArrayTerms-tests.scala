/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package theories
package tests

import org.scalatest.{ FunSuite, Matchers, NonImplicitAssertions }
import org.scalatest.prop.TableDrivenPropertyChecks
import typedterms.Commands
import interpreters.Resources

/**
 * Check sat for array terms
 */
class ArraySatTests
        extends FunSuite
        with TableDrivenPropertyChecks
        with Matchers
        with Core
        with IntegerArithmetics
        with ArrayExInt
        with ArrayExBool
        with ArrayExReal
        with ArrayExOperators
        with Commands
        with Resources {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    override def suiteName = s"Check sat for simple assertions with arrays"

    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm
    import parser.SMTLIB2Syntax.CheckSatResponses
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import scala.util.Success
    import parser.SMTLIB2Syntax.{ Sat, UnSat, SatResponses }
    import PPrinter._
    import theories.PredefinedLogics._

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_AUFLIA with Models,
        new SMTInterpol with QF_AUFLIA with Models,
        new CVC4 with QF_AUFLIA with Models,
        new MathSAT with QF_AUFLIA with Models,
        new Yices with QF_AUFLIA with Models
    )
    val a1 = ArrayInt1( "a" )
    val a1_1 = ArrayInt1( "a" ) indexed 0
    val b1 = ArrayInt1( "b" )
    val a2 = ArrayInt2( "a" )
    val a2_9 = ArrayInt2( "a" ) indexed 9

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ],    SatResponses ](
        ( "expression"              , "TypedTerm"                 , "Expected status" ) ,
        ( "a[0] == 1"               , a1(0) === 1                 , Sat() )             ,
        ( "a_0[0] == 1"             , a1_1(0) === 1               , Sat() )             ,
        ( "a[0] == 1 & a[0] > 2"    , a1(0) === 1 & a1(0) > 2     , UnSat() )           ,
        ( "a[0] == 1 & b[0] > a[0]" , a1.at(0) === 1 & a1(0) > 2  , UnSat() )           ,
        ( "a[0] == b & b[0] <= 1"   , a2(0) === b1 & b1(0) <= 2   , Sat() )             ,
        ( "a_9[0] == b & b[0] <= 1" , a2_9(0) === b1 & b1(0) <= 2 , Sat() )             ,
        ( "a[0] == b & b[0] <= 2"   , a2(0) === b1 & b1(0) <=  2  , Sat() )
    )
    //  format: ON

    for ( s ← theSolvers; ( txt, t, r ) ← theTerms ) {

        test( s"Using solver $s configured with ${pprintConfig( s )} to check sat for $txt " ) {
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( t ) should matchPattern {
                            case Success( r ) ⇒
                        }
                    }
            }
        }
    }
}

/**
 * Check sat for array terms: if sat get a value for the arrays
 */
class ArrayValueTests
        extends FunSuite
        with TableDrivenPropertyChecks
        with Matchers
        with Core
        with IntegerArithmetics
        with ArrayExInt
        with ArrayExBool
        with ArrayExReal
        with ArrayExOperators
        with Commands
        with Resources {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    override def suiteName = s"Check sat for simple assertions with arrays"

    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm
    import parser.SMTLIB2Syntax.CheckSatResponses
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import scala.util.Success
    import parser.SMTLIB2Syntax.{ Sat, UnSat, SatResponses }
    import typedterms.VarTerm
    import PPrinter._
    import theories.PredefinedLogics._

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_AUFLIA with Models,
        new SMTInterpol with QF_AUFLIA with Models,
        new CVC4 with QF_AUFLIA with Models,
        new MathSAT with QF_AUFLIA with Models,
        new Yices with QF_AUFLIA with Models
    )
    //  dimension 1 arrays
    val a1 = ArrayInt1( "a1" )
    val a1_1 = ArrayInt1( "a1" ) indexed 0
    val b1 = ArrayInt1( "b1" )
    //  dimension 2 arrays
    val a2 = ArrayInt2( "a2" )
    val b2 = ArrayInt2( "b2" )
    val a2_9 = ArrayInt2( "a2" ) indexed 9

    //  format: OFF
    val theTerms1 = Table[ String, TypedTerm[ BoolTerm, Term ],    List[VarTerm[ArrayTerm[IntTerm]]] ](
        ( "expression"              , "TypedTerm"                 , "Values" ) ,
        ( "a[0] == 1"               , a1(0) === 1                 , List(a1) )             ,
        ( "a_0[0] == 1"             , a1_1(0) === 1               , List(a1_1) )             ,
        ( "a[0] == b[1] & b[0] <= 1"   , a1(0) === b1(1) & b1(0) <= 2   , List(a1,b1) )
    )

    val theTerms2 = Table[ String, TypedTerm[ BoolTerm, Term ],    List[VarTerm[ArrayTerm[ArrayTerm[IntTerm]]]] ](
        ( "expression"              , "TypedTerm"                 , "Values" ) ,
        ( "a_9[0] == b & b[0] <= 1" , a2_9(0) === b1 & b1(0) <= 2 , List(a2_9) )             ,
        ( "a[0] == b & b[0] <= 2"   , a2(0) === b2(1) & b2(1)(0) <=  2  , List(a2,b2) )
    )
    //  format: ON

    for ( s ← theSolvers; ( txt, t, xr ) ← theTerms1 ++ theTerms2 ) {

        test( s"Using solver $s configured with ${pprintConfig( s )} to check sat for $txt " ) {
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( t ) shouldBe Success( Sat() )

                        //  get a value for each varriables in list xr
                        for ( v ← xr ) {
                            val witness = getValue( v )
                            logger.info( s"[$s] Value of {} is: {}", v.symbol, witness.get.show )
                        }
                    }
            }
        }
    }
}
