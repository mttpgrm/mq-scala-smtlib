/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package theories
package tests

import parser.Implicits._
import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

class RealComparisonTests extends FunSuite with TableDrivenPropertyChecks with Matchers with Core with RealArithmetics with Commands with Resources {

    override def suiteName = "Check the termDef built using the comparison operators on RealSort TypedTerms"

    import parser.SMTLIB2PrettyPrinter.format
    import theories.BoolTerm
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm

    val x = Reals( "x" )
    val y = Reals( "y" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ], String ](
        ( "expression" , "TypedTerm"                 , "Solver string" )      ,
        ( "x <= y"     , x <= y                      , "(<= x y ) " )         ,
        ( "x < y"      , x < y                       , "(< x y ) " )          ,
        ( "x > y"      , x > y                       , "(> x y ) " )          ,
        ( "x >= y"     , x >= y                      , "(>= x y ) " )         ,
        ( "x == y"     , x === y                     , "(= x y ) " )          ,
        ( "x != y"     , x =/= y                     , "(distinct x y ) " )   ,
        ( "x != 8.5"   , x =/= 8.5                   , "(distinct x 8.5 ) " ) ,
        ( "0 < 1.5"    , Reals( 0.0 ) < Reals( 1.5 ) , "(< 0 1.5 ) " )
    )
    //  format: ON

    for ( ( x, t, s ) ← theTerms ) {
        test( s"Check string produced by: $x -- Should be $s" ) {
            format( t.termDef ).layout shouldBe s
        }
    }
}

class LinearRealArithmeticTests extends FunSuite with TableDrivenPropertyChecks with Matchers with Core with RealArithmetics with Commands with Resources {

    override def suiteName = "Check the termDef and solvers' compatibility for LRA terms"

    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm

    val x = Reals( "x" )
    val y = Reals( "y" )
    val z = Reals( "z" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ RealTerm, Term ], String ](
        ( "expression"            , "TypedTerm"                 , "Solver string" )            ,
        ( "real number 0.0"       , Reals( 0.0 )                , "0 " )                       ,
        ( "real number 1.5"       , Reals( 1.5 )                , "1.5 " )                     ,
        ( "real number 0.0000001" , Reals( 0.0000001 )          , "0.0000000999999999999999954748111825886258685613938723690807819366455078125 " ),
        ( "1.0 + 2.0"             , Reals( 1.0 ) + Reals( 2.0 ) , "(+ 1 2 ) " )                ,
        ( "3 + 2.0"               , Reals( 3 ) + Reals( 2.0 )   , "(+ 3 2 ) " )                ,
        ( "integer variable x"    , x                           , "x " )                       ,
        ( "-x"                    , -x                          , "(- x ) " )                  ,
        ( "-x_1"                  , -(x indexed 1)              , "(- x@1 ) " )                ,
        ( "x + y"                 , x + y                       , "(+ x y ) " )                ,
        ( "x + y + z"             , x + y + z                   , "(+ (+ x y ) z ) " )         ,
        ( "x + y_3 + z"           , x + (y indexed 3) + z       , "(+ (+ x y@3 ) z ) " )       ,
        ( "x + y + z + 1.5"       , x + y + z + 1.5             , "(+ (+ (+ x y ) z ) 1.5 ) ") ,
        ( "x - y + z"             , x - y + z                   , "(+ (- x y ) z ) " )         ,
        ( "x - ( x + z )"         , x - ( y + z )               , "(- x (+ y z ) ) " )
    )
    //  format: ON

    //  check the strings produced by the prettyPrinter
    for ( ( x, t, s ) ← theTerms ) {
        test( s"Real arithmetic. Check string produced by: $x -- Should be $s" ) {
            format( t.termDef ).layout shouldBe s
        }
    }

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import scala.util.Success
    import parser.SMTLIB2Syntax.{ Sat, UnSat }
    import PPrinter._
    import theories.PredefinedLogics.QF_LRA

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_LRA,
        new SMTInterpol with QF_LRA,
        new CVC4 with QF_LRA,
        new MathSAT with QF_LRA,
        new Yices with QF_LRA
    )

    /*
    for ( s ← theSolvers; ( x, t, _ ) ← theTerms ) {

        test( s"Using solver $s configured with ${pprintConfig( s )} to check that check-sat for term ${format( t.termDef ).layout} === 1.0 can be established" ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( t === 1.0 ) should matchPattern {
                            case Success( Sat() )   ⇒
                            case Success( UnSat() ) ⇒
                        }
                    }
            }
        }
    }
*/
}

class RealFactoryTypeTests extends FunSuite with TableDrivenPropertyChecks with Matchers
        with Core with RealArithmetics with Commands with Resources {

    override def suiteName = "Check the typeDefs of real arithmetic terms"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax.{ QualifiedId, SortedQId }
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm

    //  get a SortedQId parser
    //  as SotedQId is a rule of QualifiedId we get QualifiedId parser first
    val p1 = SMTLIB2Parser[ QualifiedId ]
    val sortedIdParser : String ⇒ SortedQId = { x ⇒ p1( x ).get.asInstanceOf[ SortedQId ] }

    //  create a solver SortedQId, type Int, from a string
    def toSortedQId( name : String ) : SortedQId = sortedIdParser( s"(as $name Real)" )

    val x = Reals( "x" )
    val y = Reals( "y" )

    //  format: OFF
    val theTerms1 = Table[ String, TypedTerm[ RealTerm, Term ], Set[ SortedQId ] ](
        ( "Expression"  , "TypedTerm"          , "typeDefs" )                           ,
        ( "0"           , Reals( 0 )           , Set() )                                ,
        ( "-1.0"        , Reals( -1.0 )        , Set() )                                ,
        ( "x"           , x                    , Set( "x" ).map( toSortedQId ) )        ,
        ( "x + y"       , x + y                , Set( "x" , "y" ).map( toSortedQId ) )  ,
        ( "x - y_1"     , x - (y indexed 1)    , Set( "x" , "y@1" ).map( toSortedQId ) ),
        ( "x + y + 0.0" , x + y + Reals( 0.0 ) , Set( "x" , "y" ).map( toSortedQId ) )  ,
        ( "0.0 + x + y" , Reals( 0.0 ) + x + y , Set( "x" , "y" ).map( toSortedQId ) )
    )
    //  format: OFF

    import theories.BoolTerm

    //  format: OFF
    val theTerms2 = Table[ String, TypedTerm[ BoolTerm, Term ], Set[ SortedQId ] ](
        ( "Expression" , "TypedTerm"                 , "typeDefs" )                    ,

        ( "1.0 < y"    , Reals( 1.0 ) < y            , Set( "y" ).map( toSortedQId ) ) ,
        ( "1.0 <= y"   , Reals( 1.0 ) <= y           , Set( "y" ).map( toSortedQId ) ) ,
        ( "1.0 > y"    , Reals( 1.0 ) > y            , Set( "y" ).map( toSortedQId ) ) ,
        ( "1.0 >= y"   , Reals( 1.0 ) >= y           , Set( "y" ).map( toSortedQId ) ) ,
        ( "1.0 === y"  , Reals( 1.0 ) === y          , Set( "y" ).map( toSortedQId ) ) ,

        ( "y < 1.0"    , y < Reals( 1.0 )            , Set( "y" ).map( toSortedQId ) ) ,
        ( "y <= 1.0"   , y <= Reals( 1.0 )           , Set( "y" ).map( toSortedQId ) ) ,
        ( "y > 1.0"    , y > Reals( 1.0 )            , Set( "y" ).map( toSortedQId ) ) ,
        ( "y >= 1.0"   , y >= Reals( 1.0 )           , Set( "y" ).map( toSortedQId ) ) ,
        ( "y === 1.0"  , y === Reals( 1.0 )          , Set( "y" ).map( toSortedQId ) ) ,

        ( "x < y"      , x < y                       , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x <= y"     , x <= y                      , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x > y"      , x > y                       , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x > y_3"    , x > (y indexed 4)           , Set( "x" , "y@4" ).map( toSortedQId ) ) ,
        ( "x >= y"     , x >= y                      , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x === y"    , x === y                     , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "0 < 1.0"    , Reals( 0.0 ) < Reals( 1.0 ) , Set() )
    )
    //  format: ON

    for ( ( e, t, tdef ) ← theTerms1 ++ theTerms2 ) {
        val result = tdef.map( x ⇒ format( x ).layout )
        test( s"Check the typeDefs built with for term $e -- Should be $result" ) {
            t.typeDefs shouldBe tdef
        }
    }
}
