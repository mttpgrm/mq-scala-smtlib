/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package theories
package tests

import parser.Implicits._
import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

class BitVectorsFactoryTypeTests extends FunSuite with TableDrivenPropertyChecks with Matchers
        with Core with BitVectors {

    override def suiteName = "Check the typeDefs of BitVector terms"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax.{ QualifiedId, SortedQId, BitVectorSort }
    import parser.SMTLIB2PrettyPrinter.{ format, show }
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm

    //  get a SortedQId parser
    //  as SortedQId is a rule of QualifiedId we get QualifiedId parser first
    val p1 = SMTLIB2Parser[ QualifiedId ]
    val sortedIdParser : String ⇒ SortedQId = { x ⇒ p1( x ).get.asInstanceOf[ SortedQId ] }

    //  create a solver SortedQId, type Ints, from a string
    def toSortedQId( bits : Int )( name : String ) : SortedQId =
        sortedIdParser( s"(as $name ${show( BitVectorSort( bits.toString ) )})" )

    val x = BVs( "x", 16 )
    val y = BVs( "y", 32 )
    val y1 = y indexed 1

    //  format: OFF
    val theTerms1 = Table[ String, TypedTerm[ BVTerm, Term ], Set[ SortedQId ] ](
        ( "Expression"       , "TypedTerm"                  , "typeDefs" )                          ,
        ( "0 - 1"            , BVs(0,16)                    , Set() )                               ,
        ( "- 1"              , -BVs( 1,16 )                 , Set() )                               ,
        ( "#xa9 + 1"         , BVs( "#xa9" ) + BVs(1,16)    , Set() )                               ,
        ( "#xa9 + #b101"     , BVs( "#xa9" ) + BVs("#b101") , Set() )                               ,
        ( "x"                , x                            , Set( "x" ).map( toSortedQId(16) ) )   ,
        ( "y1"               , y1                           , Set( "y@1" ).map( toSortedQId(32) ) ) ,
        ( "x + y"            , x + y                        , Set( toSortedQId(16)("x"), toSortedQId(32)("y")  ) ) ,
        ( "x - y"            , x - y                        , Set( toSortedQId(16)("x"), toSortedQId(32)("y")  ) ) ,
        ( "x + y + #xaf0"    , x + y + BVs("#xaf0")         , Set( toSortedQId(16)("x"), toSortedQId(32)("y")  ) ) ,
        ( "#b101 + x + y"    , x + y + BVs("#b101")         , Set( toSortedQId(16)("x"), toSortedQId(32)("y")  ) ) ,
        ( "x / 2"            , x / BVs(2,16)                , Set( "x" ).map( toSortedQId(16) ) )   ,
        ( "x sdiv 2"         , x sdiv BVs(2,16)             , Set( "x" ).map( toSortedQId(16) ) )   ,
        ( "x % 2"            , x % BVs(2,16)                , Set( "x" ).map( toSortedQId(16) ) )   ,
        ( "x srem 2"         , x srem BVs(2,16)             , Set( "x" ).map( toSortedQId(16) ) )   ,
        ( "x | y + #b101"    , x or y +  BVs("#b101")       , Set( toSortedQId(16)("x"),  toSortedQId(32)("y")  ) ) ,
        ( "x ^ y + #b101"    , x xor y +  BVs("#b101")      , Set( toSortedQId(16)("x"),  toSortedQId(32)("y")  ) ) ,
        ( "x & y + #b101"    , x and y +  BVs("#b101")      , Set( toSortedQId(16)("x") , toSortedQId(32)("y")  ) ) ,
        ( "x * y"            , x * y                        , Set( toSortedQId(16)("x") , toSortedQId(32)("y")  ) ) ,
        ( "x << y"           , x << y                       , Set( toSortedQId(16)("x") , toSortedQId(32)("y")  ) ) ,
        ( "x >> y"           , x >> y                       , Set( toSortedQId(16)("x") , toSortedQId(32)("y")  ) ) ,
        ( "x ashr y"         , x ashr y                     , Set( toSortedQId(16)("x") , toSortedQId(32)("y")  ) ) ,
        ( "x sext 5"         , x sext 5                     , Set( toSortedQId(16)("x") ) ) ,
        ( "x zext 5"         , x zext 5                     , Set( toSortedQId(16)("x") ) ) ,
        ( "x extract (4, 8)" , x extract (4, 8)             , Set( toSortedQId(16)("x") ) )
    )
    //  format: ON

    for ( ( e, t, tdef ) ← theTerms1 ) {
        val result = tdef.map( x ⇒ show( x ) )
        test( s"Check the typeDefs built with for term $e -- Should be $result" ) {
            t.typeDefs shouldBe tdef
        }
    }
}

class BitVectorsFactoryTermTests extends FunSuite with TableDrivenPropertyChecks with Matchers
        with Core with BitVectors {

    override def suiteName = "Check the termDef of BitVector terms"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax._
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm

    val x = BVs( "x", 16 )
    val y = BVs( "y", 32 )
    val y1 = y indexed 1

    //  format: OFF
    val theTerms1 = Table[ String, TypedTerm[ BVTerm, Term ], Term ](
        ( "Expression"    , "TypedTerm"                   , "termDefs" ),
        ( "0 - 1"         , BVs( 0,16)                    , ConstantTerm(DecBVLit(BVvalue("0"),"16"))),
        ( "- 1"           , -BVs( 1,16 )                  , BVNegTerm(
                                                                ConstantTerm(DecBVLit(BVvalue("1"),"16")))
        ),
        ( "#xa9 + 1"      , BVs( "#xa9" ) + BVs(1,16)            , BVAddTerm(
                                                            ConstantTerm(
                                                                HexaLit("a9")),
                                                                ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                            )
        ),
        ( "#xa9 + #b101"  , BVs( "#xa9" ) + BVs("#b101") , BVAddTerm(
                                                            ConstantTerm(
                                                                HexaLit("a9")),
                                                                ConstantTerm(BinLit("101"))
                                                            )
        ),
        ( "x"             , x                            , QIdTerm(
                                                            SimpleQId(SymbolId(SSymbol("x"))))
        ),
        ( "y1"            , y1                           , QIdTerm(
                                                            SimpleQId(SymbolId(ISymbol("y",1))))
        ),
        ( "x + y"         , x + y                        , BVAddTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(SSymbol("x")))),
                                                                    QIdTerm(SimpleQId(SymbolId(SSymbol("y"))))
                                                            )
        ),
        ( "x - y"         , x - y                        , BVSubTerm(
                                                            QIdTerm(
                                                                SimpleQId(SymbolId(SSymbol("x")))),
                                                                QIdTerm(SimpleQId(SymbolId(SSymbol("y"))))
                                                            )
        ),
        ( "x + y + #xaf0" , x + y + BVs("#xaf0")         , BVAddTerm(
                                                            BVAddTerm(
                                                                QIdTerm(
                                                                    SimpleQId(SymbolId(SSymbol("x")))),
                                                                    QIdTerm(SimpleQId(SymbolId(SSymbol("y"))))
                                                            ),
                                                            ConstantTerm(HexaLit("af0"))
                                                           )
        ),
        ( "#b101 + x + y" , x + y + BVs("#b101")         , BVAddTerm(
                                                            BVAddTerm(
                                                                QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(SSymbol("x"))
                                                                    )
                                                                ),
                                                                QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(
                                                                            SSymbol("y")))
                                                                        )
                                                                    ),
                                                                    ConstantTerm(BinLit("101"))
                                                                )
        ),
        ( "x / 2"         , x / BVs(2,16)                        , BVuDivTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(SSymbol("x")))
                                                                ),
                                                                ConstantTerm(DecBVLit(BVvalue("2"),"16"))
                                                            )
        ),
        ( "x sdiv 2"      , x sdiv BVs(2,16)                     , BVsDivTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(SSymbol("x")))
                                                                ),
                                                                ConstantTerm(DecBVLit(BVvalue("2"),"16"))
                                                            )
        ),
        ( "x % 2"         , x % BVs(2,16)                        , BVuRemTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                ),
                                                                ConstantTerm(DecBVLit(BVvalue("2"),"16"))
                                                            )
        ),
        ( "x srem 2"      , x srem BVs(2,16)                     , BVsRemTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                ),
                                                                ConstantTerm(DecBVLit(BVvalue("2"),"16"))
                                                            )
        ),
        ( "x | y + #b101" , x or y +  BVs("#b101")        , BVOrTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                ),
                                                                BVAddTerm(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    ),
                                                                    ConstantTerm(BinLit("101")))
                                                                )
        ),
        ( "x ^ y + #b101" , x xor y +  BVs("#b101")       , BVXorTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                ),
                                                                BVAddTerm(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    ),
                                                                    ConstantTerm(BinLit("101")))
                                                                )
        ),
        ( "x & y + #b101" , x and y +  BVs("#b101")        , BVAndTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x")
                                                                    )
                                                                )
                                                            ),
                                                            BVAddTerm(
                                                                QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(
                                                                            SSymbol("y"))
                                                                        )
                                                                    ),
                                                                    ConstantTerm(BinLit("101"))
                                                                )
                                                            )
        ),
        ( "x * y"         , x * y                        , BVMultTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x")
                                                                    )
                                                                )
                                                            ),
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(SSymbol("y")))
                                                                )
                                                            )
        ),
        ( "x << y"        , x << y                       , BVShiftLeftTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x")
                                                                    )
                                                                )
                                                            ),
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(SSymbol("y"))
                                                                )
                                                            )
                                                        )
        ),
        ( "x >> y"        , x >> y                       , BVSLogicalShiftRightTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                ),
                                                                QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(
                                                                            SSymbol("y")
                                                                        )
                                                                    )
                                                                )
                                                            )
        ),
        ( "x ashr y"        , x ashr y                  , BVSArithmeticShiftRightTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                ),
                                                                QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(
                                                                            SSymbol("y")
                                                                        )
                                                                    )
                                                                )
                                                            )
        ),
        ( "x sext 5"        , x sext 5                 , BVSignExtendTerm(
                                                            NumLit("5"),
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                )
                                                            )
        ),
        ( "x zext 5"        , x zext 5                 , BVZeroExtendTerm(
                                                            NumLit("5"),
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                )
                                                            )
        ),
        ( "x extract (4, 8)", x extract (4, 8)         , BVExtractTerm(
                                                            NumLit("4"),
                                                            NumLit("8"),
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                )
                                                            )
        )
    )
    //  format: ON

    for ( ( e, t, tdef ) ← theTerms1 ) {
        // val result = tdef.map( x ⇒ show( x ) )
        test( s"Check the termDef built with for term $e -- Should be $tdef" ) {
            t.termDef shouldBe tdef
        }
    }
}

class BitVectorsFactoryShowTests extends FunSuite with TableDrivenPropertyChecks with Matchers
        with Core with BitVectors {

    override def suiteName = "Check the typeDefs of BitVector terms"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax.{ QualifiedId, SortedQId, BitVectorSort }
    import parser.SMTLIB2PrettyPrinter.{ format, show }
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm

    val x = BVs( "x", 16 )
    val y = BVs( "y", 32 )
    val y1 = y indexed 1

    //  format: OFF
    val theTerms1 = Table[ String, TypedTerm[ BVTerm, Term ], String ](
        ( "Expression"      , "TypedTerm"                  , "solver string" ),
        ( "0 - 1"           , BVs( 0, 16)                  , "( _ bv0 16) "),
        ( "- 1"             , -BVs( 1, 16 )                , "(bvneg ( _ bv1 16) ) "),
        ( "#xa9 + 1"        , BVs( "#xa9" ) + BVs(1,16)    , "(bvadd #xa9 ( _ bv1 16) ) "),
        ( "#xa9 + #b101"    , BVs( "#xa9" ) + BVs("#b101") , "(bvadd #xa9 #b101 ) "),
        ( "x"               , x                            , "x "),
        ( "y1"              , y1                           , "y@1 "),
        ( "x + y"           , x + y                        , "(bvadd x y ) "),
        ( "x - y"           , x - y                        , "(bvsub x y ) "),
        ( "x + y + #xaf0"   , x + y + BVs("#xaf0")         , "(bvadd (bvadd x y ) #xaf0 ) "),
        ( "#b101 + x + y"   , x + y + BVs("#b101")         , "(bvadd (bvadd x y ) #b101 ) "),
        ( "x / 2"           , x / BVs(2,16)                , "(bvudiv x ( _ bv2 16) ) "),
        ( "x sdiv 2"        , x sdiv BVs(2,16)             , "(bvsdiv x ( _ bv2 16) ) "),
        ( "x % 2"           , x % BVs(2,16)                , "(bvurem x ( _ bv2 16) ) "),
        ( "x srem 2"        , x srem BVs(2,16)             , "(bvsrem x ( _ bv2 16) ) "),
        ( "x | y + #b101"   , x or y +  BVs("#b101")       , "(bvor x (bvadd y #b101 ) ) "),
        ( "x ^ y + #b101"   , x xor y +  BVs("#b101")      , "(bvxor x (bvadd y #b101 ) ) "),
        ( "x & y + #b101"   , x and y +  BVs("#b101")      , "(bvand x (bvadd y #b101 ) ) "),
        ( "x * y"           , x * y                        , "(bvmul x y ) "),
        ( "x << y"          , x << y                       , "(bvshl x y ) "),
        ( "x >> y"          , x >> y                       , "(bvlshr x y ) "),
        ( "x ashr y"        , x ashr y                     , "(bvashr x y ) "),
        ( "x sext 5"        , x sext 5                     , "( ( _ sign_extend 5) x ) "),
        ( "x zext 5"        , x zext 5                     , "( ( _ zero_extend 5) x ) "),
        ( "x extract (4, 8)", x extract (4, 8)             , "( ( _ extract 4 8) x ) ")
    )
    //  format: ON

    for ( ( e, t, tdef ) ← theTerms1 ) {
        test( s"Check the typeDefs built with for term $e -- Should be $tdef" ) {
            show( t.termDef ) shouldBe tdef
        }
    }
}

class SimpleBitVectorTests extends FunSuite with TableDrivenPropertyChecks with Matchers
        with Core with BitVectors with Commands with Resources {

    override def suiteName = "Check-sat for BitVector terms"

    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat, SatResponses }
    import typedterms.TypedTerm

    val x = BVs( "x", 16 )
    val y = BVs( "y", 16 )
    val z = BVs( "z", 32 )

    //  BVs operators are compatible only for same size BVs
    //  format: OFF
    val theLIATerms = Table[ String, TypedTerm[ BoolTerm, Term ], Try[SatResponses] ](
        ( "expression"      , "TypedTerm"                           , "Response" )                                     ,
        ( "simple equality" , x === BVs( "#x01" )                   , Failure(new Exception(" Not same fixed size")) ) ,
        ( "bitvector 0"     , x === BVs( "#x0001" )                 , Success( Sat() ) )                               ,
        ( "#xa0 + #b0101"   , BVs( "#xa0" ) === BVs( "#b10100101" ) , Success( UnSat() ) )                             ,
        ( "-y == 2"         , -y  === 2.withBits(16)                , Success( Sat() ) )                               ,
        ( "x + y == 1"      , x + y  === 1.withBits(16)             , Success( Sat() ) )                               ,
        ( "x - y + z == 1"  , x - y + z  === 1.withBits(16)         , Failure(new Exception("z is not 32 bits")) )     ,
        ( "x * y == 1"      , x * y  === 1.withBits(16)             , Success( Sat() ) )                               ,
        ( "x / y == 1"      , x / y  === 1.withBits(16)             , Success( Sat() ) )                               ,
        ( "x sdiv y == 1"   , (x sdiv y)  === 1.withBits(16)        , Success( Sat() ) )                               ,
        ( "x % y == 2"      , x % y  === 2.withBits(16)             , Success( Sat() ) )                               ,
        ( "x srem y == 2"   , (x srem y)  === 2.withBits(16)        , Success( Sat() ) )                               ,
        ( "x | y == 12"     , (x or y)  === 12.withBits(16)         , Success( Sat() ) )                               ,
        ( "x ^ y == 12"     , (x xor y)  === 12.withBits(16)        , Success( Sat() ) )                               ,
        ( "x & y == 2"      , (x and y)  === 2.withBits(16)         , Success( Sat() ) )                               ,
        ( "x << 2 ==  y"    , (x << 2.withBits(16))  === y          , Success( Sat() ) )                               ,
        ( "x >> 4 == y"     , (x >> 4.withBits(16))  === y          , Success( Sat() ) )                               ,
        ( "x ashr 4 == y"   , (x ashr 4.withBits(16))  === y        , Success( Sat() ) )                               ,
        ( "x >> y ult 4"    , (x >> y ult 4.withBits(16))           , Success( Sat() ) )                               ,
        ( "x >> 2 ule y"    , (x >> y ule 4.withBits(16))           , Success( Sat() ) )                               ,
        ( "x >> 3 uge y"    , (x >> y uge 4.withBits(16))           , Success( Sat() ) )                               ,
        ( "x >> y ugt 4"    , (x >> y ugt 4.withBits(16))           , Success( Sat() ) )                               ,
        ( "x >> y slt 4"    , (x >> y slt 4.withBits(16))           , Success( Sat() ) )                               ,
        ( "x >> 2 sle y"    , (x >> y sle 4.withBits(16))           , Success( Sat() ) )                               ,
        ( "x >> 3 sge y"    , (x >> y sge 4.withBits(16))           , Success( Sat() ) )                               ,
        ( "x >> y sgt 4"    , (x >> y sgt 4.withBits(16))           , Success( Sat() ) )
    )

    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import parser.SMTLIB2Syntax.{ Sat, UnSat, UnKnown }
    import PPrinter._
    import theories.PredefinedLogics.QF_BV

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_BV,
        new CVC4 with QF_BV,
        new MathSAT with QF_BV,
        new Yices with QF_BV
    )

    for ( s ← theSolvers ) {
        forAll( theLIATerms ) {
            ( x : String, t : TypedTerm[ BoolTerm, Term ], r : Try[ SatResponses ] ) ⇒
                test( s"Using solver $s configured with ${pprintConfig( s )} check-sat $x i.e. bivector term ${format( t.termDef ).layout}  should be ${r match { case Failure( e ) ⇒ e; case Success( b ) ⇒ b }}" ) {

                    //  with using (monadic)
                    using( s ) {
                        implicit withSolver ⇒
                            isSat( t )
                    } should matchPattern {
                        case Success( x ) if ( r.isSuccess && r.get == x ) ⇒
                        case Failure( _ ) if ( r.isFailure )               ⇒
                    }
                }
        }
    }
}

class GetValueBitVectorTests extends FunSuite with TableDrivenPropertyChecks with Matchers
        with Core with BitVectors with Commands with Resources {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    override def suiteName = "Check-sat and get values for BitVector terms"

    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.{ QIdTerm, SimpleQId, Term, Sat, UnSat, SatResponses }
    import typedterms.{ TypedTerm, Value }

    val x = BVs( "x", 16 )
    val y = BVs( "y", 16 )
    val z = BVs( "z", 32 )

    //  BVs operators are compatible only for same size BVs
    //  format: OFF
    val theLIATerms = Table[ String, TypedTerm[ BoolTerm, Term ], Try[SatResponses] ](
        ( "expression"  , "TypedTerm"                    , "Response" )       ,
        ( "bitvector 0" , x === BVs( "#x0001" )          , Success( Sat() ) ) ,
        ( "-y"          , -y  === 2.withBits(16)         , Success( Sat() ) ) ,
        ( "x + y"       , x + y  === 1.withBits(16)      , Success( Sat() ) ) ,
        ( "x * y"       , x * y  === 1.withBits(16)      , Success( Sat() ) ) ,
        ( "x / y"       , x / y  === 1.withBits(16)      , Success( Sat() ) ) ,
        ( "x sdiv y"    , (x sdiv y ) === 1.withBits(16) , Success( Sat() ) ) ,
        ( "x % y"       , x % y  === 2.withBits(16)      , Success( Sat() ) ) ,
        ( "x srem y"    , (x srem y)  === 2.withBits(16) , Success( Sat() ) ) ,
        ( "x | y"       , (x or y)  === 12.withBits(16)  , Success( Sat() ) ) ,
        ( "x ^ y"       , (x xor y)  === 12.withBits(16) , Success( Sat() ) ) ,
        ( "x & y"       , (x and y)  === 2.withBits(16)  , Success( Sat() ) ) ,
        ( "x << y"      , (x << 2.withBits(16))  === y   , Success( Sat() ) ) ,
        ( "x >> y"      , (x >> 4.withBits(16))  === y   , Success( Sat() ) ) ,
        ( "x ashr y"    , (x ashr 4.withBits(16))  === y , Success( Sat() ) )
    )

    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import parser.SMTLIB2Syntax.{ Sat, UnSat, UnKnown }
    import PPrinter._
    import theories.PredefinedLogics.QF_BV

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_BV with Models,
        new CVC4 with QF_BV with Models,
        new MathSAT with QF_BV with Models,
        new Yices with QF_BV with Models
    )

    for ( s ← theSolvers ) {
        forAll( theLIATerms ) {
            ( name : String, t : TypedTerm[ BoolTerm, Term ], r : Try[ SatResponses ] ) ⇒
                test( s"Using solver $s configured with ${pprintConfig( s )} check-sat for bivector term ${format( t.termDef ).layout}  can be established" ) {

                    //  with using (monadic)

                    val witnesses = using( s ) {
                        implicit withSolver ⇒
                            isSat( t ) shouldBe Success( Sat () )

                            //  get variables declared on the stack
                            getDeclCmd() match {
                                case Failure( e ) ⇒ Failure( e )
                                case Success( xl ) ⇒
                                    logger.info( "getDeclCmd  returned {}", xl )
                                    //  make a TypedTerm from the declarations to extract a value
                                    val y = xl
                                        .map( { x ⇒ TypedTerm( Set( x ), QIdTerm( SimpleQId( x.id ) ) ) } )
                                        .map( getValue )
                                    //  return failure if list of Try  contains a failure otherwise
                                    //  returns a Success(list of Success)
                                    collectSuccessOrFailure( y )
                            }
                    }
                    logger.info( "Collected values are {}", witnesses )
                    //  witnesses should have been computed successfully
                    witnesses should matchPattern {
                        case Success( _ ) ⇒
                    }
                    //  witnesses should be a List[Value]. Cannot be checked easily
                    //  in one step  because of type erasure
                    //  format: OFF
                    witnesses.get shouldBe a [ List[ _ ] ]
                    witnesses.get.foreach {
                        _  shouldBe a [ Value ]
                    }
                    //  format: ON
                }
        }
    }
}
