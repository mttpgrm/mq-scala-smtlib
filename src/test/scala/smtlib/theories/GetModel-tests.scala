/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package theories
package tests

import parser.Implicits._
import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

class IntsGetModelTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Commands with Resources {

    override def suiteName = "Check sat anf get models for LIA"

    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.Term
    import typedterms.{ TypedTerm, Model }
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ] ](
        ( "expression"         , "TypedTerm")       ,
        ( "integer variable x" , x  === y )         ,
        ( "-x"                 , -x  === 1)         ,
        ( "x + y"              , x + y  === 0 )     ,
        ( "x + y = z"          , x + y  === z )     ,
        ( "2 * x = z"          , x * 2  === z )     ,
        ( "1 + 2 + x"          , x + 1 + 2  === -4)
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ Sat, UnSat }
    import theories.PredefinedLogics.QF_LIA
    import PPrinter._

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_LIA with Models,
        new SMTInterpol with QF_LIA with Models,
        new CVC4 with QF_LIA with Models,
        new MathSAT with QF_LIA with Models,
        new Yices with QF_LIA with Models
    )

    for ( s ← theSolvers; ( x, t ) ← theTerms ) {

        if ( s.isInstanceOf[ MathSAT ] || s.isInstanceOf[ Yices ] )
            test( s"Using solver $s  to get model for ${format( t.termDef ).layout} -- $s response is not conformant to SMTLIB2 grammar" ) {
                //  with using
                using( SMTSolver( s ) ) {
                    implicit solver ⇒
                        {
                            //  smtlib package eval is used
                            isSat( t ) should matchPattern {
                                case Success( Sat() ) ⇒
                            }

                            //  now get a model, should not work
                            //  returns a getValue response
                            val r = getModel()
                            logger.info( "Get-model returned: {}", r )

                            r should matchPattern {
                                case Failure( _ ) ⇒
                            }
                        }
                }
            }
        else
            test( s"Using solver $s configured with ${pprintConfig( s )} to get a model for term ${format( t.termDef ).layout} === 1 " ) {

                //  with using
                using( SMTSolver( s ) ) {
                    implicit solver ⇒
                        {
                            //  smtlib package eval is used
                            isSat( t ) should matchPattern {
                                case Success( Sat() ) ⇒
                            }

                            //  now get a model, should always work
                            val r = getModel()
                            logger.info( "Get-model returned: {}", r )
                            r should matchPattern {
                                case Success( Model( x ) ) ⇒
                            }
                        }
                }
            }
    }
}

class RealsGetModelTests extends FunSuite with TableDrivenPropertyChecks with Matchers with RealArithmetics with Core with Commands with Resources {

    override def suiteName = "Check sat anf get models for LRA"

    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.Term
    import typedterms.{ TypedTerm, Model }
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    val x = Reals( "x" )
    val y = Reals( "y" )
    val z = Reals( "z" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ] ](
        ( "expression"         , "TypedTerm")       ,
        ( "integer variable x" , x  === y )         ,
        ( "-x"                 , -x  === 1)         ,
        ( "x + y"              , x + y  === 0 )     ,
        ( "x + y = z"          , x + y  === z )     ,
        ( "2 * x = z"          , x * 2.0  === z )     ,
        ( "1 + 2 + x"          , x + 1.0 + 2.4  === -4.66)
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ Sat, UnSat, GetDeclCmd }
    import theories.PredefinedLogics.QF_LRA
    import PPrinter._

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_LRA with Models,
        new SMTInterpol with QF_LRA with Models,
        new CVC4 with QF_LRA with Models,
        new MathSAT with QF_LRA with Models,
        new Yices with QF_LRA with Models
    )

    for ( s ← theSolvers; ( _, t ) ← theTerms ) {

        if ( s.isInstanceOf[ MathSAT ] || s.isInstanceOf[ Yices ] )
            test( s"Using solver $s  to get model for ${format( t.termDef ).layout} -- $s response is not conformant to SMTLIB2 grammar -- returns a getValue instead" ) {
                //  with using
                using( SMTSolver( s ) ) {
                    implicit solver ⇒
                        {
                            //  smtlib package eval is used
                            isSat( t ) should matchPattern {
                                case Success( Sat() ) ⇒
                            }

                            //  now get a model, should not work
                            val r = getModel()
                            logger.info( "Get-model returned: {}", r )

                            r should matchPattern {
                                case Failure( _ ) ⇒
                            }
                        }
                }
            }
        else
            test( s"Using solver $s configured with ${pprintConfig( s )} to get a model for term ${format( t.termDef ).layout} === 1" ) {

                //  with using
                using( SMTSolver( s ) ) {
                    implicit solver ⇒
                        {
                            //  smtlib package eval is used
                            isSat( t ) should matchPattern {
                                case Success( Sat() ) ⇒
                            }

                            logger.info( "Declarations: {}", eval( Seq( GetDeclCmd() ) ) )

                            //  now get a model, should always work
                            val r = getModel()
                            logger.info( "Get-model returned: {}", r )
                            r should matchPattern {
                                case Success( Model( x ) ) ⇒
                            }
                        }
                }
            }
    }
}

class ArrayGetModelTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with ArrayExInt with ArrayExBool with ArrayExOperators with Core with Commands with Resources {

    override def suiteName = "Check sat and get model for terms with arrays"

    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.Term
    import typedterms.{ TypedTerm, Model }
    import parser.SMTLIB2Syntax.CheckSatResponses
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import scala.util.Success
    import PPrinter._
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.Term
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val a1 = ArrayInt1( "a1" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ] ](
        ( "expression"  , "TypedTerm")  ,
        ( "array 1"     , a1(1) === y ) ,
        ( "array 2"     , (a1(1) === y) & (a1(2) === y - 1) )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ Sat, UnSat, GetDeclCmd }
    import theories.PredefinedLogics.QF_AUFLIA
    import PPrinter._

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_AUFLIA with Models,
        new SMTInterpol with QF_AUFLIA with Models,
        new CVC4 with QF_AUFLIA with Models,
        new MathSAT with QF_AUFLIA with Models,
        new Yices with QF_AUFLIA with Models
    )

    for ( s ← theSolvers; ( _, t ) ← theTerms ) {

        if ( s.isInstanceOf[ MathSAT ] || s.isInstanceOf[ SMTInterpol ] || s.isInstanceOf[ Yices ] )
            //  note: SMTInterpol crashes and I believe this is a bug in interpol
            test( s"Using solver $s  to get model for ${format( t.termDef ).layout} -- $s response is not conformant to SMTLIB2 grammar -- returns a GetValue instead" ) {

                //  with using
                using( SMTSolver( s ) ) {
                    implicit solver ⇒
                        {
                            //  smtlib package eval is used
                            isSat( t ) should matchPattern {
                                case Success( Sat() ) ⇒
                            }

                            //  now get a model, should not work
                            val r = getModel()
                            logger.info( "Get-model returned: {}", r )

                            r should matchPattern {
                                case Failure( _ ) ⇒
                            }
                        }
                }
            }
        else
            test( s"Using solver $s configured with ${pprintConfig( s )} to get a model for term ${format( t.termDef ).layout} === 1" ) {

                //  with using
                using( SMTSolver( s ) ) {
                    implicit solver ⇒
                        {
                            //  smtlib package eval is used
                            isSat( t ) should matchPattern {
                                case Success( Sat() ) ⇒
                            }

                            logger.info( "Declarations: {}", eval( Seq( GetDeclCmd() ) ) )

                            //  now get a model, should always work
                            val r = getModel()

                            r should matchPattern {
                                case Success( Model( x ) ) ⇒
                            }
                        }
                }
            }
    }
}
