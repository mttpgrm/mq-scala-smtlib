/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package theories
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ IntegerArithmetics, Core }
import interpreters.Resources
import typedterms.{ Commands, QuantifiedTerm }

class DeclareLetTermsTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with ArrayExInt with Core with Commands with Resources {

    override def suiteName = "Declare let terms in solvers test suite"

    import scala.util.Success
    import parser.SMTLIB2Syntax.{ Term, TermTester, CheckSatResponses, Sat }
    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax.{ QualifiedId, SimpleQId, SymbolId, SSymbol }
    import parser.Implicits._
    import typedterms.TypedTerm
    import theories.PredefinedLogics.QF_AUFLIA
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.format
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._

    val p1 = SMTLIB2Parser[ TermTester ]

    //  create a solver SortedQId, type Ints, from a string
    def toSimpleQId( name : String ) : QualifiedId = SimpleQId( SymbolId( SSymbol( name ) ) )

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_AUFLIA,
        new SMTInterpol with QF_AUFLIA,
        new CVC4 with QF_AUFLIA,
        new MathSAT with QF_AUFLIA,
        new Yices with QF_AUFLIA
    )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )
    val u = Ints( "u" )
    val a1 = ArrayInt1( "a1" )

    val theTerms = Table(
        ( "Terms", "Expected response" ),

        ( """|
             | (
             |  let
             |    (
             |     (  a!1
             |         (>= (+ x (* (- 1 ) (select a1 1 ) ) ) 0 ) )
             |     )
             |    (not a!1 )
             | )
             |""".stripMargin, Set( x, a1 ) )
    )

    for ( s ← theSolvers; ( xt, vars ) ← theTerms ) {

        val Success( TermTester( yt ) ) = p1( xt )

        val letTypedTerm = TypedTerm[ BoolTerm, Term ]( vars.map( _.typeDefs ).reduceLeft( _ ++ _ ), yt )

        test( s"Using solver $s configured with ${s.configScript} assert letTerm  ${format( letTypedTerm.termDef ).layout} -- should succeed" ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        // val letTypedTerm = TypedTerm( vars.map( typeDefs ).reducLeft( _ ++ _ ), xt )
                        |=( letTypedTerm ) should matchPattern {
                            case Success( _ ) ⇒
                        }

                    }
            }
        }
    }

}

class AssertLetTermsTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Commands with QuantifiedTerm with Resources {

    override def suiteName = "Assert simple let terms LIA in solvers test suite"

    import scala.util.Success
    import parser.SMTLIB2Syntax.{ Term, TermTester, CheckSatResponses, Sat, UnSat, Command }
    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax.{ QualifiedId, SimpleQId, SymbolId, SSymbol }
    import parser.Implicits._
    import typedterms.TypedTerm
    import theories.PredefinedLogics.QF_LIA
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.format
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._

    //  prettyPrinter for solver configuration
    def pprintConfig( s : Seq[ Command ] ) =
        s map ( l ⇒ format( l ).layout ) mkString ( "\n\t", "\n\t", "\n" )

    val p1 = SMTLIB2Parser[ TermTester ]

    //  create a solver SortedQId, type Ints, from a string
    def toSimpleQId( name : String ) : QualifiedId = SimpleQId( SymbolId( SSymbol( name ) ) )

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_LIA,
        new SMTInterpol with QF_LIA,
        new CVC4 with QF_LIA,
        new MathSAT with QF_LIA,
        new Yices with QF_LIA
    )

    val x = Ints( "x" )
    val y = Ints( "y" )

    val theTerms = Table(
        ( "Terms", "Expected response" ),

        (
            let {
                val x1 = BoundedVar( "x1", x + 1 )
                val x2 = BoundedVar( "y1", y * 2 )
                x1 <= x2
            },
            Sat()
        ),

        (
            let {
                val x1 = BoundedVar( "x1", x + 1 )
                let {
                    val x2 = BoundedVar( "y1", x1 - 2 )
                    x1 <= y & y < x2
                }
            },
            UnSat()
        )
    )

    for ( s ← theSolvers; ( xt, r ) ← theTerms ) {

        test( s"Using solver $s configured with ${pprintConfig( s.configScript.toList )} assert the letTerm  ${format( xt.termDef ).layout} -- should be ${format( r ).layout} " ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        isSat( xt ) shouldBe ( Success( r ) )

                    }
            }
        }
    }

}
