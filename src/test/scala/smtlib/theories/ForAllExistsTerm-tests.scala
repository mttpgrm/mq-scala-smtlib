/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package theories
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ IntegerArithmetics, Core }
import interpreters.Resources
import typedterms.{ Commands, QuantifiedTerm }

class DeclareForAllExistsTermsTests
        extends FunSuite
        with TableDrivenPropertyChecks
        with Matchers
        with IntegerArithmetics
        with ArrayExInt
        with Core
        with Commands
        with Resources
        with QuantifiedTerm {

    override def suiteName = "Declare forall/exists terms in solvers test suite"

    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ Term, TermTester, CheckSatResponses, Sat }
    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax.{ QualifiedId, SimpleQId, SymbolId, SSymbol, SortedQId }
    import parser.Implicits._
    import typedterms.{ VarTerm, TypedTerm }
    import theories.PredefinedLogics.AUFNIRA
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.show
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._

    val p1 = SMTLIB2Parser[ TermTester ]

    //  create a solver SortedQId, type Ints, from a string
    def toSimpleQId( name : String ) : QualifiedId = SimpleQId( SymbolId( SSymbol( name ) ) )

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with AUFNIRA
    )

    val x = Ints( "x" )
    val y = Ints( "y" )

    //  format: OFF
    val theTerms = Table[  TypedTerm[BoolTerm, Term] ](

        "typed term version",
        (
            forall (Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= 1
            }
        ),

        //  one var not bounded
        (
            (forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= 1
            }) & (x === 3)
        ),

        //  one var not bounded
        (
            (forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= y
            }) & (x === 3)
        ),

        //  indexed variable
        (
            forall(Ints("x1").indexed(1).symbol) {
                val x1 = Ints("x1").indexed(1)
                x1 + 2 <= 1
            }
        ),

        //  nested forall
        (
            forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                forall(Ints("y1").symbol) {
                    val y1 = Ints("y1")
                    y1 >= 0 imply y1 >= x1
                }
            }
        ),

        //  exists and forall
        (
            exists(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                forall(Ints("y1").symbol) {
                    val y1 = Ints("y1")
                    y1 >= 0 imply y1 >= x1
                }
            }
        ),

        (
            forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                exists(Ints("y1").symbol) {
                    val y1 = Ints("y1")
                    y1 >= 0 imply y1 >= x1
                }
            }
        )
    )
    // format: ON

    for ( s ← theSolvers; ( xt ) ← theTerms ) {

        test( s"Using solver $s configured with ${s.configScript} assert quantified Term  ${show( xt.termDef )} -- should succeed" ) {

            //  with using
            using( s ) {
                implicit solver ⇒
                    {
                        |=( xt )
                    }
            } should matchPattern {
                case Success( _ ) ⇒
            }
        }
    }
}

class CheckSatForAllExistsTermsTests
        extends FunSuite
        with TableDrivenPropertyChecks
        with Matchers
        with IntegerArithmetics
        with ArrayExInt
        with Core
        with Commands
        with Resources
        with QuantifiedTerm {

    override def suiteName = "Declare forall/exists terms in solvers test suite"

    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ SatResponses, Term, TermTester, CheckSatResponses, Sat, UnSat }
    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax.{ QualifiedId, SimpleQId, SymbolId, SSymbol, SortedQId }
    import parser.Implicits._
    import typedterms.{ VarTerm, TypedTerm }
    import theories.PredefinedLogics.AUFNIRA
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.show
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._

    val p1 = SMTLIB2Parser[ TermTester ]

    //  create a solver SortedQId, type Ints, from a string
    def toSimpleQId( name : String ) : QualifiedId = SimpleQId( SymbolId( SSymbol( name ) ) )

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with AUFNIRA
    )

    val x = Ints( "x" )
    val y = Ints( "y" )

    //  format: OFF
    val theTerms = Table[  TypedTerm[BoolTerm, Term], SatResponses ](

        ("typed term version", "Response"),
        (
            forall (Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= 1
            },
            UnSat()
        ),

        //  one var not bounded
        (
            (forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= 1
            }) & (x === 3),
            UnSat()
        ),

        //  one var not bounded
        (
            (forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= y
            }) & (x === 3),
            UnSat()
        ),

        //  indexed variable
        (
            forall(Ints("x1").indexed(1).symbol) {
                val x1 = Ints("x1").indexed(1)
                x1 + 2 <= 1
            },
            UnSat()
        ),

        //  nested forall
        (
            forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                forall(Ints("y1").symbol) {
                    val y1 = Ints("y1")
                    y1 >= 0 imply y1 >= x1
                }
            },
            Sat()
        ),

        //  exists and forall
        (
            exists(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                forall(Ints("y1").symbol) {
                    val y1 = Ints("y1")
                    y1 >= 0 imply y1 >= x1
                }
            },
            Sat()
        ),

        (
            forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                exists(Ints("y1").symbol) {
                    val y1 = Ints("y1")
                    y1 >= 0 imply y1 >= x1
                }
            },
            Sat()
        )
    )
    // format: ON

    for ( s ← theSolvers; ( xt, r ) ← theTerms ) {

        test( s"Using solver $s configured with ${s.configScript} assert quantified Term  ${show( xt.termDef )} -- should be ${show( r )}" ) {

            //  with using
            using( s ) {
                implicit solver ⇒
                    {
                        |=( xt ) flatMap
                            { _ ⇒ checkSat() }
                    }
            } shouldBe Success( r )
        }
    }
}
