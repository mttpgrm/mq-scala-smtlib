/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package configurations
package tests

import Configurations._
import solvers._
import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import resource._
import interpreters.SMTSolver.SolverResource
import interpreters.SMTSolver
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources
import parser.PredefinedParsers

/**
 * Check configurations for each solver
 */
class SolverConfigTests extends FunSuite with Matchers with TableDrivenPropertyChecks with Resources
        with PredefinedParsers {

    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.{ Command, SuccessResponse }
    import interpreters.SMTLIB2Interpreter

    /**
     *  Recursive evaluation of configuration script
     *  It stops at the first fail because:
     *    - either solver did not send back  String
     *    - or the string could not be parsed as a SuccessResponse
     */
    private def evalConfig(
        script : Seq[ Command ]
    )( implicit solver : SMTLIB2Interpreter ) : Try[ SuccessResponse ] = script match {

        case Nil ⇒ Success( SuccessResponse() )

        case c1 :: xc ⇒
            solver.eval( c1 ) flatMap
                { x ⇒ parseAsSuccess( x ) } flatMap
                { _ ⇒ ( evalConfig( xc )( solver ) ) }
    }

    /**
     * Pretty print the configuration
     */
    def pprintConfig( s : Seq[ Command ] ) =
        s map ( l ⇒ format( l ).layout ) mkString ( "\n\t", "\n\t", "\n" )

    //  list of (solver, List of configs to test)
    val configdSolver = Table(
        ( "Solver", "Configuration" ),
        (
            new Z3,
            List(
                QFLIASatOnlyConfig,
                QFLIASatModelConfig,
                QFLIAFullConfig,
                AUFNIRASatModelConfig,
                QFAUFLIAFullConfig
            )
        ),
        (
            new SMTInterpol,
            List(
                QFLIASatModelConfig,
                QFLIAFullConfig, QFAUFLIASatModelConfig,
                QFAUFLIAFullConfig
            )
        ),
        (
            new CVC4,
            List(
                QFLIASatModelConfig,
                QFAUFLIASatModelConfig
            )
        ),
        (
            new MathSAT,
            List(
                QFLIASatModelConfig,
                QFLIAFullConfig,
                QFAUFLIASatModelConfig,
                QFAUFLIAFullConfig
            )
        ),
        (
            new Yices,
            List(
                QFLIASatModelConfig,
                QFAUFLIASatModelConfig
            )
        )
    )

    import parser.SMTLIB2Syntax.{ SuccessResponse, Command, ProduceProofs, UnsupportedResponse }

    forAll ( configdSolver ) {
        ( s : Solver, lc : List[ Config ] ) ⇒
            {
                for ( cs ← lc ) {
                    test( s"Checking configuration ${pprintConfig( cs.configScript.toList )} for solver $s -- eval cmd" ) {

                        using( SMTSolver( s ) ) {
                            implicit solver ⇒
                                {
                                    //  smtlib package eval is used
                                    //  mathSAT does not support ProduceProofs

                                    if ( !s.isInstanceOf[ MathSAT ] ||
                                        !cs.options.contains( ProduceProofs( true ) ) ) {

                                        evalConfig( cs.configScript ) shouldBe Success( SuccessResponse() )

                                    } else
                                        evalConfig( cs.configScript ) should matchPattern {
                                            case Failure( _ ) ⇒
                                        }
                                }
                        }
                    }
                }
            }
    }
}
