/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package configurations
package tests

import parser.SMTLIB2Syntax.{
    Command,
    SetOption,
    SetLogic,
    SMTLIBOption,
    SSymbol,
    ProduceModels,
    ProduceProofs,
    ProduceInterpolants
}

/**
 * A config is provided as script which is a sequence of [[parser.SMTLIB2Syntax.Command]]
 *
 * This trait in only used for tests
 */
private[ tests ] trait Config {
    val configScript : Seq[ Command ]
    val options : Seq[ SMTLIBOption ]
}

/**
 * Helper to generate configs for standard usage
 */
private[ tests ] case class makeConfig(
        logic :   String,
        options : Seq[ SMTLIBOption ] = Seq()
) extends Config {

    lazy val configScript : Seq[ Command ] = {

        import scala.collection.mutable.ListBuffer
        val script = new ListBuffer[ Command ]()

        options.foreach { script += SetOption( _ ) }

        //  in most solvers the logic has to be set at the end

        script += SetLogic( SSymbol( logic ) )
        script.toSeq
    }
}

//  some useful configurations

private[ tests ] object EmptyConfig extends Config {
    val configScript = Seq[ Command ]()
    val options = Seq[ SMTLIBOption ]()

}

/**
 * Linear integer arithmetic
 */

//  check sat and get a model if sat
private[ tests ] object QFLIASatModelConfig extends makeConfig(
    "QF_LIA",
    Seq( ProduceModels( true ) )
)

//  check sat, get a model and interpolants
private[ tests ] object QFLIAFullConfig extends makeConfig( "QF_LIA", Seq( ProduceProofs( true ), ProduceModels( true ), ProduceInterpolants( true ) ) )

//  check sat only (no interpolants, no model)
private[ tests ] object QFLIASatOnlyConfig extends makeConfig( "QF_LIA" )

/**
 * Real arithmetic
 */

private[ tests ] object AUFNIRASatModelConfig extends makeConfig( "AUFNIRA", Seq( ProduceModels( true ) ) )

private[ tests ] object QFUFLIRASatModelConfig extends makeConfig( "QF_UFLIRA", Seq( ProduceModels( true ) ) )

// QF_AUFLIA, arrays

private[ tests ] object QFAUFLIASatModelConfig extends makeConfig( "QF_AUFLIA", Seq( ProduceModels( true ) ) )

private[ tests ] object QFAUFLIAFullConfig extends makeConfig( "QF_AUFLIA", Seq( ProduceProofs( true ), ProduceModels( true ), ProduceInterpolants( true ) ) )
