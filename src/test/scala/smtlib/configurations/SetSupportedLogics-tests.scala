/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package configurations
package tests

import Configurations._
import solvers._
import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success
}
import resource._
import interpreters.SMTSolver.SolverResource
import interpreters.SMTSolver
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources
import parser.PredefinedParsers

/**
 * Check that each solver actually supports its list of supported logics
 */
class SolverSupportedLogicsTests extends FunSuite with Matchers with TableDrivenPropertyChecks with Resources
        with PredefinedParsers {

    //  list of (solver, List of configs to test)
    val configdSolver = Table(
        "Solver",
        new Z3,
        new SMTInterpol,
        new CVC4,
        new MathSAT,
        new Yices
    )

    import parser.SMTLIB2Syntax.{ SuccessResponse, Command, ProduceProofs, UnsupportedResponse }

    forAll ( configdSolver ) {
        ( s : Solver ) ⇒
            {
                for ( lo ← s.supportedLogics ) {
                    test( s"[$s] should support logic ${lo} " ) {

                        using( SMTSolver( s ) ) {
                            implicit solver ⇒
                                {
                                    eval( makeConfig( lo ).configScript ) flatMap
                                        parseAsGeneralResponse shouldBe
                                        Success( SuccessResponse() )
                                }
                        }
                    }
                }
            }
    }
}
