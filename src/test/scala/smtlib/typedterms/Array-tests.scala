/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package typedterms
package tests

import org.scalatest.{ FunSuite, Matchers, NonImplicitAssertions }
import org.scalatest.prop.TableDrivenPropertyChecks
import typedterms.Commands
import interpreters.Resources
import theories.{
    Core,
    IntegerArithmetics,
    RealArithmetics,
    ArrayExInt,
    ArrayExBool,
    ArrayExReal,
    ArrayExOperators
}

/**
 * Test the typedDef component when building Array typedterms
 */
class ArrayTypeDefsTests
        extends FunSuite
        with TableDrivenPropertyChecks
        with Matchers
        with Core
        with IntegerArithmetics
        with ArrayExInt
        with ArrayExBool
        with ArrayExReal {

    override def suiteName = s"Check the typeDefs built using the Array constructors"

    import parser.SMTLIB2PrettyPrinter.format
    import typedterms.TypedTerm
    import parser.SMTLIB2Syntax.{ Term, QIdTerm }

    //  some useful variable and shortcuts
    val a1 = ArrayInt1( "a" )
    val a2 = ArrayInt2( "a" )
    val b1 = ArrayBool1( "a" )
    val b2 = ArrayBool2( "a" )
    val c1 = ArrayReal1( "a" )
    val c2 = ArrayReal2( "a" )

    test( s"Check the typeDefs produced by: $a1 -- Should be Set(((as a (Array Int Int )))))" ) {
        a1.typeDefs.map( format( _ ).layout ) shouldBe
            Set( ( "(as a (Array Int Int ) )" ) )
    }

    test( s"Check the typeDefs produced by: $a2 -- Should be Set(((as a (Array Int (Array Int Int ))))))" ) {
        a2.typeDefs.map( format( _ ).layout ) shouldBe
            Set( ( "(as a (Array Int (Array Int Int )) )" ) )
    }

    test( s"Check the typeDefs produced by: $b1-- Should be Set(((as a (Array Int Bool )))))" ) {
        b1.typeDefs.map( format( _ ).layout ) shouldBe
            Set( ( "(as a (Array Int Bool ) )" ) )
    }

    test( s"Check the typeDefs produced by: $b2 -- Should be Set(((as a (Array Int (Array Int Bool ))))))" ) {
        b2.typeDefs.map( format( _ ).layout ) shouldBe
            Set( ( "(as a (Array Int (Array Int Bool )) )" ) )
    }

    test( s"Check the typeDefs produced by: $c1 -- Should be Set(((as a (Array Int real )))))" ) {
        c1.typeDefs.map( format( _ ).layout ) shouldBe
            Set( ( "(as a (Array Int Real ) )" ) )
    }

    test( s"Check the typeDefs produced by: $c2 -- Should be Set(((as a (Array Int (Array Int real ))))))" ) {
        c2.typeDefs.map( format( _ ).layout ) shouldBe
            Set( ( "(as a (Array Int (Array Int Real )) )" ) )
    }
}

/**
 * Test the typeDef component for select TypedTerm
 */
class ArraySelectTests
        extends FunSuite
        with TableDrivenPropertyChecks
        with Matchers
        with Core
        with IntegerArithmetics
        with ArrayExInt
        with ArrayExBool
        with ArrayExReal
        with ArrayExOperators {

    override def suiteName = s"Check the typeDefs built with the select operator"

    import parser.SMTLIB2PrettyPrinter.format

    //  some useful variable and shortcuts
    val a1 = ArrayInt1( "a" )
    val a2 = ArrayInt2( "a" )
    val b1 = ArrayBool1( "a" )
    val b2 = ArrayBool2( "a" )
    val c1 = ArrayReal1( "a" )
    val c2 = ArrayReal2( "a" )

    test( s"Check the TypedTerm produced by: select 1 in $a1" ) {

        ( a1 select Ints( 1 ) ).typeDefs.map( format( _ ).layout ) shouldBe
            Set( ( "(as a (Array Int Int ) )" ) )

        format( ( a1 select 1 ).termDef ).layout shouldBe
            "(select a 1 ) "
    }

    test( s"Check the typeDefs produced by: select 1 in $a2" ) {

        ( a2 select 1 ).typeDefs.map( format( _ ).layout ) shouldBe
            Set( ( "(as a (Array Int (Array Int Int )) )" ) )

        format( ( a2 select 1 ).termDef ).layout shouldBe
            "(select a 1 ) "
    }

    test( s"Check the typeDefs produced by: select 1 in $b1" ) {

        ( b1 select 1 ).typeDefs.map( format( _ ).layout ) shouldBe
            Set( ( "(as a (Array Int Bool ) )" ) )

        format( ( b1 select 1 ).termDef ).layout shouldBe
            "(select a 1 ) "
    }

    test( s"Check the typeDefs produced by: select 1 in $b2" ) {

        ( b2 select 1 ).typeDefs.map( format( _ ).layout ) shouldBe
            Set( "(as a (Array Int (Array Int Bool )) )" )

        format( ( b2 select 1 ).termDef ).layout shouldBe
            "(select a 1 ) "
    }

    test( s"Check the typeDefs produced by: select 1 in $c1" ) {

        ( c1 select 1 ).typeDefs.map( format( _ ).layout ) shouldBe
            Set( ( "(as a (Array Int Real ) )" ) )

        format( ( c1 select 1 ).termDef ).layout shouldBe
            "(select a 1 ) "
    }

    test( s"Check the typeDefs produced by: select 1 in $c2" ) {

        ( c2 select 1 ).typeDefs.map( format( _ ).layout ) shouldBe
            Set( ( "(as a (Array Int (Array Int Real )) )" ) )

        format( ( c2 select 1 ).termDef ).layout shouldBe
            "(select a 1 ) "
    }

    test( s"Check the typeDefs produced by: select true|2.0 $a1 -- Should not typecheck" ) {
        //  format: OFF
        "ArrayInt1( \"a\" ) select Bool( true )"  shouldNot typeCheck
        "ArrayInt1( \"a\" ) select SReal( 2.0 )"   shouldNot typeCheck
        //  format: ON
    }
}

/**
 * Test the typeDef component for store TypedTerm
 */
class ArrayStoreTests
        extends FunSuite
        with TableDrivenPropertyChecks
        with Matchers
        with Core
        with IntegerArithmetics
        with ArrayExInt
        with ArrayExBool
        with ArrayExReal
        with ArrayExOperators {

    override def suiteName = s"Check the typeDefs built with the store operator"

    import parser.SMTLIB2PrettyPrinter.format

    //  some useful variable and shortcuts
    val a1 = ArrayInt1( "a" )
    val a2 = ArrayInt2( "a" )
    val a12 = ArrayInt1( "b" )
    val b1 = ArrayBool1( "a" )
    val b2 = ArrayBool1( "a" )
    val c1 = ArrayReal1( "a" )
    val c2 = ArrayReal1( "a" )
    val x = Ints( "x" )

    test( s"Check the TypedTerm produced by: store 1 at index 2 in $a1" ) {

        ( a1 store ( 2, 1 ) ).typeDefs.map( format( _ ).layout ) shouldBe
            Set( "(as a (Array Int Int ) )" )

        format( ( a1 store ( 2, 1 ) ).termDef ).layout shouldBe
            "(store a 2 1 ) "
    }

    test( s"Check the TypedTerm produced by: $a1(2) := 1" ) {

        ( a1( 2 ) := 1 ).typeDefs.map( format( _ ).layout ) shouldBe
            Set( ( "(as a (Array Int Int ) )" ) )

        format( ( a1( 2 ) := 1 ).termDef ).layout shouldBe
            "(store a 2 1 ) "

    }

    test( s"Check the TypedTerm produced by: $a1(2) := x" ) {

        ( a1( 2 ) := x ).typeDefs.map( format( _ ).layout ) shouldBe
            Set( ( "(as a (Array Int Int ) )" ), "(as x Int )" )

        format( ( a1( 2 ) := x ).termDef ).layout shouldBe
            "(store a 2 x ) "

    }

    test( s"Check the TypedTerm produced by: store 1 at index 2 in Array2* -- should not typecheck" ) {

        //  format: OFF
        "( ArrayInt2( \"a\" ) store ( 2, 1 ) )"      shouldNot typeCheck
        "( ArrayBool2( \"a\" )  store ( 2, 1 ) )"    shouldNot typeCheck
        "( ArrayReal2( \"a\" ) store ( 2, 1 ) )"     shouldNot typeCheck
        //  format: ON
    }

    test( s"Check the TypedTerm produced by: store 1 at index 2 in $b1" ) {

        ( b1 store ( 2, Bools( true ) ) ).typeDefs.map( format( _ ).layout ) shouldBe
            Set( ( "(as a (Array Int Bool ) )" ) )

        format( ( b1 store ( 2, Bools( false ) ) ).termDef ).layout shouldBe
            "(store a 2 false ) "

    }

    test( s"Check the TypedTerm produced by: store ($a2 store 1 2) at index 3 in $a12" ) {

        (
            a2 store (
                3,
                a12 store ( 1, 2 )
            )
        ).typeDefs.map( format( _ ).layout ) shouldBe
                Set( ( "(as a (Array Int (Array Int Int )) )" ), ( "(as b (Array Int Int ) )" ) )

        format(
            (
            a2 store (
                3,
                a12 store ( 1, 2 )
            )
        ).termDef
        ).layout shouldBe
            "(store a 3 (store b 1 2 ) ) "

    }
}

/**
 * Test the typeDef component for equal array TypedTerm
 */
class ArrayEqualTests
        extends FunSuite
        with TableDrivenPropertyChecks
        with Matchers
        with Core
        with IntegerArithmetics
        with ArrayExInt
        with ArrayExBool
        with ArrayExReal
        with ArrayExOperators
        with Resources {

    override def suiteName = s"Check the typeDefs built with the Array Equal operator"

    import parser.SMTLIB2PrettyPrinter.format

    //  some useful variable and shortcuts
    val a1 = ArrayInt1( "a" )
    val aa1 = ArrayInt1( "b" )
    val a2 = ArrayInt1( "a" )
    val b1 = ArrayBool1( "a" )
    val b2 = ArrayBool1( "a" )
    val c1 = ArrayReal1( "a" )
    val c2 = ArrayReal2( "a" )
    val x = Ints( "x" )

    test( s"Check the TypedTerm produced by: a === b" ) {

        ( a1 === aa1 ).typeDefs.map( format( _ ).layout ) shouldBe
            Set( ( "(as a (Array Int Int ) )" ), ( "(as b (Array Int Int ) )" ) )

        format( ( a1 === aa1 ).termDef ).layout shouldBe
            "(= a b ) "

    }

    test( s"Check the TypedTerm produced by: a === b with a and b of different type -- should not typecheck" ) {

        //  format: OFF
        "( ArrayInt1( \"a\" ) ===  ArrayInt2( \"b\" ))"   shouldNot typeCheck
        "( ArrayInt1 (\"a\") store ( 2, Bool(true) ) )"   shouldNot typeCheck
        "( ArrayInt2 (\"a\") store ( 2, 1 ) )"            shouldNot typeCheck
        "( ArrayInt1 (\"a\") store ( 2, SReal(3.0) ) )"   shouldNot typeCheck
        "( ArrayBool1 (\"a\") store ( 2, 1 ) )"           shouldNot typeCheck
        "( ArrayInt1 (\"a\") store ( 2, SReal(1.0) ) )"   shouldNot typeCheck
        "( ArrayReal2 (\"a\") select 1 === Ints(1) )"     shouldNot typeCheck
        //  format: ON
    }
}
