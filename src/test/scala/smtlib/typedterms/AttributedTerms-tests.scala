/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package typedterms
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ Core, IntegerArithmetics }

class AttributedTermTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core {

    override def suiteName = "Test form of attributed terms"

    import parser.SMTLIB2Syntax.{ Term }
    import theories.IntTerm
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.format

    val x = Ints( "x" )

    //  format: OFF
    val theTerms0 = Table[ String, TypedTerm[ IntTerm, Term ], List[ ( String, String ) ], String ](
        ( "expression"         , "raw term"   , "attributes"                , "Solver string" ) ,

        //  empty list should yield an Exception so we do not need the solver string
        ( "integer 0"          , Ints( 0 )             , List()                      , "" )     ,
        ( "1 + 2"              , Ints( 1 ) + Ints( 2 ) , List( ( "a" , "b" ) ) , "(! (+ 1 2 ) :a b ) " )
    )

    val theTerms1 = Table[ String, TypedTerm[ IntTerm, Term ], List[ ( String, String ) ], String ](
        ( "expression"         , "raw term" , "attributes"                                  , "Solver string" ) ,
        ( "integer variable x" , x          , List( ( "name" , "x_" ) )                     , "(! x :name x_ ) " ),
        ( "named variable x"   , x - 1      , List( ( "named" , "x_" ) )                    , "(! x :named x_ ) " ),
        ( "Addition"           , x + 1      , List( ( "a1" , "v1_" ) , ( "a2" , "v_2" ) )   , "(! (+ x 1 ) :a1 v1_ :a2 v_2 ) " )

    )
    //  format: ON

    val b = Bools( "b" )
    val c = Bools( "c" )

    //  format: OFF
    val theTerms2 = Table[ String, TypedTerm[ BoolTerm, Term ], List[ ( String, String ) ], String ](
        ( "expression",   "raw term",  "attributes",             "Solver string" ),
        ( " boolean b",    b,           List( ( "name", "x_" ) ), "(! b :name x_ ) " ),
        ( "And with attr", b & c,       List( ( "a1", "_1" ),
                                                ( "_2", "@#" ) ), "(! (and b c ) :a1 _1 :_2 @# ) "),
        //  empty list should yield an Exception so we do not need the solver string
        ( "true",          Bools( true ),  List(),                   "" ),
        ( "Or ",           b | c,          List(),                   "" ),
        ( "false",         Bools( false ), List(),                   "" )
    )
    //  format: ON

    for ( ( x, t, xa, s ) ← theTerms0 ++ theTerms1 ++ theTerms2 ) {
        if ( xa.isEmpty )
            test( s"Check string produced by: $x -- Should be exception as argument List is empty" ) {
                ( intercept[ Exception ] {
                    val x = t.withAttributes( xa.head, xa.tail : _* )
                } ).getMessage shouldBe "head of empty list"
            }
        else if ( xa.find( _._1 == "named" ).nonEmpty )
            test( s"Check string produced by: $x -- Should be exception as argument List contains named and we should use withNameAndAttributes" ) {
                ( intercept[ Exception ] {
                    val x = t.withAttributes( xa.head, xa.tail : _* )
                } ).getMessage shouldBe "requirement failed: Use withNameAndAttributes to set the name of a term"
            }
        else {
            test( s"Check string produced by: $x -- Should be $s" ) {
                format( t.withAttributes( xa.head, xa.tail : _* ).termDef ).layout shouldBe s
            }
        }
    }
}

class NamedTermTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core {

    override def suiteName = "Test form of named terms"

    import parser.SMTLIB2Syntax.{ Term }
    import theories.IntTerm
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.format

    val x = Ints( "x" )

    //  format: OFF
    val theTerms = Table[ TypedTerm[ IntTerm, Term ], String , String ](
        ("the term","name",     "expected solver term"),

        (Ints("0"), "zero", "(! 0 :named zero ) " ),
        (x + 1,     "P1", "(! (+ x 1 ) :named P1 ) ")
    )

    for ( ( t, name, s ) ← theTerms) {
        test( s"Check named term for term $t named $name -- Should be $s" ) {
            format( t.withNameAndAttributes(name).termDef ).layout shouldBe s
        }

        test( s"Get the name of named term ${ t.named(name).termDef} -- Should be $name" ) {
            t.withNameAndAttributes(name).nameDef shouldBe name
        }

        test( s"Get the name of term ${ t.named(name).termDef} -- Should be None" ) {

            "t.nameDef" shouldNot compile
        }
    }
}
