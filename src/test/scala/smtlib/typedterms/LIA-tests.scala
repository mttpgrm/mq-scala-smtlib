/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package typedterms
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ IntegerArithmetics, Core }
import interpreters.Resources

class SimpleLIATests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Commands with Resources {

    override def suiteName = "QF_LIA simple check-sat test suite"

    import scala.util.Success
    import parser.SMTLIB2Syntax.{ Term, Sat, SatResponses }
    import theories.PredefinedLogics.QF_LIA
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.format
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import theories.PredefinedLogics._

    val theSolvers = Table[ Solver ](
        "Solver", new Z3 with QF_LIA,
        new SMTInterpol with QF_LIA,
        new CVC4 with QF_LIA,
        new MathSAT with QF_LIA,
        new Yices with QF_LIA
    )

    val theTerms = Table[ Seq[ TypedTerm[ BoolTerm, Term ] ], SatResponses ](
        ( "Terms", "Expected response" ),

        //  1 < 2 should be Sat
        (
            Seq( Ints ( 1 ) < Ints ( 2 ) ),
            Sat()
        ),

        //  1 < 2 and 2 < 3, Sat
        (
            Seq( Ints ( 1 ) < Ints ( 2 ), Ints ( 2 ) <= Ints ( 3 ) ),
            Sat()
        ),

        //  x < 2 and 1 < x should be Sat
        (
            Seq(
                Ints( "x" ) < Ints( 2 ),
                Ints( 1 ) <= Ints( "x" )
            ),
                Sat()
        ),

        //  x == -4, should be Sat
        (
            Seq(
                Ints( "x" ) === Ints( -4 )
            ),
                Sat()
        )
    )

    for ( s ← theSolvers; ( xt, r ) ← theTerms ) {

        test( s"Using solver $s configured with ${s.configScript} to check sat for terms ${xt.map( x ⇒ format( x.termDef ).layout )} -- should be SAT" ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( xt : _* ) shouldBe Success( Sat() )
                    }
            }
        }
    }

}

/**
 * We are using the implicits and operators to build complex
 * TypedTerms and check sat
 */
class ArithmeticOperatorsTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Commands with Resources {

    override def suiteName = "QF_LIA simple terms check-sat"

    import scala.util.Success
    import parser.SMTLIB2Syntax.{ Sat, UnSat }
    import theories.PredefinedLogics.QF_LIA
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_LIA,
        new SMTInterpol with QF_LIA,
        new CVC4 with QF_LIA,
        new MathSAT with QF_LIA,
        new Yices with QF_LIA
    )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    for ( s ← theSolvers ) {

        test( s"[$s] Addition: x + 1 = 3  -- should be SAT" ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        isSat( x + 1 === 3 ) shouldBe Success( Sat() )
                    }
            }
        }

        test( s"[$s] 3-ary Addition: x + 1 + y = 3  -- should be SAT" ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        isSat( x + 1 + y === 3 ) shouldBe Success( Sat() )
                    }
            }
        }

        test( s"[$s] Addition with 3 operands:  x + 1 + y = 3  -- should be SAT" ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        isSat( x + 1 + y === 3 ) shouldBe Success( Sat() )
                    }
            }
        }

        test( s"[$s] Subtraction:  x - 1 = 3  -- should be SAT" ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        isSat( x - 1 === 3 ) shouldBe Success( Sat() )
                    }
            }
        }

        test( s"[$s] Unary Subtraction:  - y > 3 and x + y == -1 -- should be SAT" ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        isSat( ( -y > 3 ) & ( x + y === -1 ) ) shouldBe Success( Sat() )
                    }
            }
        }

        test( s"[$s] Unsat test: x < 1 and x >= 2 -- should be UNSAT" ) {
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        isSat( x < 1 & x >= 2 ) shouldBe Success( UnSat() )
                    }
            }
        }

        test( s"[$s] Multiplication test: x * 2 < 1 -- should be SAT" ) {
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        isSat( x * 2 < 1 ) shouldBe Success( Sat() )
                    }
            }
        }

        test( s"[$s] Unsat test: x == 2,  y > 2, y == x + 1, z == 3, z == x should be UNSAT" ) {
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        isSat(
                            x === 2,
                            y >= 2,
                            y === x + 1,
                            z === 3,
                            z === x
                        ) shouldBe Success( UnSat() )
                    }
            }
        }

        test( s"[$s] Sat test: i + j + 1 == 2 -- should be SAT" ) {
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        val i = Ints( "i" )
                        val j = Ints( "j" )
                        isSat(
                            i + j + 1 === 2
                        ) shouldBe Success( Sat() )
                    }
            }
        }
    }
}

class AssertBoolTermTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Commands with Resources {

    import scala.util.Success
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat, SatResponses }
    import theories.BoolTerm
    import theories.PredefinedLogics.QF_LIA
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._

    val theSolvers = Table[ Solver ](
        "Solver", new Z3 with QF_LIA,
        new SMTInterpol with QF_LIA,
        new CVC4 with QF_LIA,
        new MathSAT with QF_LIA,
        new Yices with QF_LIA
    )

    val theTerms = Table[ TypedTerm[ BoolTerm, Term ], SatResponses ](
        ( "Terms", "Response" ),
        ( True(), Sat() ),
        ( False(), UnSat() )
    )

    for ( s ← theSolvers; ( t, r ) ← theTerms ) {

        test( s"Using solver $s  to check sat for terms ${format( t.termDef ).layout} -- should be ${format( r ).layout}" ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        isSat( t ) shouldBe Success ( r )
                    }
            }
        }
    }
}
