/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package typedterms
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ Core, IntegerArithmetics }
import interpreters.Resources

class GetInfoCmdTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Resources {

    override def suiteName = "get-info command test suite"

    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2Syntax.{ Raw, GetInfoResponses, GetInfoResponseSuccess, GetInfoResponseError }
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.format
    import interpreters.SMTSolver
    import configurations.Configurations._
    import solvers._
    import theories.PredefinedLogics.QF_LIA
    import theories.tests.PPrinter._
    import parser.SMTLIB2Parser
    import parser.Implicits._

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    val theSolvers = Table[ Solver ](
        "Solver",
        new Z3 with QF_LIA,
        new SMTInterpol with QF_LIA,
        new CVC4 with QF_LIA,
        new MathSAT with QF_LIA,
        new Yices with QF_LIA
    )

    val parseGetInfoResponse = SMTLIB2Parser[ GetInfoResponses ]

    //  <get-info> test strings
    //  format: OFF
    val getInfoTests = Table[ String, Solver => Boolean](
        ( "input string?"           , "is get-info : yes/no" ) ,
        ("(get-info :all-statistics)", { x  => if (x.isInstanceOf[MathSAT]) false else true }),
        ("(get-info :assertions-stack-levels)", _ => false),
        ("(get-info :authors)", _ => true),
        ("(get-info :error-behaviour)",{ x  => if (x.isInstanceOf[MathSAT]) true else false }),
        ("(get-info :reason-unknown)", _ => false),
        ("(get-info :name)", _ => true),
        ("(get-info :version)", _ => true),

        ("(get-info :mykwd)", _ => false),

        ( """|
             | (
             |  get-info
             |    :authors
             |      )
             |
             |""".stripMargin     , _ => true ),
         ( """|
              | (
              |  get-inf
              |    :authors
              |      )
              |
              |""".stripMargin     , _ => false ),
          ( """|
               | (
               |  get-info
               |    x
               |      )
               |
               |""".stripMargin     , _ => false )
    )
    //  format: ON

    for ( s ← theSolvers; ( xt, r ) ← getInfoTests ) {

        test( s"Using solver $s configured with ${pprintConfig( s )} with $xt -- ${if ( r( s ) ) "should succeed" else "should fail"}" ) {

            //  with using
            using( SMTSolver( s ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        eval ( Seq( Raw( xt ) ) ) match {

                            case Success( solverResponse ) ⇒

                                logger.info( "Solver returned : {}", solverResponse )
                                parseGetInfoResponse ( solverResponse ) should matchPattern {

                                    case Success( GetInfoResponseSuccess( _ ) ) if r( s ) ⇒
                                    case Success( GetInfoResponseError( _ ) ) if !r( s )  ⇒
                                    case Failure( _ ) if !r( s )                          ⇒
                                }

                            case Failure( e ) if !r( s ) ⇒
                        }

                    }
            }
        }
    }
}
