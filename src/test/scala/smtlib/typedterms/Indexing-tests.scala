/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package typedterms
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ IntegerArithmetics, RealArithmetics, Core, ArrayExInt, ArrayExOperators, BitVectors }
import interpreters.Resources

/**
 * Remove indices in terms
 */
class DeIndexingTests
        extends FunSuite
        with TableDrivenPropertyChecks
        with Matchers
        with IntegerArithmetics
        with RealArithmetics
        with ArrayExInt
        with ArrayExOperators
        with QuantifiedTerm
        with Core
        with BitVectors
        with Resources {

    override def suiteName = "De-Indexing typedTerms test suite"

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory
    import scala.util.Success
    import parser.SMTLIB2Syntax.{ Term }
    import parser.SMTLIB2Syntax.{ QualifiedId, SSymbol }
    import parser.Implicits._
    import parser.Analysis
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.show
    import interpreters.SMTSolver

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    val r1 = Reals( "r1" )
    val r2 = Reals( "r2" )

    val xbv = BVs( "x", 16 )
    val ybv = BVs( "y", 32 )
    val y1bv = ybv indexed 1

    val a1 = ArrayInt1( "a1" )

    //  format: OFF
    val theTerms = Table[ TypedTerm[ BoolTerm, Term ], TypedTerm[ BoolTerm, Term ] ](
        ( "Indexed Term"                    , "Expected Unindexed" ) ,

        //  int terms
        ( x.indexed(1) === 1 & y === z      , x === 1 & y === z )    ,
        ( x.indexed(10) + 1 <= x.indexed(0) , x + 1 <= x )           ,

        //  real terms
        ( r1.indexed(2) + 1.0 <= r2         , r1 + 1.0 <= r2  )      ,

        // Bit vectors
        (y1bv === BVs(0,32),  ybv === BVs(0,32)),

        //  let terms
        (
           let {
               val x1 = BoundedVar("x1"     , x.indexed(2) + 1)
               x1 <= 6
           }                                ,
           let {
               val x1 = BoundedVar("x1"     , x + 1)
               x1 <= 6
           }
       )                                    ,

       //   arrays
       ( a1.indexed(2)(x.indexed(0)) === y              , a1(x) === y)  ,
       ( ( a1.indexed(2)(x) := 0 ) === a1.indexed(3)    , ( a1(x) := 0 ) === a1)
    )
    //  format: ON

    for ( ( xt, res ) ← theTerms ) {

        test( s"Unindexing ${show( xt.termDef )}  should yield ${show( res.termDef )}" ) {

            logger.info( "indexed term is :" + xt.termDef )
            xt.unIndexed shouldBe res

        }
    }
}

/**
 * Add/change indices in terms
 */
class IndexingTests
        extends FunSuite
        with TableDrivenPropertyChecks
        with Matchers
        with IntegerArithmetics
        with RealArithmetics
        with ArrayExInt
        with ArrayExOperators
        with QuantifiedTerm
        with Core
        with BitVectors
        with Resources {

    override def suiteName = "Indexing typedTerms test suite"

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory
    import scala.util.Success
    import parser.SMTLIB2Syntax.{ Term }
    import parser.SMTLIB2Syntax.{ QualifiedId, SMTLIB2Symbol, SSymbol, ISymbol }
    import parser.Implicits._
    import parser.Analysis
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.show
    import interpreters.SMTSolver

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    val r1 = Reals( "r1" )
    val r2 = Reals( "r2" )

    val xbv = BVs( "x", 16 )
    val ybv = BVs( "y", 32 )
    val y1bv = ybv indexed 1

    val a1 = ArrayInt1( "a1" )

    //  format: OFF
    val theTerms = Table[ TypedTerm[ BoolTerm, Term ], SMTLIB2Symbol ==> Int,  TypedTerm[ BoolTerm, Term ] ](
        ( "Indexed Term",       "Index map",                     "Expected Unindexed" ) ,

        //  true or false should not be indexed
        ( True()                        , Map(SSymbol("true") -> 2) ,       True()),
        ( False()                       , Map(SSymbol("true") -> 2) ,       False()),
        //  no index map
        ( x.indexed(1) === 1 & y === z , Map(),  x.indexed(1) === 1 & y === z ),

        ( x.indexed(1) === 1 & y.indexed(2) === z , Map( ISymbol("y",2) -> 3),  x.indexed(1) === 1 & y.indexed(3) === z ),
        ( x.indexed(1) === 1 & y.indexed(3) === z , Map( SSymbol("y") -> 0),  x.indexed(1) === 1 & y.indexed(3) === z ),

        //  no index to index
        ( x + 1 <= x , Map(SSymbol("x") -> 1), x.indexed(1) + 1 <= x.indexed(1) ),
        ( x + 1 <= y , Map(SSymbol("x") -> 1, SSymbol("y") -> 2), x.indexed(1) + 1 <= y.indexed(2) ),

        //  re-index
        ( r1.indexed(2) + 1.0 <= r2,    Map(ISymbol("r1",2) -> 3)  ,     r1.indexed(3) + 1.0 <= r2  ),
        (
           let {
               val x1 = BoundedVar("x1"     , x.indexed(2) + 1)
               x1 <= 6
           },
           Map( ISymbol("x",2) -> 4),
           let {
               val x1 = BoundedVar("x1"     , x.indexed(4) + 1)
               x1 <= 6
           }
       ),

       (
          let {
              val x1 = BoundedVar("x1"     , x + 1)
              x1 <= 6
          },
          Map( SSymbol("x1") -> 2),
          let {
              val x1 = BoundedVar("x1"     , x + 1)
              x1 <= 6
          } indexedBy Map(SSymbol("x1")->2)
      ),

       //   arrays
       ( a1(x) === y,   Map(SSymbol("a1")->2, SSymbol("y") -> 1),   a1.indexed(2)(x) === y.indexed(1))
    )
    //  format: ON

    for ( ( xt, indexMap, res ) ← theTerms ) {

        test( s"(Re)-Indexing ${show( xt.termDef )} with $indexMap should yield ${show( res.termDef )}" ) {

            logger.info( "indexed term is :" + show( xt.indexedBy( indexMap ).termDef ) )
            xt.indexedBy( indexMap ) shouldBe res

        }
    }
}

/**
 * Add/change indices in terms
 */
class IndexedByTests
        extends FunSuite
        with TableDrivenPropertyChecks
        with Matchers
        with IntegerArithmetics
        with RealArithmetics
        with ArrayExInt
        with ArrayExOperators
        with QuantifiedTerm
        with Core
        with BitVectors
        with Resources {

    override def suiteName = "IndexedBy typedTerms test suite"

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory
    import scala.util.Success
    import parser.SMTLIB2Syntax.{ Term }
    import parser.SMTLIB2Syntax.{ QualifiedId, SMTLIB2Symbol, SSymbol, ISymbol, QIdTerm }
    import parser.Implicits._
    import parser.Analysis
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.show
    import interpreters.SMTSolver

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    val r1 = Reals( "r1" )
    val r2 = Reals( "r2" )

    val xbv = BVs( "x", 16 )
    val ybv = BVs( "y", 32 )
    val y1bv = ybv indexed 1

    val a1 = ArrayInt1( "a1" )

    //  format: OFF
    val theTerms = Table[ TypedTerm[ BoolTerm, Term ],  TypedTerm[ BoolTerm, Term ] ](
        ( "Indexed Term",       "Expected Unindexed" ) ,

        //  no index map
        ( x.indexedBy(Map(SSymbol("x") -> 1) )  === 1 & y === z , x.indexed(1) === 1 & y === z ),
        ( (x === 1 & y === z).indexedBy({case _ => 0}) ,  x.indexed(0) === 1 & y.indexed(0) === z.indexed(0) ),

        //  partial indexing
        ( x.indexedBy( {case _:SMTLIB2Symbol => 0}) + 1 <= x ,  x.indexed(0) + 1 <= x ),
        ( (x + 1).indexedBy({case _:SMTLIB2Symbol => 1}) <= y , x.indexed(1) + 1 <= y ),
        ( ((x + 2).indexedBy(Map( SSymbol("x")-> 32))).indexedBy({ case _ => 1}) <= y , x.indexed(1) + 2 <= y ),

        //  re-index
        ( r1.indexedBy({case _ => 2}) + 1.0 <= r2,    r1.indexed(2) + 1.0 <= r2  ),

       //   arrays
       ( a1.indexedBy({case _ => 2})(x.indexedBy({case _ => 0})) === y,   a1.indexed(2)(x.indexed(0)) === y)
    )
    //  format: ON

    for ( ( xt, res ) ← theTerms ) {

        test( s"Using IndexedBy ${show( xt.termDef )} should equal ${show( res.termDef )}" ) {

            logger.info( "indexed term is :" + xt.termDef )
            xt shouldBe res

        }
    }
}
