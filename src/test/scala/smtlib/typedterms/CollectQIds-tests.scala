/*
 * This file is part of MQ-Scala-SMTLIB2.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB2 is distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB2. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package typedterms
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ IntegerArithmetics, RealArithmetics, Core, ArrayExInt, ArrayExOperators }
import interpreters.Resources

/**
 * Collect QualifiedId that are free variables
 */
class CollectQIdsTests
        extends FunSuite
        with TableDrivenPropertyChecks
        with Matchers
        with IntegerArithmetics
        with RealArithmetics
        with ArrayExInt
        with ArrayExOperators
        with QuantifiedTerm
        with Core
        with Resources {

    override def suiteName = "CollectQIds free variables test suite"

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory
    import scala.util.Success
    import parser.SMTLIB2Syntax.{ Term }
    import parser.SMTLIB2Syntax.{ QualifiedId, SimpleQId, SymbolId, SSymbol }
    import parser.Implicits._
    import parser.Analysis
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.format
    import interpreters.SMTSolver

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    //  create a solver SortedQId, type Ints, from a string
    def toSimpleQId( name : String ) : QualifiedId = SimpleQId( SymbolId( SSymbol( name ) ) )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    val r1 = Reals( "r1" )
    val r2 = Reals( "r2" )
    // val a1 = ArrayInt1( "a1" )

    //  format: OFF
    val theTerms = Table[ TypedTerm[ BoolTerm, Term ], Set[ String ] ](
        ( "Terms", "Expected response" ),

        ( x === 1 & y === z,        Set( "x", "y", "z" ) ),
        ( x + 1 <= x,               Set( "x" ) ),
        ( r1 + 1.0 <= r2,           Set("r1", "r2") ),
        (
           let {
               val x1 = BoundedVar("x1", x + 1)
               x1 <= 6
           },                       Set("x")
       ),

       (
          let {
              val x1 = BoundedVar("x1", x + 1)
              let {
                  val x2 = BoundedVar("x2", y + x1)
                  x2 <= x + 2
              }
          },                       Set("x", "y")
      )

    )
    //  format: ON

    for ( ( xt, r ) ← theTerms ) {

        test( s"Collect the Qualified Ids in ${format( xt.termDef ).layout} -- should be $r" ) {

            logger.info( "term is :" + xt.termDef )
            Analysis( xt.termDef ).ids shouldBe ( r map toSimpleQId )

        }
    }
}
