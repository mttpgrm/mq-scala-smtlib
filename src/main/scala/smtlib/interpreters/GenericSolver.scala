/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package interpreters

import solvers.Solver

/**
 * A generic solver process that accepts commands and returns command responses
 */
private[ smtlib ] class GenericSolver( val s : Solver ) extends SMTLIB2Interpreter {

    import scala.util.{
        Try,
        Success,
        Failure
    }
    import org.bitbucket.franck44.expect.Expect

    //  logger

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    //  Expect

    //  create an Expect object running the solver
    private val solverTerminal = Expect( s.executable, s.args.toList )

    //  Evaluator

    import scala.concurrent.duration._
    import parser.SMTLIB2Syntax.{ Command, ExitCmd }

    def eval[ C <: Command ](
        cmd :     C,
        timeout : Option[ Duration ] = None
    ) : Try[ String ] = {

        import parser.SMTLIB2PrettyPrinter.format
        import parser.SMTLIB2Syntax.{
            EchoCmd,
            StringLiteral
        }
        import scala.util.matching.Regex

        cmd match {

            //  if cmd is exit, send and return Success(emptyString). No Expect
            //  should follow as the solver is dead
            case ExitCmd() ⇒
                logger.info( "Sending Exit command. No expect will follow" )

                solverTerminal.send( format( cmd ).layout ) match {

                    case Success( _ ) ⇒ Success( "Solver exit" )

                    case Failure( e ) ⇒ Failure( e )
                }

            //  otherwise forward command to solver
            case _ ⇒

                //  the solver echo command
                val echoCmd : Command = EchoCmd( StringLiteral( s.cmdSentinelleString ) )

                //  the solver expected prompt after successful command
                val prompt : Regex = s.expectedPromptAfterCommand

                logger.info( "Entering eval" )

                /*
                 * Try to evaluate the cmd within the time bound
                 * @note add a \n as for a few solver (e.g. CVC4)  this is needed
                 *
                 * The evaluation consists in 3 phases:
                 * 1.  send the cmd to the solver terminal-like processing
                 * 2.  send the sentinelle echo Command
                 * 3.  expect a response (within timeout): a string preceeding the prompt
                 *
                 * If any of the preceeding steps fails, a Failure is generated and
                 * propagated (by the flatMap). Otherwise, the Success(_) result of a
                 * command is fed to the next step.
                 *
                 * @note It uses the fact that Try is a monad. Assuming that f returns
                 * a Try[something], Success(v) flatMap f is mapped to Success(f(v))
                 * and Failure(ex) flatMap f to Failure(ex)
                 *
                 */

                //  send the cmd to the solver. SolverTerminal.send returns a Try[Unit]
                solverTerminal.send( format( cmd ).layout + "\n" ) flatMap
                    //
                    //  send the echo sentinelle command
                    //
                    ( _ ⇒ solverTerminal.send( format( echoCmd ).layout + "\n" ) ) flatMap
                    //
                    //  wait for the response, a Try[String], and return it
                    //
                    ( _ ⇒ solverTerminal.expect(
                        prompt,
                        timeout.getOrElse( s.timeoutPerEval )
                    ) )
        }
    }

    /** Kill (hopefully nicely) the external process running the solver */
    def destroy() : Unit = {

        solverTerminal.destroy()
        logger.trace( "solverTerminal (Expect object) destroyed" )

    }
}
