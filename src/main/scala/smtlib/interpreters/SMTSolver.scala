/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package interpreters

/**
 *  Provide an SMT solver with an optional configuration
 */
object SMTSolver {

    import scala.util.{
        Try,
        Success,
        Failure
    }

    //  logger

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    //  Resource management

    import resource._

    /**
     * Provide an implicit object to close un-used resources
     */
    implicit object SolverResource extends Resource[ Try[ ExtendedSMTLIB2Interpreter ] ] {

        /** Kills the process of the interpreter */
        def close( r : Try[ ExtendedSMTLIB2Interpreter ] ) : Unit = r match {

            case Success( s ) ⇒
                logger.info( "Destroying ExtendedSMTLIB2Interpreter" )
                s.destroy()

            case Failure( _ ) ⇒
                logger.info( "No ExtendedSMTLIB2Interpreter to destroy" )
                ()

        }
    }

    //  Config evaluator and response parser

    import parser.{ SMTLIB2Parser, PredefinedParsers }
    import parser.SMTLIB2Syntax.{
        SuccessResponse,
        UnsupportedResponse,
        ErrorResponse,
        GeneralResponse,
        Command
    }
    import parser.SMTLIB2PrettyPrinter.show

    object usefulParser extends PredefinedParsers
    import usefulParser.parseAsGeneralResponse

    /**
     * recursive evaluation of configuration script
     * stops t the first fail or error/unsupported respoinse from solver.
     * - either solver did not send back String
     * - or the command produec an error
     *
     * Note that the solver's reponse is parsed as GeneralResponse but the
     * negative instances of Generalresponse (Unsupported, Error) are converted
     * into failures.
     */
    private def evalConfig(
        script : Seq[ Command ],
        solver : ExtendedSMTLIB2Interpreter
    ) : Try[ SuccessResponse ] = script match {

        case Nil ⇒ Success( SuccessResponse() )

        case c1 :: xc ⇒
            ( solver.eval( c1 ) flatMap parseAsGeneralResponse ) match {

                case Success( SuccessResponse() ) ⇒ evalConfig( xc, solver )

                case Success( UnsupportedResponse() ) ⇒

                    Failure(
                        InterpreterUnsupportedException( s"Command ${show( c1 )} unsupported" )
                    )

                case Success( ErrorResponse( e ) ) ⇒
                    Failure( InterpreterErrorException(
                        s"Command ${show( c1 )} produced error ${show( ErrorResponse( e ) )}"
                    ) )

                case Failure( e ) ⇒ Failure( e )
            }
    }

    //  Creation of solver and configuration

    import solvers.Solver

    /**
     * Create an interpreter based on a solver.
     *
     * @tparam T      A class (e.g. Z3, SMTInterpol) that extends [[solvers.Solver]]
     * @param  s      An actual solver
     *
     * @return        An SMTLIB2 interpreter.
     */
    def apply[ T <: Solver ]( s : T ) : Try[ ExtendedSMTLIB2Interpreter ] =

        try {

            logger.trace( "trying to get a GenericSolver" )

            /*
             * Get a generic solver extended with GetDeclCmd capability
             * This enables to retrieve the variables declared and their types.
             */
            val solver = new GenericSolver( s ) with ExtendedSMTLIB2Interpreter

            logger.trace( "got a GenericSolver" )

            /*
             * Configure the solver to printSuccess i.e. provide a response
             * for every command and then configure according to Config c
             * build a stream that will lazily evaluate the cmd one by one
             */
            logger.trace( "Defined the stream of responses" )

            /*
             * Check result: if one command unsuccessful fail and return
             * list of reponses; otherwise return the solver.
             * solver.eval is used as we cannot access smtlib package
             * eval yet (solver not created)
             */

            evalConfig( s.configScript, solver ) match {

                case Success( SuccessResponse() ) ⇒

                    // the config was successfully set
                    logger.info( "solver configured successfully" )

                    Success( solver )

                case Failure( r ) ⇒

                    //  One configuration item failed
                    logger.error( "solver could not be configured" )

                    //  destroy the process
                    solver.destroy()

                    Failure( new Exception( s"Solver returned $r" ) )

            }

        } catch {

            case e : Throwable ⇒

                logger.error( "Exception while trying to get a solver: " + e )

                Failure( e )

        }
}
