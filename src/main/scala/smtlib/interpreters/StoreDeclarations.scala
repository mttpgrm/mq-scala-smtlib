/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package interpreters

/**
 * Provide extended interface for an SMTLIB2 v2.5 compliant interpreter
 *
 * This interface adds a GetDeclCmd command to obtain the variable
 * declarations including the type.
 */
trait ExtendedSMTLIB2Interpreter extends SMTLIB2Interpreter {

    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2Syntax.{
        SMTLIB2Symbol,
        SortedQId,
        Command,
        GetDeclCmdResponse,
        SuccessResponse,
        PopCmd,
        PushCmd,
        GetDeclCmd,
        DeclareFunCmd,
        FunDecl,
        SSymbol,
        SymbolId
    }
    import scala.concurrent.duration._
    import parser.SMTLIB2PrettyPrinter.format
    import parser.PredefinedParsers

    object parsers extends PredefinedParsers
    import parsers._

    /*
     * Stack of declarations on the solver.
     * Every time a Command that declares a variables is pushed we store the
     * type of the declared term. Push/pop are allowed and the stack is updated
     * accordingly.
     */
    private[ smtlib ] val context =
        scala.collection.mutable.Stack(
            scala.collection.mutable.Set[ SortedQId ]()
        )

    /**
     * Add get-declarations capability to the solver
     *
     * This command has a timeout so will terminate.
     *
     * @param cmd       The command to evaluate
     * @param timeout   The timeout in seconds
     *
     * @return          Either a Success or Failure.
     *
     */
    abstract override def eval[ C <: Command ](
        cmd :     C,
        timeout : Option[ Duration ] = None
    ) : Try[ String ] = {
        import com.typesafe.scalalogging.Logger
        import org.slf4j.LoggerFactory

        //  logger
        val logger = Logger( LoggerFactory.getLogger( "ExtendedSMTLIB2Interpreter.eval" ) )

        cmd match {

            //  This command is not provided by an SMTSolver but by this trait
            case GetDeclCmd() ⇒

                logger.info( "GetDeclCmd command with no call to solver" )

                // collect all the declarations on the stack and union them
                val declarations = context.reduceLeft( _ ++ _ ).toList
                Success( format( GetDeclCmdResponse( declarations ) ).layout )

            //  Declare some simple variables (Ints, Bools, Reals, ArraysEx).
            //  If command is successful we update the context (stack).
            case DeclareFunCmd( FunDecl( e : SMTLIB2Symbol, List(), sort ) ) ⇒

                super.eval( cmd, timeout ) match {

                    //  declare command evaluated successfully by the solver
                    case Success( r ) ⇒

                        // add declaration to context
                        context.top += SortedQId( SymbolId( e ), sort )
                        Success( r )

                    //  Failure
                    case Failure( f ) ⇒ Failure( f )
                }

            //  Declare functions (non constant).
            //  This will go through to the solver but the declaration is not
            //  stored in the context
            case DeclareFunCmd( FunDecl( e : SMTLIB2Symbol, _, sort ) ) ⇒

                // a function is declared. Store in context not supported yet
                logger.warn( "Declaration of non-const term. Cannot be added to context" )
                super.eval( cmd, timeout )

            //  Pop command
            case p @ PopCmd( x ) ⇒

                logger.info( "Evaluate Pop({}) command ", x )

                //  Send pop command and collect the result as a Try[String]
                //  Check that result is Success before updating context.
                val r : Try[ String ] = super.eval( p, timeout )

                ( r flatMap parseAsGeneralResponse ) match {

                    case Success( SuccessResponse() ) ⇒
                        logger.info( "Pop({}) command successfully evaluated", x )
                        //  pop x elements
                        Range( 0, x.toInt ).foreach { _ ⇒ context.pop() }
                        r

                    case Success( e ) ⇒
                        logger.info( "Pop({}) command failed {}", e )
                        Failure( new Exception( "Pop command failed " + e ) )

                    case Failure( e ) ⇒
                        logger.info( "Pop({}) command failed {}", e )
                        Failure( e )
                }

            //      Push command
            case p @ PushCmd( x ) ⇒

                logger.info( "Evaluate Push({}) command ", x )

                //  Send push command and collect result as a Try[String]
                //  Check that result is Success before updatting context.
                val r : Try[ String ] = super.eval( p, timeout )

                ( r flatMap parseAsGeneralResponse ) match {

                    case Success( SuccessResponse() ) ⇒
                        logger.info( "Push({}) command successfully evaluated", x )
                        //  push x empty sets on the stack
                        Range( 0, x.toInt ).foreach {
                            _ ⇒
                                context.push(
                                    scala.collection.mutable.Set[ SortedQId ]()
                                )
                        }
                        r

                    case Success( e ) ⇒
                        logger.info( "Push({}) command failed {}", e )
                        Failure( new Exception( "Push command failed " + e ) )

                    case Failure( e ) ⇒
                        logger.info( "Push({}) command failed {}", e )
                        Failure( e )
                }

            // If the command is not monitored, it goes directly to the solver
            case _ ⇒

                logger.info( "command {} is forwarded to the solver ", cmd )
                super.eval( cmd, timeout )
        }
    }
}
