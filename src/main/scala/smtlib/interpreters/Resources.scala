/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package interpreters

trait Resources {

    import resource._
    import scala.util.{ Try, Success, Failure }
    import interpreters.{ SMTLIB2Interpreter }
    import interpreters.SMTSolver.SolverResource

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    /**
     * Provide a method to obtain a solver resource, use it, and
     * safely release it at the end.
     *
     * @param r  Typically an [[interpreters.SMTSolver]] call of the
     *           form `SMTSolver(Z3)` or any other available solver.
     *
     * @param f  A map that associates a computation with an
     *           <strong>implicit</strong> solver.
     *
     */
    def using(
        r : ⇒ Try[ ExtendedSMTLIB2Interpreter ]
    )(
        f : ⇒ ( ExtendedSMTLIB2Interpreter ⇒ Unit )
    ) : Unit = {

        /*
         * Get a managed Generic solver to make sure we release the resources
         * (processes) when not needed any more
         */
        for ( s ← managed( r ) ) {

            val solver = s.get

            //  now evaluate the computation by applying f to the managed solver
            f( solver )
        }
    }

    /**
     * Provide an implicit object to close un-used resources
     */
    implicit object SolverResource2 extends Resource[ ExtendedSMTLIB2Interpreter ] {

        /** Kills the process of the interpreter */
        def close( r : ExtendedSMTLIB2Interpreter ) : Unit = {

            logger.info( "Destroying ExtendedSMTLIB2Interpreter" )
            r.destroy()
        }
    }

    /**
     * Monadic version of using.
     *
     * @param r  A solver e.g. 'new Z3 with QF_LIA'
     *
     * @param f  A map that associates a solver computation with an
     *           <strong>implicit</strong> solver.
     *
     * @todo     Log the Seq of Throwables in x when Left non empty
     */
    def using[ S <: solvers.Solver, T ](
        s : S
    )(
        f : ⇒ ( ExtendedSMTLIB2Interpreter ⇒ Try[ T ] )
    ) : Try[ T ] = {

        import resource._

        SMTSolver( s ) flatMap
            {
                r ⇒
                    logger.info( s"Solver $s created and configured" )
                    ( managed( r ) map f ).either match {

                        //  managed failed if x.nonEmpty
                        case ExtractedEither( Left( x ) ) if x.nonEmpty ⇒
                            Failure( x.head )

                        //  otherwise return the result of f
                        case ExtractedEither( Right( x ) ) ⇒ x
                    }
            }
    }
}
