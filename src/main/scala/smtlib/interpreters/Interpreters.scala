/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package interpreters

/**
 * Exception triggered when the solver response is (error ...)
 */
case class InterpreterErrorException( msg : String ) extends Exception( msg )

/**
 * Exception triggered when the solver response is unsupported
 */
case class InterpreterUnsupportedException( msg : String ) extends Exception( msg )

/**
 * Standard interface for an SMTLIB2 v2.5 compliant interpreter
 */
trait SMTLIB2Interpreter {

    import scala.util.Try
    import parser.SMTLIB2Syntax.Command
    import scala.concurrent.duration._

    /**
     * Send a command to an SMTLIB2 compliant SMTSolver and get a response.
     *
     * This command has a timeout so will terminate.
     *
     * @param cmd       The command to evaluate
     * @param timeout   The timeout in seconds
     *
     * @return          Either a Success with the solver raw (String)
     *                  response or Failure.
     *
     */
    def eval[ C <: Command ]( cmd : C, timeout : Option[ Duration ] = None ) : Try[ String ]

    /**
     * Kill (nicely) the external process running the solver
     */
    def destroy() : Unit

}
