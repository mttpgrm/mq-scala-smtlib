/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package typedterms

import au.edu.mq.comp.smtlib.parser.SMTLIB2Syntax._

/**
 * A term with attributes.
 *
 * @param typeDefs      As for [[TypedTerm]]
 * @param termDef       As for [[TypedTerm]]
 * @param theAttributes The attributes in the form (name, value)
 */
class Attributed[ A, +B <: Term ](
        typeDefs :      Set[ SortedQId ],
        termDef :       B,
        theAttributes : Map[ Keyword, AttValueSymbol ]
) extends TypedTerm[ A, B ]( typeDefs, termDef ) {

    /**
     * Retrieve the attribute value
     */
    def attVal( k : Keyword ) : Option[ AttValueSymbol ] = theAttributes.get( k )
}

/**
 * A term with attributes, must include a named attribute
 *
 * @param typeDefs      As for [[TypedTerm]]
 * @param termDef       As for [[TypedTerm]]
 * @param theAttributes The attributes in the form (name, value)
 *                      and one of them must be ("named", _)
 */
class Named[ A, +B <: Term ](
        theDefs :       Set[ SortedQId ],
        theTerm :       B,
        theAttributes : Map[ Keyword, AttValueSymbol ]
) extends Attributed[ A, B ]( theDefs, theTerm, theAttributes ) {

    require (
        theAttributes.get( Keyword( "named" ) ).nonEmpty,
        "You need to provide the attribute \"named\" to build a named term"
    )

    /**
     * The name of the TypedTerm retirved from the list of attributes
     */
    lazy val AttValueSymbol( SSymbol( nameDef : String ) ) =
        attVal( Keyword( "named" ) ).get

}
