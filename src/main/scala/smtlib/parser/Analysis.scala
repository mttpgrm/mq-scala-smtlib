/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package parser

import scala.util.{
    Try,
    Success,
    Failure
}
import parser.SMTLIB2Syntax._
import org.bitbucket.inkytonik.kiama.relation.{ Tree, CheckTree }
import org.bitbucket.inkytonik.kiama.rewriting.Rewriter
import org.bitbucket.inkytonik.kiama.attribution.Decorators
import org.bitbucket.inkytonik.kiama.attribution.Attribution
import org.bitbucket.inkytonik.kiama.rewriting.Rewriter

/**
 * Provides access to properties of a term
 *
 * @example
 * Let `t1` be the term defined by `(* x y@2)`.
 * {{{
 * val indexer = Map(ISymbol("y", 2)  -> 3, SSymbol("x") -> 0)
 * val t2 = Analysis( t1 ).index( indexer )
 * }}}
 * The pretty-printed version of `t2` is `(* x@0 y@3)`.
 */
case class Analysis[ T <: ASTNode ]( term : T ) extends Attribution with Rewriter {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    /*
     * Create a relational representation (Tree) of the term
     * Notice that Tree build an actual tree from the term.
     * If the term is Add(x,x) so not a tree as x appears twice,
     * Tree will split them apart and create two different nodes
     * for the 2 x's
     */
    private lazy val tree = new Tree[ ASTNode, T ]( term )

    /**
     * The boundedVars in a term (used in Let)
     */
    lazy val bvarDefs = Rewriter.collects {
        case e : BVarDef ⇒ e
    } ( tree.root )

    /**
     * The free variables in term
     */
    lazy val ids : Set[ QualifiedId ] = collectFreeVars( tree.root )

    /**
     * collect QualifiedIds that are not in a VarBinding (LetTerm) nor
     * in SortedVar (ForAllTerm, ExistsTerm)
     */
    lazy private val collectFreeVars : ASTNode ⇒ Set[ QualifiedId ] = attr {

        case e : QualifiedId ⇒ Set( e )

        case e @ LetTerm( varBindingList, xt ) ⇒
            /*
             * A let term is of the form
             * ( let (                  // List of VarBinding varBindingList
             *       (a!1  (+ x 1))     // form is: '(' localVar Spacing expression ')'
             *       (z  (* y x))
             *       )
             *      ( <= a!1 z )        // Term xt
             * )
             * The freeVars are the non-bounded vars x and y.
             * Each varbinding consists of a pair (localVar, expression)
             * of type (SMTLIB2Symbol, Term)
             */

            //  collect the variables in the expressions of the varBindings
            val exprVars = ( varBindingList map {
                case VarBinding( _, b ) ⇒ b
            } ).map( collectFreeVars ).reduceLeft( _ ++ _ )

            logger.info( "Set of vars in expressions is {}", exprVars )

            //  collect the vars in the term definition 'xt'
            val termDefvars = collectFreeVars( xt )

            logger.info( "Set of vars in let term is {}", termDefvars )

            //  compute result
            ( termDefvars ++ exprVars ) &~ boundedVars( e )

        /*
         * Note for  ForAllTerm and ExistsTerm. A ForAllTerm is of the form:
         * (forall
         *      (
         *          (x1 Int )   //  quantifiedVars, list of variables with types (SortedVar)
         *      )
         *      ( =>
         *          ( <= x1 0 ) // the term xt
         *          ( forall
         *              (
         *                  (y1 Int )
         *              )
         *              ( =>
         *                  ( >= y1 0 )
         *                  ( >= y1 x1 )
         *              )
         *          )
         *      )
         * )
         * Same form for ExistsTerm.
         * As the freevars are already in the typeDefs part of the quantified terms,
         * we can compute them using the following rules that collect the freevars of the
         * children
         */

        //  otherwise take the union on the children
        case other ⇒
            tree.child( other ).foldLeft( Set[ QualifiedId ]() ) {
                ( s, t ) ⇒ s ++ collectFreeVars( t )
            }
    }

    /**
     * Compute the bounded variables in a LetTerm
     */
    lazy private val boundedVars : LetTerm ⇒ Set[ QualifiedId ] = attr {

        case e @ LetTerm( varBindingList, xt ) ⇒

            val boundedVars : Set[ QualifiedId ] =
                //  note: varBindingList cannot be empty so we can use reduceLeft
                ( varBindingList map {
                    //  each VarBinding is a pair, (symbol, term)
                    //  with the symbol a local/biunded variable
                    case VarBinding( symbol, _ ) ⇒
                        val y : QualifiedId = SimpleQId( SymbolId( symbol ) )
                        Set( y )
                } ).reduceLeft( _ ++ _ )

            logger.info( "Set of bounded vars in {} is {}", e, boundedVars )

            boundedVars
    }

    /**
     * The different levels for logics
     * @todo  Use enum type instead
     */
    private val scalar = 0
    private val linear = 1
    private val nonLinear = 2

    /**
     * If more than 2 are linear or larger then it is nonLinear
     * we use a foldLeft to possibly avoit computing the value of the attribute
     * when not necessary. Example is: (* x y 1 ...) after x and y we know
     * this is non linear
     */
    private def findMaxModulo( xs : List[ ASTNode ], m : Int, initVal : Int = scalar ) =
        xs.foldLeft( initVal ) {
            case ( l, t ) ⇒
                if ( l >= m )
                    m
                else
                    scala.math.min( l + computeLogic( t ), m )
        }

    /**
     * Synthesise the logic to express a term
     */
    lazy private val computeLogic : ASTNode ⇒ Int = attr {

        case e : SimpleQId     ⇒ linear

        case e : NumLit        ⇒ scalar

        case e : DecLit        ⇒ scalar

        //  * linear if no more than one linear terms
        case MultTerm( a, xa ) ⇒ findMaxModulo( a :: xa, nonLinear )

        //  div is linear only if xa is a scalar and a is a scalar or linear
        case IntDivTerm( a, xa ) ⇒
            if ( findMaxModulo( xa, linear ) >= linear )
                //  one the divisors if at least linear
                nonLinear
            else
                //  xa is a scalar
                computeLogic( a )

        //  / is linear only if xa is a scalar
        case RealDivTerm( a, xa ) ⇒
            if ( findMaxModulo( xa, linear ) >= linear )
                //  one the divisors if at least linear
                nonLinear
            else
                //  xa is a scalar
                computeLogic( a )

        //  mod is linear only if b is a scalar
        case IntModTerm( a, b ) ⇒
            if ( computeLogic( b ) >= linear )
                //  one the divisors if at least linear
                nonLinear
            else
                //  xa is a scalar
                computeLogic( a )

        //  select and store are non linear
        case SelectTerm( _, _ )   ⇒ nonLinear

        case StoreTerm( _, _, _ ) ⇒ nonLinear

        //  otherwise compute the max logic
        case other ⇒
            //  we do it in a lzy manner and compute the attribute on the children
            //  only if needed
            tree.child( other ).foldLeft( scalar ) {
                case ( l, t ) ⇒
                    if ( l >= nonLinear )
                        nonLinear
                    else
                        scala.math.max( l, computeLogic( t ) )
            }

    }

    /**
     * The least upper bound in the lattice of logics that
     * can support the term
     */
    lazy val isLinear = computeLogic( tree.root ) <= linear

    //  Indexing and substitution

    /**
     * Subsitute some SMTLIB2Symbol by another SMTLIB2Symbol
     *
     * @param subs    The partial function that defines the substitution
     *
     * @return        A rule to implement the substitution
     */
    private def substitutionRule( subs : SMTLIB2Symbol ==> SMTLIB2Symbol ) =
        rule[ SMTLIB2Symbol ] {
            case s : SMTLIB2Symbol if subs.isDefinedAt( s ) ⇒ subs( s )
        }

    /**
     * Remove index of indexed variables
     */
    def unIndexed = {

        //  de-indexer function
        val deindexerFunction : SMTLIB2Symbol ==> SMTLIB2Symbol = {

            case ISymbol( name, _ ) ⇒ {
                logger.info( s"removing index of $name" )
                SSymbol( name )
            }
        }

        rewrite ( everywhere ( substitutionRule( deindexerFunction ) ) ) ( term )
    }

    /**
     * Index or re-index some symbols.
     *
     * @note              It rewrites symbols regardless of their status.
     *                    This implies that bounded variables (in Let, Exists, ForAll
     *                    terms) are also subject to rewriting if defined by `indexMap`
     *                    True() and False() are not indexed
     *
     * @param indexMap    A partial function that defines the indexing
     *
     * @return            A term where the symbols 's' defined by indexMap are replaced
     *                    by their indexed version 's_indexMap(s)'
     */
    def withIndex( indexMap : SMTLIB2Symbol ==> Int ) = {

        //  indexer function
        val indexerFunction : SMTLIB2Symbol ==> SMTLIB2Symbol = {

            case sym @ SSymbol( name ) if ( name == "false" || name == "true" ) ⇒
                logger.info( s"not indexing $sym as it is True|False" )
                sym

            //  re-index indexed symbol
            case sym @ ISymbol( name, _ ) if indexMap.isDefinedAt( sym ) ⇒
                logger.info( s"indexing $sym with ${indexMap( sym )}" )
                ISymbol( name, indexMap( sym ) )

            //  index unindexed symbol
            case sym @ SSymbol( name ) if indexMap.isDefinedAt( sym ) ⇒
                logger.info( s"re-indexing $sym with ${indexMap( sym )}" )
                ISymbol( name, indexMap( sym ) )
        }

        logger.info( s"applying indexing rule" )
        rewrite ( everywhere ( substitutionRule ( indexerFunction ) ) ) ( term )
    }

}
