/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package typedterms

import au.edu.mq.comp.smtlib.parser.SMTLIB2Syntax._

/**
 * Terms that are variables of some simple sort.
 *
 * @param id      The name of the variable
 * @param sort    The sort (ints, reals, array of some sort)
 * @param index   If provided the index to attach to the variable
 *
 */
class VarTerm[ A ](
    val id :    String,
    val sort :  Sort,
    val index : Option[ Int ] = None
) extends TypedTerm[ A, QIdTerm ](
    //  Set of variables is the single Id
    Set( SortedQId(
        SymbolId(
            if ( index.isEmpty ) SSymbol( id )
            else ISymbol( id, index.get )
        ),
        sort
    ) ),
    //  term is a SimpleQId
    QIdTerm( SimpleQId(
        SymbolId(
            if ( index.isEmpty ) SSymbol( id )
            else ISymbol( id, index.get )
        )

    ) )
) {

    /**
     * The SymbolId for the variable
     */
    val symbol : SMTLIB2Symbol =
        if ( index.isEmpty ) SSymbol( id )
        else ISymbol( id, index.get )

    /**
     * Set (or reset) the index of the variable
     *
     * @note If index is already set 'i' replaces the old one
     */
    def indexed( i : Int ) = new VarTerm[ A ]( id, sort, Some( i ) )

}

/**
 * The value of a term of Simple Sort
 */
case class Value( t : Term ) {

    def show : String = t match {
        case ConstantTerm( NumLit( e ) )            ⇒ e
        case NegTerm( ConstantTerm( NumLit( e ) ) ) ⇒ "-" + e
        case RealDivTerm(
            ConstantTerm(
                DecLit( d ) ),
            List( ConstantTerm( DecLit( n ) ) )
            ) ⇒ d + "/" + n
        case _ ⇒ parser.SMTLIB2PrettyPrinter.show( t )
    }
}

/**
 * A model for the current stack of assertions when it is Sat
 */
case class Model( private val xl : List[ ModelResponse ] ) {

    /**
     * make a map from pairs (SMTLIB2Symbol, Sort) to Value
     */
    private lazy val funDecltoTerm : Map[ ( SMTLIB2Symbol, Sort ), Value ] =
        (
            for {
                ModelResponse( FunDef( symbol, xs, tsort, t ) ) ← xl
            } yield ( ( symbol, tsort ), Value( t ) )
        ).toMap

    /**
     * The value of a variable in the model
     *
     * @param symbol    The name of the variable
     * @param sort      The sort of the variable
     */
    def get( symbol : SMTLIB2Symbol, sort : Sort ) : Option[ Value ] =
        funDecltoTerm.get( ( symbol, sort ) )

    def valueOf[ A ]( s : VarTerm[ A ] ) : Option[ Value ] =
        funDecltoTerm.get( ( s.symbol, s.sort ) )
}
