/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package configurations

import solvers.Solver

/**
 * Indicate the solver support for options
 */
object SupportsOptions {

    /**
     * Trait to indicate the solver supports proofs
     */
    trait SupportsProofs extends Solver {
        override def supportedOptions = super.supportedOptions + "Proofs"
    }

    /**
     * Trait to indicate the solver supports proofs
     */
    trait SupportsModels extends Solver {
        override def supportedOptions = super.supportedOptions + "Models"
    }

    /**
     * Trait to indicate the solver supports interpolants
     */
    trait SupportsInterpolants extends Solver {

        override def supportedOptions = super.supportedOptions + "Interpolants"

        val getInterpolantCmdString : String
    }

}

/**
 * Provide solver logics, options.
 *
 * {@link http://smtlib.cs.uiowa.edu/logics.shtml} for details
 */
object Configurations {

    import parser.SMTLIB2Syntax.{
        ProduceModels,
        ProduceProofs,
        ProduceInterpolants
    }
    import SupportsOptions._

    //  Options

    /** Add option :produce-models to solver */
    trait Models extends Solver {

        this : SupportsModels ⇒

        abstract override def options = super.options + ProduceModels( true )
    }

    /** Add option :produce-proofs to solver */
    trait Proofs extends Solver {

        this : SupportsProofs ⇒

        abstract override def options = super.options + ProduceProofs( true )
    }

    /** Add option :produce-interpolants to solver */
    trait Interpolants extends Solver {

        this : SupportsInterpolants ⇒

        abstract override def options = super.options + ProduceInterpolants( true )
    }
}
