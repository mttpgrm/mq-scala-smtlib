/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package theories

trait BVTerm

/**
 * This theory contains the basic elements of integer arithmetic.
 *
 * <ul>
 *  <li> a sort (_ BitVec n) is defined for each non-zero numeral n </li>
 *  <li> the function concat is defined that combines two bit-vectors into a longer one </li>
 *  <li> a family of functions (_ extract i j) is defined that extracts a contiguous sub-vector
 *          from index i to j (inclusive) of a bit-vector </li>
 *  <li> unary functions bvnot and bvneg</li>
 *  <li> binary functions bvand bvor bvadd bvmul bvudiv bvurem bvshl bvlshr</li>
 *  <li> the binary comparison function bvult</li>
 * </ul>
 *
 * For operators on Bit vectors, logics QF_BV see
 * {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#QF_BV}
 */
trait BitVectors {

    import typedterms.{ TypedTerm, VarTerm }

    /**
     *  Provide factory and methods to build integer terms in SMTLIB2
     */
    object BVs {

        import au.edu.mq.comp.smtlib.parser.SMTLIB2Syntax.{
            Term,
            SSymbol,
            QIdTerm,
            SortedQId,
            SymbolId,
            NumLit,
            HexaLit,
            BinLit,
            SimpleQId,
            ConstantTerm,
            BitVectorSort,
            DecBVLit,
            BVvalue,
            DecLit
        }

        /**
         * Create an BitVector variable.
         *
         * @param name The identifier of the variable
         */
        def apply( name : String, bits : Int ) =
            new VarTerm[ BVTerm ]( name, BitVectorSort( bits.toString ) )

        /**
         *  Create an BV constant constant
         *
         * @param i The value of the constant. #x is hexadecimal,
         * #b binary
         *
         * @todo Check what to do with negative ints
         */
        def apply( s : String ) : TypedTerm[ BVTerm, ConstantTerm ] = {
            require( s.length >= 1, "Must provide a hexadecimal or binary" )
            TypedTerm[ BVTerm, ConstantTerm ](
                Set(),
                ConstantTerm(
                    s( 0 ) match {

                        case '#' ⇒ s( 1 ) match {
                            case 'x' ⇒ HexaLit( s.takeRight( s.length - 2 ) )
                            case 'b' ⇒ BinLit( s.takeRight( s.length - 2 ) )
                        }

                        //  otherwise not supported
                        case _ ⇒ sys.error( "int not supported" )
                    }
                )
            )
        }

        /**
         * BitVector from a decimal. Need to specify the number of bits.
         */
        def apply( n : Int, bits : Int ) = {
            if ( n >= 0 )
                TypedTerm[ BVTerm, ConstantTerm ](
                    Set(),
                    ConstantTerm( DecBVLit( BVvalue( n.toString ), bits.toString ) )
                )
            else
                -TypedTerm[ BVTerm, ConstantTerm ](
                    Set(),
                    ConstantTerm( DecBVLit( BVvalue( ( -n ).toString ), bits.toString ) )
                )
        }

        /**
         * BitVector from a decimal string. Need to specify the number of bits. This method
         * can't be an overload of `apply` since the `String` version of `apply` is used
         * for named bit vector values.
         */
        def fromString( s : String, bits : Int ) =
            if ( s( 0 ) == '-' )
                -TypedTerm[ BVTerm, ConstantTerm ](
                    Set(),
                    ConstantTerm( DecBVLit( BVvalue( s.tail ), bits.toString ) )
                )
            else
                TypedTerm[ BVTerm, ConstantTerm ](
                    Set(),
                    ConstantTerm( DecBVLit( BVvalue( s ), bits.toString ) )
                )

    }

    /**
     * Make a BVs from an int and a number of bits
     */
    implicit class IntToBVs( i : Int ) {
        def withBits( bits : Int ) = BVs( i, bits )
    }

    import parser.SMTLIB2Syntax.Term
    /**
     * BitVector operations
     */
    implicit class Operations[ T1 <: Term ]( op1 : TypedTerm[ BVTerm, T1 ] ) {

        import parser.SMTLIB2Syntax.{
            BVAddTerm,
            BVSubTerm,
            BVMultTerm,
            BVNegTerm,
            BVNotTerm,
            BVuDivTerm,
            BVuRemTerm,
            BVsDivTerm,
            BVsRemTerm,
            BVOrTerm,
            BVXorTerm,
            BVAndTerm,
            BVShiftLeftTerm,
            BVSLogicalShiftRightTerm,
            BVSArithmeticShiftRightTerm,
            BVSignExtendTerm,
            BVZeroExtendTerm,
            BVExtractTerm,
            NumLit
        }

        /**
         * Addition
         */
        def +[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVAddTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVAddTerm( op1.termDef, op2.termDef )
            )

        /**
         * Subtraction
         */
        def -[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVSubTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVSubTerm( op1.termDef, op2.termDef )
            )

        /**
         * Unary minus
         */
        def unary_- =
            TypedTerm[ BVTerm, BVNegTerm ]( op1.typeDefs, BVNegTerm( op1.termDef ) )

        /**
         * Unary not
         */
        def unary_! =
            TypedTerm[ BVTerm, BVNotTerm ]( op1.typeDefs, BVNotTerm( op1.termDef ) )

        /**
         * Multiplication
         */
        def *[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVMultTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVMultTerm( op1.termDef, op2.termDef )
            )

        /**
         * Unsigned division
         */
        def /[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVuDivTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVuDivTerm( op1.termDef, op2.termDef )
            )

        /**
         * Signed division.
         */
        def sdiv[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVsDivTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVsDivTerm( op1.termDef, op2.termDef )
            )

        /**
         * Unsigned Modulus
         */
        def %[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVuRemTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVuRemTerm( op1.termDef, op2.termDef )
            )

        /**
         * Signed Modulus
         */
        def srem[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVsRemTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVsRemTerm( op1.termDef, op2.termDef )
            )

        /**
         * Or
         */
        def or[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVOrTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVOrTerm( op1.termDef, op2.termDef )
            )

        /**
         * Xor
         */
        def xor[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVXorTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVXorTerm( op1.termDef, op2.termDef )
            )

        /**
         * And
         */
        def and[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVAndTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVAndTerm( op1.termDef, op2.termDef )
            )

        /**
         * Shift left
         */
        def <<[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVShiftLeftTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVShiftLeftTerm( op1.termDef, op2.termDef )
            )

        /**
         * Logical shift right
         */
        def >>[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVSLogicalShiftRightTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVSLogicalShiftRightTerm( op1.termDef, op2.termDef )
            )

        /**
         * Artihmetic shift right
         */
        def ashr[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVSArithmeticShiftRightTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVSArithmeticShiftRightTerm( op1.termDef, op2.termDef )
            )

        /**
         * Sign extend
         */
        def sext[ T2 <: Term ]( bits : Int ) =
            TypedTerm[ BVTerm, BVSignExtendTerm ](
                op1.typeDefs,
                BVSignExtendTerm( NumLit( bits.toString ), op1.termDef )
            )

        /**
         * Zero extend
         */
        def zext[ T2 <: Term ]( bits : Int ) =
            TypedTerm[ BVTerm, BVZeroExtendTerm ](
                op1.typeDefs,
                BVZeroExtendTerm( NumLit( bits.toString ), op1.termDef )
            )

        /**
         * Extract
         */
        def extract[ T2 <: Term ]( low : Int, high : Int ) =
            TypedTerm[ BVTerm, BVExtractTerm ](
                op1.typeDefs,
                BVExtractTerm( NumLit( low.toString ), NumLit( high.toString ), op1.termDef )
            )

    }

    /**
     * Comparison operators that can be applied to [[BVTerm]]
     */
    implicit class BVBVToBoolOperators[ T2 <: Term ]( op1 : TypedTerm[ BVTerm, T2 ] ) {

        import typedterms.TypedTerm
        import parser.SMTLIB2Syntax.{
            BVULessThanEqualTerm,
            BVULessThanTerm,
            BVUGreaterThanEqualTerm,
            BVUGreaterThanTerm,
            BVSLessThanEqualTerm,
            BVSLessThanTerm,
            BVSGreaterThanEqualTerm,
            BVSGreaterThanTerm
        }

        /**
         * BV Less than or equal unsigned
         */
        def ule[ T4 <: Term ]( op2 : TypedTerm[ BVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, BVULessThanEqualTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVULessThanEqualTerm( op1.termDef, op2.termDef )
            )

        /**
         * BV Less than unsigned
         */
        def ult[ T4 <: Term ]( op2 : TypedTerm[ BVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, BVULessThanTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVULessThanTerm( op1.termDef, op2.termDef )
            )

        /**
         * BV Greater than or equal unsigned
         */
        def uge[ T4 <: Term ]( op2 : TypedTerm[ BVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, BVUGreaterThanEqualTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVUGreaterThanEqualTerm( op1.termDef, op2.termDef )
            )

        /**
         * BV Greater than unsigned
         */
        def ugt[ T4 <: Term ]( op2 : TypedTerm[ BVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, BVUGreaterThanTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVUGreaterThanTerm( op1.termDef, op2.termDef )
            )

        /**
         * BV Less than or equal signed
         */
        def sle[ T4 <: Term ]( op2 : TypedTerm[ BVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, BVSLessThanEqualTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVSLessThanEqualTerm( op1.termDef, op2.termDef )
            )

        /**
         * BV Less than signed
         */
        def slt[ T4 <: Term ]( op2 : TypedTerm[ BVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, BVSLessThanTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVSLessThanTerm( op1.termDef, op2.termDef )
            )

        /**
         * BV Greater than or equal signed
         */
        def sge[ T4 <: Term ]( op2 : TypedTerm[ BVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, BVSGreaterThanEqualTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVSGreaterThanEqualTerm( op1.termDef, op2.termDef )
            )

        /**
         * BV Greater than signed
         */
        def sgt[ T4 <: Term ]( op2 : TypedTerm[ BVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, BVSGreaterThanTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVSGreaterThanTerm( op1.termDef, op2.termDef )
            )
    }

}
