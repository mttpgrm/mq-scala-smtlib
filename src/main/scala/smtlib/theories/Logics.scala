/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package theories

object PredefinedLogics {

    import solvers.SupportsLogics._

    /**
     * Quantifier-Free propositional logic (and uninterpreted functions)
     * {@link http://smtlib.cs.uiowa.edu/logics.shtml#QF_UF}
     *
     */
    trait QF_UF {
        this : Supports_QF_UF ⇒
        abstract override val logic = Some( "QF_UF" )
    }

    /**
     * Quantifier-Free fixed size bit vectors logic (and uninterpreted functions)
     * {@link http://smtlib.cs.uiowa.edu/logics.shtml#QF_BV}
     *
     */
    trait QF_BV {
        this : Supports_QF_BV ⇒
        abstract override val logic = Some( "QF_BV" )
    }

    /**
     * Closed quantifier-free formulas over the theory of bitvectors and bitvector arrays.
     * {@link http://smtlib.cs.uiowa.edu/logics.shtml#QF_ABV}
     *
     */
    trait QF_ABV {
        this : Supports_QF_ABV ⇒
        abstract override val logic = Some( "QF_ABV" )
    }

    /**
     * Quantifier-Free Linear Integer Arithmetic
     * {@link http://smtlib.cs.uiowa.edu/logics.shtml#QF_LIA}
     *
     */
    trait QF_LIA {
        this : Supports_QF_LIA ⇒
        abstract override val logic = Some( "QF_LIA" )
    }

    /**
     * Quantifier-Free non-Linear Integer Arithmetic
     * {@link http://smtlib.cs.uiowa.edu/logics.shtml#QF_NIA}
     *
     * @todo add NonLinearInts
     */
    trait QF_NIA {
        this : Supports_QF_NIA ⇒
        abstract override val logic = Some( "QF_NIA" )
    }

    /**
     * Quantifier-Free Linear Real Arithmetic
     * {@link http://smtlib.cs.uiowa.edu/logics.shtml#QF_LRA}
     *
     */
    trait QF_LRA {
        this : Supports_QF_LRA ⇒
        abstract override val logic = Some( "QF_LRA" )
    }

    /**
     * Quantifier-Free Mixed Linear Integer Real Arithmetic
     * {@link http://smtlib.cs.uiowa.edu/logics.shtml#QF_LIRA}
     *
     */
    trait QF_LIRA {
        this : Supports_QF_LIRA ⇒
        abstract override val logic = Some( "QF_LIRA" )
    }

    /**
     * Quantifier-Free Non-Linear Real Arithmetic
     * {@link http://smtlib.cs.uiowa.edu/logics.shtml#QF_NRA}
     *
     */
    trait QF_NRA {
        this : Supports_QF_NRA ⇒
        abstract override val logic = Some( "QF_NRA" )
    }

    /**
     * QF_AUFLIA
     */
    trait QF_AUFLIA {
        this : Supports_QF_AUFLIA ⇒
        abstract override val logic = Some( "QF_AUFLIA" )
    }

    /**
     * QF_UFLIA
     */
    trait QF_UFLIRA {
        this : Supports_QF_UFLIRA ⇒
        abstract override val logic = Some( "QF_UFLIRA" )
    }

    /**
     * AUFNIRA
     */
    trait AUFNIRA {
        this : Supports_AUFNIRA ⇒
        abstract override val logic = Some( "AUFNIRA" )
    }

}
