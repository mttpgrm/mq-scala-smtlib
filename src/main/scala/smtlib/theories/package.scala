/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib

package object theories {

    /**
     * SMTLIB2 sort associated with a TypedTerm[T._]
     */
    object TermToSort {

        import parser.SMTLIB2Syntax.{ IntSort, RealSort, BoolSort }

        /**
         * Map pre-defined terms types to the corresponding
         * declaration of the sort in SMTLIB2.
         */
        //  format: OFF
        private val dispatch = Map(
            classOf[ IntTerm ]      → IntSort() ,
            classOf[ RealTerm ]     → RealSort(),
            classOf[ BoolTerm ]     → BoolSort()
        )
        //  format: ON

        import scala.reflect.ClassTag

        /**
         * Provide the SMTLIB2 sort associated with a TypedTerm[T,_]
         */
        def apply[ T ]( implicit mf : ClassTag[ T ] ) = {
            dispatch.find(
                _._1 isAssignableFrom mf.runtimeClass
            ).map( _._2 ).get
        }
    }
}
