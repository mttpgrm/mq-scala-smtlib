/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package theories

/**
 *  SMTLIB2 Array terms
 *
 *  This is an array with indices in Int and elements in T
 */
trait ArrayTerm[ T ]

/**
 * Provides factory for arrays of IntTerm, dimensions 1 and 2
 */
trait ArrayExInt extends ArrayEx {

    /**
     * Array of Ints
     */
    object ArrayInt1 {
        def apply( name : String ) = ArrayEx1[ IntTerm ]( name )
    }

    /**
     * Array of Array of Ints
     */
    object ArrayInt2 {
        def apply( name : String ) = ArrayEx2[ IntTerm ]( name )
    }
}

/**
 * Provides factory for arrays of RealTerm, dimensions 1 and 2
 */
trait ArrayExReal extends ArrayEx {

    /**
     * Array of Reals
     */
    object ArrayReal1 {
        def apply( name : String ) = ArrayEx1[ RealTerm ]( name )
    }

    /**
     * Array of Array of Reals
     */
    object ArrayReal2 {
        def apply( name : String ) = ArrayEx2[ RealTerm ]( name )
    }

}

/**
 * Provides factory for arrays of BoolTerm, dimensions 1 and 2
 */
trait ArrayExBool extends ArrayEx {

    /**
     *  Array of Bool
     */
    object ArrayBool1 {
        def apply( name : String ) = ArrayEx1[ BoolTerm ]( name )
    }

    /**
     * Array of Array of Bool
     */
    object ArrayBool2 {
        def apply( name : String ) = ArrayEx2[ BoolTerm ]( name )
    }

}
/**
 * Provide factory for Arrays of dimensions 1 and 2 of element type T
 */
sealed trait ArrayEx {

    import parser.SMTLIB2Syntax.{
        SSymbol,
        SortedQId,
        SymbolId,
        Array1,
        Array1Sort,
        Array2,
        Array2Sort
    }

    import scala.reflect.ClassTag

    /**
     * Build and Array of dimension 1 ot some sort V
     */
    object ArrayEx1 {
        def apply[ V ]( name : String )( implicit mf : ClassTag[ V ] ) = {
            val q = SortedQId(
                SymbolId( SSymbol( name ) ),
                Array1( Array1Sort( TermToSort[ V ] ) )
            )
            makeArray[ V ]( q )
        }
    }

    /**
     * Build and Array of dimension 2 ot some sort V
     */
    object ArrayEx2 {
        def apply[ V ]( name : String )( implicit mf : ClassTag[ V ] ) = {
            val q = SortedQId(
                SymbolId( SSymbol( name ) ),
                Array2 ( Array2Sort( Array1Sort( TermToSort[ V ] ) ) )
            )
            makeArray[ ArrayTerm[ V ] ]( q )
        }
    }

    import typedterms.VarTerm

    /**
     * Make a typed term with a parametric array type
     */
    private def makeArray[ T ]( q : SortedQId ) = {
        val SymbolId( SSymbol( id ) ) = q.id
        new VarTerm[ ArrayTerm[ T ] ]( id, q.sort )
    }

}
