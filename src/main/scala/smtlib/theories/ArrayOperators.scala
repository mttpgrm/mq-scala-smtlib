/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package theories

/**
 * Standard operators for the theory of arrays ArrayEx
 */
trait ArrayExOperators {

    import typedterms.TypedTerm
    import au.edu.mq.comp.smtlib.parser.SMTLIB2Syntax.{ Term, QIdAndTermsTerm, SelectTerm, StoreTerm }
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    /**
     * Array operators that can be applied to [[ArrayTerm]]
     *
     * This implicit class adds new operators to [[typedterms.TypedTerm]]s that are [[ArrayTerm]]
     *
     */
    implicit class ArrayOperators[ T, T1 <: Term ]( op1 : TypedTerm[ ArrayTerm[ T ], T1 ] ) {

        /**
         * Select an element in an Array term
         *
         * The index must be an [[IntTerm]]
         *
         * @param index The index to select
         */
        def select[ T3 <: IntTerm, T2 <: Term ]( index : TypedTerm[ T3, T2 ] ) =
            //  type of element is T if op1 is of type ArrayTerm[ T ]
            TypedTerm[ T, SelectTerm ](
                ( op1.typeDefs ++ index.typeDefs ),
                SelectTerm( op1.termDef, index.termDef )
            )

        /**
         * Select an element in array term (see [[select]])
         *
         * @param index The element so select
         */
        def at[ T3 <: IntTerm, T2 <: Term ]( index : TypedTerm[ T3, T2 ] ) = op1.select( index )

        /**
         * Select an element in array term (see [[select]])
         *
         * @param index The element so select
         */
        def apply[ T3 <: IntTerm, T2 <: Term ]( index : TypedTerm[ T3, T2 ] ) = op1.select( index )

        /**
         * Store an element in an Array and build a new Array
         *
         * @param index The index to store the value 'value' at
         * @param value The value to store at index 'index'
         *
         */
        def store[ T5 <: T, T4 <: IntTerm, T2 <: Term, T3 <: Term ](
            index : TypedTerm[ T4, T2 ],
            value : TypedTerm[ T5, T3 ]
        ) =
            //  returns the same Array type ArrayTerm[ T ]
            TypedTerm[ ArrayTerm[ T ], StoreTerm ](
                ( op1.typeDefs ++ index.typeDefs ++ value.typeDefs ),
                StoreTerm( op1.termDef, index.termDef, value.termDef )
            // Store( op1.termDef, index.termDef, value.termDef )
            )
    }

    /**
     * Array operators that can be applied to [[parser.SMTLIB2Syntax.SelectTerm]]
     *
     * This implicit class adds new operators to [[typedterms.TypedTerm]]s
     * that are [[parser.SMTLIB2Syntax.SelectTerm]]. This enables us to use
     * a friendly  syntax to write  array assignment
     *
     */
    implicit class ArrayAssignOperator[ T ]( op1 : TypedTerm[ T, SelectTerm ] ) {

        //  select is an TypedTerm( array var + index var,   SelectTerm)
        def :=[ T1 <: T, T2 <: Term ](
            newValue : TypedTerm[ T1, T2 ]
        ) = {

            /*
             * A TypedTerm[_, SelectTerm] should always have a termDef of type
             * SelectTerm but the compiler cannot infer that
             * If this operator is used with wrong type, we throw an exception.
             */
            require(
                op1.termDef.isInstanceOf[ SelectTerm ],
                "An TypedTerm[T, SelectTerm] should have a termDef of type SelectTerm]"
            )

            //  The previous 'require' will prevent us from
            //  going further with a wrong type
            op1.termDef.asInstanceOf[ SelectTerm ] match {
                case SelectTerm( a, index ) ⇒
                    //  returns the same Array type ArrayTerm[ T ]
                    TypedTerm[ ArrayTerm[ T ], StoreTerm ](
                        ( op1.typeDefs ++ newValue.typeDefs ),
                        StoreTerm( a, index, newValue.termDef )
                    )
            }
        }
    }

}
