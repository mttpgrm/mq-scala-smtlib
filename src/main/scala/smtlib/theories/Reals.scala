/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package theories

trait RealTerm

trait RealArithmetics {

    import typedterms.TypedTerm
    import typedterms.VarTerm

    /**
     *  Provide factory and methods to build  real terms in SMTLIB2
     */
    object Reals {

        import au.edu.mq.comp.smtlib.parser.SMTLIB2Syntax.{
            Term,
            SSymbol,
            QIdTerm,
            SortedQId,
            SymbolId,
            DecLit,
            SimpleQId,
            ConstantTerm,
            RealSort
        }
        import java.math.BigDecimal

        /**
         * Create a real variable.
         *
         * @param name The identifier of the variable
         */
        def apply( name : String ) = new VarTerm[ RealTerm ]( name, RealSort() )

        /**
         * Make a Real constant.
         * In SMTLIB2, this is QIdterm
         */
        def apply( r : Double ) : TypedTerm[ RealTerm, Term ] = ( r >= 0 ) match {

            //  positive real numbers are constantTerms
            case true ⇒
                val s = new BigDecimal( r ).toPlainString()
                TypedTerm[ RealTerm, ConstantTerm ](
                    Set[ SortedQId ](),
                    ConstantTerm( DecLit( s ) )
                )

            //  negative real numbers are QIdAndTermsTerm
            case false ⇒
                //  import the unary minus implicits
                // import Implicits._
                //  return negation of the ConstantTerm of the absolute value
                val s = new BigDecimal( -r ).toPlainString()
                -TypedTerm[ RealTerm, ConstantTerm ](
                    Set[ SortedQId ](),
                    ConstantTerm( DecLit( s ) )
                )
        }
    }

    import scala.language.implicitConversions

    /**
     * Make an Reals from a Double
     */
    implicit def realToTypedTerm( i : Double ) = Reals( i )

    import parser.SMTLIB2Syntax.{
        Term,
        AbsTerm
    }

    /**
     * Absolute value
     */
    def absR[ T1 <: Term ]( op1 : TypedTerm[ RealTerm, T1 ] ) =
        TypedTerm[ RealTerm, AbsTerm ]( op1.typeDefs, AbsTerm( op1.termDef ) )

    /**
     * Real Arithmetic operations
     */
    implicit class Operations[ T1 <: Term ]( op1 : TypedTerm[ RealTerm, T1 ] ) {

        import parser.SMTLIB2Syntax.{
            PlusTerm,
            SubTerm,
            MultTerm,
            NegTerm,
            // Uminus,
            RealDivTerm
        }

        /**
         * Addition
         */
        def +[ T2 <: Term ]( op2 : TypedTerm[ RealTerm, T2 ] ) =
            TypedTerm[ RealTerm, PlusTerm ](
                op1.typeDefs ++ op2.typeDefs,
                PlusTerm( op1.termDef, List( op2.termDef ) )
            )

        /**
         * Subtraction
         */
        def -[ T2 <: Term ]( op2 : TypedTerm[ RealTerm, T2 ] ) =
            TypedTerm[ RealTerm, SubTerm ](
                op1.typeDefs ++ op2.typeDefs,
                SubTerm( op1.termDef, List( op2.termDef ) )
            )

        /**
         * Unary minus
         */
        def unary_- =
            TypedTerm[ RealTerm, NegTerm ]( op1.typeDefs, NegTerm( op1.termDef ) )

        /**
         * Multiplication
         */
        def *[ T2 <: Term ]( op2 : TypedTerm[ RealTerm, T2 ] ) =
            TypedTerm[ RealTerm, MultTerm ](
                op1.typeDefs ++ op2.typeDefs,
                MultTerm( op1.termDef, List( op2.termDef ) )
            )

        /**
         * Division
         */
        def /[ T2 <: Term ]( op2 : TypedTerm[ RealTerm, T2 ] ) =
            TypedTerm[ RealTerm, RealDivTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                RealDivTerm( op1.termDef, List( op2.termDef ) )
            )
    }

    /**
     * Comparison operators that can be applied to [[RealTerm]]
     */
    implicit class RealRealToBoolOperators[ T2 <: Term ]( op1 : TypedTerm[ RealTerm, T2 ] ) {
        import typedterms.TypedTerm
        import parser.SMTLIB2Syntax.{
            LessThanEqualTerm,
            LessThanTerm,
            GreaterThanEqualTerm,
            GreaterThanTerm
        }

        /**
         * Less than or equal
         */
        def <=[ T4 <: Term ]( op2 : TypedTerm[ RealTerm, T4 ] ) =
            TypedTerm[ BoolTerm, LessThanEqualTerm ](
                op1.typeDefs ++ op2.typeDefs,
                LessThanEqualTerm( op1.termDef, op2.termDef )
            )

        /**
         * Less than
         */
        def <[ T4 <: Term ]( op2 : TypedTerm[ RealTerm, T4 ] ) =
            TypedTerm[ BoolTerm, LessThanTerm ](
                op1.typeDefs ++ op2.typeDefs,
                LessThanTerm( op1.termDef, op2.termDef )
            )

        /**
         * Greater than or equal
         */
        def >=[ T4 <: Term ]( op2 : TypedTerm[ RealTerm, T4 ] ) =
            TypedTerm[ BoolTerm, GreaterThanEqualTerm ](
                op1.typeDefs ++ op2.typeDefs,
                GreaterThanEqualTerm( op1.termDef, op2.termDef )
            )

        /**
         * Greater than
         */
        def >[ T4 <: Term ]( op2 : TypedTerm[ RealTerm, T4 ] ) =
            TypedTerm[ BoolTerm, GreaterThanTerm ](
                op1.typeDefs ++ op2.typeDefs,
                GreaterThanTerm( op1.termDef, op2.termDef )
            )
    }
}
