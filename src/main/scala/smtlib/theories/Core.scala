/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package theories

trait BoolTerm

/**
 *
 * From {@link http://www.a.com}
 * The theory contains the basic elements of Boolean logic. It defines:
 * <ul>
 *  <li>    the 'Bool' sort</li>
 *  <li>    the constants 'True' and 'False' of sort 'Bool'</li>
 *  <li>    the 'not' operation</li>
 *  <li>    the familiar left-associative functions 'and' and
 *          'or' and 'xor' (for conjunction and disjunction and inequality)
 *          on 'Bool' values </li>
 *  <li>    the right-associative function '=>', which is implication</li>
 *  <li>    the (chainable) equality function among the elements of
 *          any set of values of the same sort</li>
 *  <li>    the (pairwise) inequality function (called 'distinct') among the
 *          elements of any set of values of the same sort</li>
 *  <li>    the 'if-then-else' functions (called 'ite'), which take a first
 *          'Bool' argument and two additional arguments of the same but
 *          arbitrary sort</li>
 * </ul>
 *
 */
trait Core {

    import typedterms.TypedTerm
    import typedterms.VarTerm
    import au.edu.mq.comp.smtlib.parser.SMTLIB2Syntax.{
        Term,
        SSymbol,
        QIdTerm,
        SortedQId,
        SymbolId,
        SimpleQId,
        BoolSort,
        AndTerm,
        OrTerm,
        ImplyTerm,
        NotTerm,
        XorTerm,
        EqualTerm,
        DistinctTerm,
        IfThenElseTerm
    }

    /**
     *  Provide factory and methods to build  boolean terms in SMTLIB2
     */
    object Bools {

        //  Convenience methods to create BoolSort variables and values

        /**
         * Create a boolean variable.
         *
         * @param name The identifier of the variable
         */
        def apply( name : String ) = new VarTerm[ BoolTerm ]( name, BoolSort() )

        /**
         * Make a boolean term.
         * In SMTLIB2, this is QIdterm
         */
        def apply( b : Boolean ) : TypedTerm[ BoolTerm, QIdTerm ] =
            TypedTerm(
                Set(),
                QIdTerm(
                    SimpleQId(
                        SymbolId(
                            SSymbol( if ( b ) "true" else "false" )
                        )
                    )
                )
            )

    }

    //  True and false

    /**
     * The TypedTerm for the truth value true
     */
    object True {

        def apply() = Bools( true )

    }

    /**
     * The TypedTerm for the truth value false
     */
    object False {

        def apply() = Bools( false )

    }

    import scala.language.implicitConversions

    /**
     * Make a TypedTerm of type Bool from a Boolean
     */
    implicit def booleanToBoolSort( b : Boolean ) : TypedTerm[ BoolTerm, QIdTerm ] = Bools( b )

    //  Operators on Bool

    /**
     * Logical operators that can be applied to [[BoolTerm]]
     *
     * This implicit class adds new operators to [[typedterms.TypedTerm]]s that are [[BoolTerm]]
     *
     */
    implicit class BoolBoolToBoolOperators[ T1 <: Term ]( op1 : TypedTerm[ BoolTerm, T1 ] ) {

        /**
         * Logical implication between two [[BoolTerm]]
         */
        def imply[ T2 <: Term ]( op2 : TypedTerm[ BoolTerm, T2 ] ) =
            TypedTerm[ BoolTerm, ImplyTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                ImplyTerm( op1.termDef, List( op2.termDef ) )
            )

        /**
         * Logical conjunction between two [[BoolTerm]]
         */
        def &[ T2 <: Term ]( op2 : TypedTerm[ BoolTerm, T2 ] ) =
            TypedTerm[ BoolTerm, AndTerm ](

                //  set of typeDefs is the union
                op1.typeDefs ++ op2.typeDefs,

                //  the termDef maybe simplified
                ( op1.termDef, op2.termDef ) match {

                    //  merge two and
                    case ( AndTerm( a, xa ), AndTerm( b, xb ) ) ⇒ AndTerm( a, b :: xa ::: xb )

                    //  merge in left term
                    //  note that we insert xt before xa
                    //  (more efficient to prepend an element to a
                    //  list then append
                    case ( AndTerm( a, xa ), xt )               ⇒ AndTerm( a, xt :: xa )

                    //  merge in right term
                    case ( xt, AndTerm( a, xa ) )               ⇒ AndTerm( xt, a :: xa )

                    //  no merge, build new addition
                    case ( x, y )                               ⇒ AndTerm( x, List( y ) )
                }
            )

        /**
         * Logical disjunction (n-ary, n >= 2) between two [[BoolTerm]]
         */
        def |[ T2 <: Term ]( op2 : TypedTerm[ BoolTerm, T2 ] ) =
            TypedTerm[ BoolTerm, OrTerm ](

                //  set of typeDefs is the union
                op1.typeDefs ++ op2.typeDefs,

                //  the termDef maybe simplified
                ( op1.termDef, op2.termDef ) match {

                    //  merge two Or
                    case ( OrTerm( a, xa ), OrTerm( b, xb ) ) ⇒ OrTerm( a, b :: xa ::: xb )

                    //  merge in left term
                    //  note that we insert xt before xa (more efficient to prepend an element to a //  list then append
                    case ( OrTerm( a, xa ), xt )              ⇒ OrTerm( a, xt :: xa )

                    //  merge in right term
                    case ( xt, OrTerm( a, xa ) )              ⇒ OrTerm( xt, a :: xa )

                    //  no merge, build new addition
                    case ( x, y )                             ⇒ OrTerm( x, List( y ) )
                }
            )

        /**
         * Logical xor between two [[BoolTerm]]
         */
        def xor[ T2 <: Term ]( op2 : TypedTerm[ BoolTerm, T2 ] ) =
            TypedTerm[ BoolTerm, XorTerm ](

                //  set of typeDefs is the union
                op1.typeDefs ++ op2.typeDefs,

                XorTerm( op1.termDef, op2.termDef )
            )

        /**
         * If-then-else with a condition [[BoolTerm]]
         *
         * @tparam T        The type of the result of if-then-else
         * @param ifop      The result when the the condition op1 is True
         * @param elseop    The result when the condition op1 is False
         */
        def ite[ T, T2 <: Term, T3 <: Term ](
            ifop :   TypedTerm[ T, T2 ],
            elseop : TypedTerm[ T, T3 ]
        ) =
            TypedTerm[ T, IfThenElseTerm ](
                ( op1.typeDefs ++ ifop.typeDefs ++ elseop.typeDefs ),
                IfThenElseTerm( op1.termDef, ifop.termDef, elseop.termDef )
            )

        /**
         * Not (logical negation)
         *
         * @tparam T        The type of the result of if-then-else
         */
        def unary_! =
            TypedTerm[ BoolTerm, NotTerm ](
                op1.typeDefs,
                NotTerm ( op1.termDef )
            )
    }

    /**
     * Equality and distinct can be used between two terms of same type
     */
    implicit class EqualOperators[ T1, T2 <: Term ]( op1 : TypedTerm[ T1, T2 ] ) {

        import au.edu.mq.comp.smtlib.parser.SMTLIB2Syntax.{
            EqualTerm,
            DistinctTerm
        }

        /**
         * equality between two T1
         */
        def ===[ T4 <: Term ]( op2 : TypedTerm[ T1, T4 ] ) =
            TypedTerm[ BoolTerm, EqualTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                EqualTerm( op1.termDef, op2.termDef )
            )

        /**
         * distinct between two T1
         */
        def =/=[ T4 <: Term ]( op2 : TypedTerm[ T1, T4 ] ) =
            TypedTerm[ BoolTerm, DistinctTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                DistinctTerm( op1.termDef, op2.termDef )
            )
    }

}
