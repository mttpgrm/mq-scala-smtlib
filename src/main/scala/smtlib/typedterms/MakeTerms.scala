/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package typedterms

import au.edu.mq.comp.smtlib.parser.SMTLIB2Syntax._

/**
 * A term with type information.
 *
 * @tparam B        The SMTLIB2 type of the term
 * @tparam A        The logical type of the term
 *
 * @param typeDefs  The type of the free variables in t.
 * @param t         The SMTLIB2 term
 * @param name      The name of the term is set
 *
 */
case class TypedTerm[ A, +B <: Term ](
        val typeDefs : Set[ SortedQId ],
        val aTerm :    B
) {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    /**
     * Legacy naming
     */
    lazy val termDef = aTerm

    /**
     * Give a name to the typedTerm
     *
     * @param name The name to be attachec to the term.
     */
    def named( name : String ) = new Named[ A, NamedTerm ](
        typeDefs,
        NamedTerm( termDef, SSymbol( name ) ),
        Map (
            Keyword( "named" ) → AttValueSymbol( SSymbol( name ) )
        )
    )

    /**
     * Pretty print term
     */
    def show : String = {
        import au.edu.mq.comp.smtlib.parser.SMTLIB2PrettyPrinter.{ show ⇒ pprint }
        pprint( termDef )
    }

    /**
     * Set some attributes.
     *
     * @param a     The first attribute in the form (name, value)
     * @param xa    The optional other attributes (same form)
     *
     * @note        This is only used in testing. The only attribute
     *              allowed in SMTLIB2 is :named and access to it is
     *              provided by via the [[Named]] trait
     */
    def withAttributes(
        a :  ( String, String ),
        xa : ( String, String )*
    ) : Attributed[ A, AttributedTerm ] = {
        //  if attribute named is in the list we should use withNameAndAttributes
        require (
            ( a :: xa.toList ).find( _._1 == "named" ).isEmpty,
            "Use withNameAndAttributes to set the name of a term"
        )

        new Attributed(
            typeDefs,
            AttributedTerm (
                TermPlusAttributes (
                    aTerm,
                    a :: ( xa.toList ) map {
                        case ( kwd, value ) ⇒
                            AttributeKeywordAndValue(
                                Keyword( kwd ),
                                AttValueSymbol( SSymbol( value ) )
                            )
                    }
                )
            ),
            ( a :: xa.toList map
                {
                    e ⇒ ( Keyword( e._1 ), AttValueSymbol ( SSymbol ( e._2 ) ) )
                } ).toMap
        )
    }

    /**
     * Set the name of the term and some (optionally) attributes.
     *
     * @param name  The name of the term
     * @param xa    the optional other attributes (same form)
     *
     * @note        This is only used in testing. The only attribute
     *              allowed in SMTLIB2 is :named and access to it is
     *              provided via the [[Named]] trait
     *
     */
    def withNameAndAttributes(
        name : String,
        xa :   ( String, String )*
    ) : Named[ A, AttributedTerm ] =
        new Named[ A, AttributedTerm ](
            typeDefs,
            AttributedTerm (
                TermPlusAttributes (
                    aTerm,
                    ( "named", name ) :: ( xa.toList ) map {
                        case ( kwd, value ) ⇒
                            AttributeKeywordAndValue(
                                Keyword( kwd ),
                                AttValueSymbol( SSymbol( value ) )
                            )
                    }
                )
            ),

            ( xa.toList map
                {
                    e ⇒ ( Keyword( e._1 ), AttValueSymbol ( SSymbol ( e._2 ) ) )
                } ).toMap +
                ( Keyword( "named" ) → AttValueSymbol ( SSymbol ( name ) ) )
        )

    import au.edu.mq.comp.smtlib.parser.Analysis
    /**
     * Remove indices of the variable in a term
     *
     * @return The term with unindexed variables
     */
    def unIndexed = TypedTerm[ A, B ](
        typeDefs.map( Analysis( _ ).unIndexed ),
        Analysis( termDef ).unIndexed
    )

    /**
     * Add indices to some variables in the term
     *
     * @param   indexMap    The partial function that defines the index for
     *                      some variables (can be a Map).
     *
     * @return              The term with indexed variable (True and False not indexed)
     */
    def indexedBy( indexMap : SMTLIB2Symbol ==> Int ) = TypedTerm[ A, B ](
        typeDefs.map( Analysis( _ ).withIndex( indexMap ) ),
        Analysis( termDef ).withIndex( indexMap )
    )
}
