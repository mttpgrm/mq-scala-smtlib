/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package typedterms

import scala.util.{
    Try,
    Success,
    Failure
}
import parser.PredefinedParsers

/**
 * Provides Standard SMTLIB2 commands to check whether a list of predicates is SAT,
 * extract interpolants (if UNSAT), push and pop.
 *
 * == Usage ==
 * Example usage can be found in the file {@link ./General-usage-tests.scala}
 *
 */
trait Commands extends CoreCommands with PredefinedParsers {

    import theories.BoolTerm
    import parser.SMTLIB2Syntax._
    import interpreters.ExtendedSMTLIB2Interpreter
    import parser.SMTLIB2Parser
    import parser.Implicits._
    import scala.collection.immutable.Stream.StreamBuilder
    import scala.collection.mutable.ListBuffer
    import parser.SMTLIB2PrettyPrinter.format

    import scala.annotation.tailrec
    /**
     * Check wether a sequence of 'typed terms' is satisfiable
     *
     * @param xb        The sequence of terms t1, t2, ... tk. The corresponding
     *                  logical statement is t1 &and; t2 and; ... &and; tk.
     * @param solver    The context should provide an implicit solver.
     *                  You can use the convenience method
     *                  [[[interpreters.Resources.using monadic using]]] to create a solver
     *                  and  use it.
     *
     * @return          Either a Success with the sat status or a
     *                  failure and (reason for failure).
     */
    @tailrec
    final def isSat[ T <: Term ](
        xb : TypedTerm[ BoolTerm, T ]*
    )(
        implicit
        solver : ExtendedSMTLIB2Interpreter
    ) : Try[ SatResponses ] = {

        import com.typesafe.scalalogging.Logger
        import org.slf4j.LoggerFactory

        //  logger
        val logger = Logger( LoggerFactory.getLogger( this.getClass + ".isSat" ) )

        logger.info( "Entering isSat" )

        /*
         * Use if/then and case instead of flatMap (initial version) as
         * otherwise the compiler does not accept the tailrec annotation
         */
        if ( xb.isEmpty ) {
            logger.info( "Calling CheckSat()" )
            checkSat()( solver )
        } else {
            //  assert head and if Success evaluate the tail
            logger.info( "Evaluating list of terms" )
            ( |= ( xb.head ) ) match {
                case Failure( e ) ⇒
                    logger.error( s"|= returned Error: $e -- aborting evaluation" )
                    Failure( e )
                case Success( ok ) ⇒
                    logger.error( s"|= was a success $ok" )
                    isSat( xb.tail : _* )
            }
        }
    }

    /**
     * Pop scopes.
     *
     * @param n  The number of elements to pop.
     *
     */
    def pop(
        n : Int = 1
    )(
        implicit
        solver : ExtendedSMTLIB2Interpreter
    ) : Try[ GeneralResponse ] = {

        eval( Seq( PopCmd( n.toString ) ) ) flatMap parseAsGeneralResponse
    }

    /**
     * Push new scopes.
     *
     * @param  n  The number of scopes to push.
     *
     */
    def push(
        n : Int = 1
    )(
        implicit
        solver : ExtendedSMTLIB2Interpreter
    ) : Try[ GeneralResponse ] = {

        eval( Seq( PushCmd( n.toString ) ) ) flatMap parseAsGeneralResponse
    }

    import interpreters.GenericSolver
    import configurations.SupportsOptions._

    /**
     * When check-sat returned UnSat, provides interpolants if possible
     *
     * @todo: Clear the isInstanceOf and asInstanceOf
     */
    private def getInterpolantsAsTerms[ T <: Term ](
        x1 : Named[ BoolTerm, T ],
        x2 : Named[ BoolTerm, T ],
        xb : Named[ BoolTerm, T ]*
    )(
        implicit
        solver : ExtendedSMTLIB2Interpreter
    ) : Try[ List[ Term ] ] = {

        //  this is ugly but for the time being will do
        require (
            solver.asInstanceOf[ GenericSolver ].s.isInstanceOf[ SupportsInterpolants ],
            "Solver does not support interpolants"
        )
        val x = solver.asInstanceOf[ GenericSolver ].s.asInstanceOf[ SupportsInterpolants ]

        val parseInterpolants = SMTLIB2Parser[ GetInterpolantResponses ]

        //  send command to solver
        eval (
            Seq (
                GetInterpolantCmd(
                    x.getInterpolantCmdString,
                    ( x1 :: x2 :: xb.toList ).map( _.nameDef )
                        .map( x ⇒ QIdTerm( SimpleQId( SymbolId( SSymbol( x ) ) ) ) )
                )
            )
        ) flatMap {
                solverResponse ⇒
                    parseInterpolants ( solverResponse ) match {
                        //  solver returned a response which is not a SatStatus
                        case Success(
                            GetInterpolantResponsesSuccess(
                                InterpolantResponse( xl )
                                )
                            ) ⇒ Success( xl )

                        case Success( GetInterpolantResponsesError( e ) ) ⇒
                            Failure( new Exception( "GetDeclCmd returned: " + e ) )

                        //  Problem occurred in getting interpolants
                        case Failure( e ) ⇒ Failure( e )
                    }
            }

    }

    /**
     * Returns interpolants.
     *
     * The solver must support interpolants. The subject terms must be of type
     * [[parser.SMTLIB2Syntax.NamedTerm] and there must be more than 2 terms.
     *
     * Given `x1` and `x2` such that x1 &and; x2 is UNSAT, an interpolant
     * is a predicate I such that:
     *
     * <ol>
     *      <li>x1 &rArr; I</li>
     *      <li> I &and; x2 is UNSAT</li>
     *      <li>the variables in I are variables in both `x1` and `x2`</li>
     * </ol>
     *
     * When `xb` is not empty the previous definition generalises to inductive
     * interpolants.
     *
     * @param x1        First predicate.
     * @param x2        Second predicate.
     * @param xb        Optional list of predicates.
     *
     * @param  return     A list of 1 + |xb| predicates
     */
    def getInterpolants[ T <: Term ](
        x1 : Named[ BoolTerm, T ],
        x2 : Named[ BoolTerm, T ],
        xb : Named[ BoolTerm, T ]*
    )(
        implicit
        solver : ExtendedSMTLIB2Interpreter
    ) : Try[ List[ TypedTerm[ BoolTerm, Term ] ] ] =
        {
            val parserDeclCmdResponse = SMTLIB2Parser[ GetDeclCmdResponse ]

            //  get interpolants as solver terms
            getInterpolantsAsTerms( x1, x2, xb : _* ) match {

                case Success( xitp ) ⇒

                    //  Success. Retrieve the list of free vars in each term and
                    //  their declarations on the solver stack to make a TypedTerm

                    eval( Seq( GetDeclCmd() ) ) flatMap
                        //
                        { solverResponse ⇒ parserDeclCmdResponse ( solverResponse ) } flatMap
                        //
                        {
                            case GetDeclCmdResponse( solverDeclStack ) ⇒
                                Success ( xitp map {
                                    case i ⇒

                                        import parser.Analysis
                                        //  collect the ids for interpolant i
                                        val b = Analysis( i ).ids

                                        //  Filter the declarations on the stack.
                                        //  Elemenst on the stack are SortedQId
                                        val defs = for {
                                            SortedQId( x, s ) ← solverDeclStack if b.contains( SimpleQId( x ) )
                                        } yield SortedQId( x, s )

                                        TypedTerm[ BoolTerm, Term ]( defs.toSet, i )
                                } )
                        }

                //  Interpolants could not be computed.
                case Failure( e ) ⇒ Failure( e )
            }
        }
}
