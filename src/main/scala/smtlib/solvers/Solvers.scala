/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package solvers

/**
 * Interface of a generic solver process
 */
trait Solver {

    import parser.SMTLIB2Syntax.{
        SMTLIBOption,
        Command,
        SetOption,
        SetLogic,
        PrintSuccess,
        SSymbol
    }

    /** A set of options to configure the solver.*/
    private[ smtlib ] def options = Set[ SMTLIBOption ]()

    /** The name of a logic, e.g. QF_LIA. */
    protected[ smtlib ] val logic : Option[ String ] = None

    /**
     * Provide a sorted configuration script with the optional
     * [[parser.SMTLIB2Syntax.SetLogic]] command at the end.
     *
     * This is what most solvers require. The scriptalso pre-pends
     * the config script with the `(set-option print-success true)`
     * to make sure the solver response is visible on the output channel.
     */
    private[ smtlib ] def configScript : Seq[ Command ] = {

        import scala.collection.mutable.ListBuffer
        val script = new ListBuffer[ Command ]()

        script += SetOption( PrintSuccess( true ) )

        options.foreach { script += SetOption( _ ) }

        //  in most solvers the logic has to be set at the end
        if ( logic.nonEmpty )
            script += SetLogic( SSymbol( logic.get ) )

        script.toSeq
    }

    /** The name of the program to be run, e.g. "/bin/bash" */
    private[ smtlib ] val executable : String

    /** The list of arguments of the program, e.g. "-c", "ls -al" */
    private[ smtlib ] val args : Array[ String ]

    /**
     * The list of logics supported by the solver
     *
     * It is automatically computed when declaring that the solver
     * supports a logic
     */
    private[ smtlib ] def supportedLogics = Set[ String ]()

    /**
     * The list of options supported by the solver
     *
     * It is automatically computed when declaring that the solver
     * supports an option
     */
    private[ smtlib ] def supportedOptions = Set[ String ]()

    import scala.concurrent.duration._

    /**
     * Timeout for each command we send.
     *
     * This corresponds to the maximum time before we throw an TimeoutException
     * in th eval command. This happends if we send a command and after the
     * timeout delay the prompt could not be read from the outout stream of
     * the solver.
     */
    private[ smtlib ] val timeoutPerEval : Duration = 10.seconds

    /**
     * The sentinnelle string (tag) to send to the solver after a regular `cmd`
     * command to detect that `cmd` has completed.
     */
    private[ smtlib ] val cmdSentinelleString : String = "<EOC>"

    import java.util.regex.Pattern
    import scala.util.matching.Regex

    /**
     * A regular expression that describes the expected result of
     * sending (echo cmdSentinelleString) to the solver.
     *
     */
    private[ smtlib ] val expectedPromptAfterCommand : Regex

}
