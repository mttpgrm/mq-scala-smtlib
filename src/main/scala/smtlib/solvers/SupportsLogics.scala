/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package solvers

/**
 * Indicate support for logics L
 *
 * @see [[theories.PredefinedLogics]] for the defintions of the logics.
 */
private[ smtlib ] object SupportsLogics {

    trait Supports_QF_BV extends Solver {
        override def supportedLogics = super.supportedLogics + "QF_BV"
    }

    trait Supports_QF_ABV extends Solver {
        override def supportedLogics = super.supportedLogics + "QF_ABV"
    }

    trait Supports_QF_UF extends Solver {
        override def supportedLogics = super.supportedLogics + "QF_UF"
    }

    trait Supports_QF_LIA extends Solver {
        override def supportedLogics = super.supportedLogics + "QF_LIA"
    }

    trait Supports_QF_NIA extends Solver {
        override def supportedLogics = super.supportedLogics + "QF_NIA"
    }

    trait Supports_QF_LRA extends Solver {
        override def supportedLogics = super.supportedLogics + "QF_LRA"
    }

    trait Supports_QF_NRA extends Solver {
        override def supportedLogics = super.supportedLogics + "QF_NRA"
    }

    trait Supports_QF_AUFLIA extends Solver {
        override def supportedLogics = super.supportedLogics + "QF_AUFLIA"
    }

    trait Supports_QF_LIRA extends Solver {
        override def supportedLogics = super.supportedLogics + "QF_LIRA"
    }

    trait Supports_QF_UFLIRA extends Solver {
        override def supportedLogics = super.supportedLogics + "QF_UFLIRA"
    }

    trait Supports_AUFNIRA extends Solver {
        override def supportedLogics = super.supportedLogics + "AUFNIRA"
    }

}
