/*
 * This file is part of MQ-Scala-SMTLIB.
 *
 * Copyright (C) 2015-2017 Franck Cassez.
 *
 * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.smtlib
package solvers

import configurations.SupportsOptions._
import SupportsLogics._

/**
 * MathSAT solver executable and command line arguments
 */
class MathSAT
        extends Supports_QF_LIA
        with Supports_QF_LRA
        with Supports_QF_NRA
        with Supports_QF_AUFLIA
        with Supports_AUFNIRA
        with Supports_QF_UF
        with Supports_QF_BV
        with Supports_QF_ABV
        with SupportsModels
        // with SupportsInterpolants    //  supports only a simple form, not inductive
        with Solver {

    val executable = "mathsat"

    val args = Array[ String ]()

    // val getInterpolantCmdString = "get-interpolant"

    import scala.util.matching.Regex

    /** (echo "x") returns "x" followed by success in MathSAT */
    val expectedPromptAfterCommand : Regex = (
        "(\\s)*\"" +
        cmdSentinelleString +
        "\"(\\s)*(success)"
    ).r

    override def toString = "MathSAT"
}
