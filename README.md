[![License: LGPL v3](https://img.shields.io/badge/License-LGPL%20v3-blue.svg)](http://www.gnu.org/licenses/lgpl-3.0)
[![Run Status](https://api.shippable.com/projects/57497cfa2a8192902e21b58c/badge?branch=master)](https://app.shippable.com/projects/57497cfa2a8192902e21b58c)
[![Coverage Badge](https://api.shippable.com/projects/57497cfa2a8192902e21b58c/coverageBadge?branch=master)](https://app.shippable.com/projects/57497cfa2a8192902e21b58c)

MQ-Scala-SMTLIB
===============

MQ-Scala-SMTLIB provides an abstraction over [SMTLIB](http://smtlib.cs.uiowa.edu) standard for Satisfiability Modulo Theory (SMT) solvers.

It was orginally forked off [Scala SMTLIB](https://github.com/regb/scala-smtlib) in September 2015, but has since evolved to a brand new and redesigned library.

MQ-Scala-SMTLIB
---------------

The main features of MQ-Scala-SMTLIB:

- support for SMTLIB2 compliant solvers; currently Z3, CVC4, SMTInterpol, Yices and MathSAT have been tested.
- safe resource management using the [Scala-ARM](https://github.com/jsuereth/scala-arm) library. Resources (solvers and pipes) that are spawned by the library are properly released after usage.
- DSL to write predicates, terms in many logics. This provides some type safety e.g. prevents the user from writing an addition between two boolean terms.
- ability to extract results produced by the solvers as DSL terms (values, interpolants).
- the parsing/pretty printing part of MQ-Scala-SMTLIB is developped using the [sbt-rats](https://bitbucket.org/inkytonik/sbt-rats) plugin for SBT. This provides a compact and maintainable grammar for SMTLIB syntax and considerably reduces the source code.
- interaction with the solvers is provided by an Expect-like package (see [Expect]() ).
- support for 'get-interpolants' commands.
- support for parsing SMTLB2 compliant files.


Applications
------------

We use our MQ-Scala-SMTLIB abstract SMT-solver in our static analysis tool [Skink](http://science.mq.edu.au/~fcassez/software-verif.html).


Install
------

#### Scala Build Tool

You need a recent version of [SBT](http://www.scala-sbt.org/). To build the library, simply type

~~~
sbt package
~~~

#### SBT-rats and Kiama

[sbt-rats](https://bitbucket.org/inkytonik/sbt-rats) sbt-rats provides a plugin that enables the Rats! parser generator to be used in Scala projects.
The MQ-Scala-SMTLIB `build.sbt` should  fetch the correct version of `sbt-rats` the first time you build the library. We use the plugin `sbt-rats` to generate a parser
from a the `.syntax` file in parser package.

[Kiama](https://bitbucket.org/inkytonik/kiama) is a Scala library for language processing.
should be fetched the first time you build the library.

#### Building MQ-Scala-SMTLIB

~~~
sbt compile
~~~

#### SMT solvers

MQ-Scala-SMTLIB currently supports:

- [Z3 4.4.2](https://github.com/Z3Prover/z3)
- [SMTInterpol 2.1-219-g60cc1b6](https://ultimate.informatik.uni-freiburg.de/smtinterpol/)
- [CVC4 1.4](http://cvc4.cs.nyu.edu/downloads/) (note: homebrew version crashes)
- [MathSAT 5.3.11](http://mathsat.fbk.eu)
- [Yices 2.5.1](http://yices.csl.sri.com)

Refer to the respective solver url above for installation instructions.
The solvers executable must in your path for the library to find and run
solvers.
[SMTInterpol](https://ultimate.informatik.uni-freiburg.de/smtinterpol/) is distributed as a Java .jar file and we use shell wrapper to run it:
~~~~
#!/bin/bash

# wrapper for calling the JAR file for inter proc analysis
java -jar ~/development/SMTinterpol/bin/smtinterpol.jar "$@"

exit 0
~~~~
This executable shell script should be in the file `interpol` and `interpol` should be in your path.

#### Testing

To test that the solvers are correctly found you can run the following test:
~~~
sbt testOnly *CreateSolver*
~~~

If this is successful, you can run the tests that configure the solvers with basic configurations they should support:
~~~
sbt testOnly *SolverConfigTests*
~~~

If successful you may run the full test suite:
~~~
sbt test
~~~

All tests should pass (and a few may be ignored).
If some tests fail please open an issue.

Getting Started
---------------

#### Basic usage
The following example illustrates the basic usage but **may not compile nor run in the scala REPL**.

The basic usage is to get a solver via the `using` method:

~~~

import au.edu.mq.comp.smtlib.solvers._
import au.edu.mq.comp.smtlib.interpreters.Resources
import au.edu.mq.comp.smtlib.theories.PredefinedLogics.QF_LIA

using( new Z3 with QF_LIA  ) {
    implicit solver ⇒

    //  ActionBlock: what you want to do with the solver

}
~~~
The `implicit solver =>` line is fixed nd should be used verbatim. The `SMTSolver` line can be changed and customised to your needs: for example `new CVC4 with QF_AUFLIA` wil use CVC4 with the theories of arrays and Linear Integer Linear Arithmetic.

The following example illustrates the basic usage but **may not compile nor run in the scala REPL**.

Within the `ActionBlock` you can define predictes and assert them on the solver stack, or check if the current stack of predicates is satisfiable:

~~~
import au.edu.mq.comp.smtlib.parser.Implicits._
import au.edu.mq.comp.smtlib.interpreters.Resources
import au.edu.mq.comp.smtlib.theories.{ Core, IntegerArithmetics}
import au.edu.mq.comp.smtlib.typedterms.Commands

class ExampleLIA extends Core with IntegerArithmetics with Resources with Commands {

    import scala.util.{ Try }
    //  DSL package to build TypedTerm
    import au.edu.mq.comp.smtlib.typedterms.TypedTerm
    import au.edu.mq.comp.smtlib.theories.PredefinedLogics.QF_LIA
    import au.edu.mq.comp.smtlib.typedterms.Commands
    import au.edu.mq.comp.smtlib.solvers._
    import au.edu.mq.comp.smtlib.parser.SMTLIB2Syntax.SatResponses

    //  create two SMTLIB2 Int variables using the DSL
    val x = Ints( "x" )
    val y = Ints( "y" )

    using( new Z3 with QF_LIA ) {
        implicit solver ⇒

        //  Check simple sat query
        val r : Try[SatResponses] =
            isSat(
                    x + 1 <= y,
                    y % 2 === 3
                )
        r
    }
}
~~~

### Examples

The file [General-usage-tests.scala](src/test/scala/smtlib/General-usage-tests.scala) provides a few examples how to use the library.

For more examples see the scaladoc.

How to add features?
--------------------
Start [here](./wiki/contribute.md)

Changelog
---------

See [Changelog](/CHANGELOG.md).
