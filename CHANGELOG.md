# Changelog

## Macquarie University Scala SMTLIB2 project

## v1.0

- Support for the SMTLIB standard version 2.5
- SMTLIB2 expressions represented with a Scala AST
- Parser turn input stream into AST representation
- Printer turn AST representation into an SMTLIB2 compliant string output
- abstraction layer to communicate with solver processes over their textual input
- Support for Z3, CVC4, SMTInterpol, MathSAT
- get-interpolants (Z3 and SMTInterpol tested)
