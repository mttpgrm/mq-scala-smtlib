

How to add features?
====================


Add a logic and test if solver supports it
------------------------------------------

To add a logic and test if a solver supports it follow the following steps:

1. Add the logic in [Logics.scala](./src/main/scala/smtlib/theories/Logics.scala). The list of logics solvers may support is available [here](http://smtlib.cs.uiowa.edu/logics.shtml). If you want to add the logic named "QF_AUFBV" you can add a trait in the the PredefinedLogics object:
  
---
    trait QF_AUFBV {
        this : Supports_QF_AUFBV ⇒
        abstract override val logic = Some( "QF_AUFBV" )
    }
---
    
    The name of the trait is arbitrary but using the common name is recommended.
    
2. Add a _Support_ trait in the SupportsLogics object in [SupportsLogics.scala](./src/main/scala/smtlib/solvers/Logics.scala):
  
---
    trait Supports_QF_AUFBV extends Solver {
        override def supportedLogics = super.supportedLogics + "QF_AUFBV"
    }
---

3. Add the support trait to a solver to claim it supports the logics. For [Z3](./src/main/scala/smtlib/solvers/Z3Solver.scala)


---
   class Z3 extends Supports_QF_LIA
           with Supports_QF_LRA
           with Supports_QF_NRA
           with Supports_QF_AUFLIA
           with Supports_AUFNIRA
           with Supports_QF_UF
           with Supports_QF_BV
           with Supports_QF_ABV     //  add after other logics
           ...
---
   
4. Compile (should be successful)
5. Test: this will send the set-logic command to the solvers and try all the logics they claim to support.
 
---
    testOnly *SolverSupportedLogicsTests*
---
    
