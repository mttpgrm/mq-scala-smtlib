name := "MQ Scala SMTLIB"

organization := "au.edu.mq.comp"

version := "1.0-SNAPSHOT"

// Scala compiler settings

scalaVersion in ThisBuild := "2.12.1"
crossScalaVersions in ThisBuild := Seq ("2.12.1")

scalacOptions :=
    Seq (
        "-deprecation",
        "-feature",
        "-sourcepath", baseDirectory.value.getAbsolutePath,
        "-unchecked",
        "-Xlint",
        "-Xcheckinit"
    )

javaOptions in IntegrationTest ++= Seq("-Xss200M")

// test options for generating formatted test results (and use with CI)
testOptions in Test <+= (target in Test) map {
  t => Tests.Argument(TestFrameworks.ScalaTest, "-u", "%s" format (t / "../shippable/testresults"))
}

fork in IntegrationTest := true

libraryDependencies ++=
    Seq (
        "org.bitbucket.inkytonik.kiama" %% "kiama" % "2.1.0-SNAPSHOT",
        "org.bitbucket.inkytonik.kiama" %% "kiama" % "2.1.0-SNAPSHOT" % "test" classifier ("tests"),
        "org.scalatest" %% "scalatest" % "3.0.0" % "test",
        "org.scalacheck" %% "scalacheck" % "1.13.4" % "test",
        "org.bitbucket.franck44.expect" %% "expect-for-scala" % "1.0-SNAPSHOT",
        "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
        "com.jsuereth" % "scala-arm_2.12" % "2.0",
        "ch.qos.logback" % "logback-classic" % "1.1.7"
    )


//  scoverage
coverageEnabled := true
coverageMinimum := 80
coverageFailOnMinimum := false  //  false is safer as otherwise the build breaks
coverageHighlighting := true    //  enable highlighting of covered/non-covered
//  exclude some package from coverage
coverageExcludedPackages := ".*sbtrats.*"

logBuffered in IntegrationTest := false

parallelExecution in Test := true

logLevel := Level.Info

shellPrompt in ThisBuild := {
    state =>
        Project.extract(state).currentRef.project + " [" + name.value + "] " + version.value +
            " " + scalaVersion.value + "> "
}

scalacOptions in (Compile,doc) ++= Seq(
    "-groups",
    "-implicits",
    "-diagrams",
    "-diagrams-dot-restart", "80",
    "-diagrams-dot-path", "/usr/local/bin/dot"
    // ,"-diagrams-debug"
)

// Dependencies

// Rats! setup

sbtRatsSettings

ratsScalaRepetitionType := Some (ListType)

ratsUseScalaOptions := true

ratsUseScalaPositions := true

ratsDefineASTClasses := true

ratsDefinePrettyPrinter := true

// ratsUseDefaultLayout := true

ratsUseDefaultComments := false

// ratsVerboseOutput := true

ratsUseKiama := 2


// ScalariForm

import scalariform.formatter.preferences._
import com.typesafe.sbt.SbtScalariform
import com.typesafe.sbt.SbtScalariform.ScalariformKeys

scalariformSettings
//scalariformSettingsWithIt
//
ScalariformKeys.preferences := ScalariformKeys.preferences.value
    .setPreference (AlignSingleLineCaseStatements, true)
    .setPreference (IndentSpaces, 4)
    .setPreference (SpaceBeforeColon, true)
    .setPreference (SpaceInsideBrackets, true)
    .setPreference (SpaceInsideParentheses, true)
    .setPreference (SpacesWithinPatternBinders, true)
    .setPreference(SpacesAroundMultiImports, true)
    .setPreference (SpacesAroundMultiImports, true)
    .setPreference (PreserveSpaceBeforeArguments, true)
    .setPreference (SpacesWithinPatternBinders, true)
    .setPreference (RewriteArrowSymbols, true)
   .setPreference (AlignParameters, true)
   .setPreference(AlignArguments, true)
   // .setPreference(doubleIndentMethodDeclaration, true)


// headers
import de.heikoseeberger.sbtheader.HeaderPattern
import de.heikoseeberger.sbtheader.license.LGPLv3
import de.heikoseeberger.sbtheader.license.Apache2_0

headers := Map(
    // "scala" -> LGPLv3("2016", "Franck Cassez"),
    "syntax" -> LGPLv3("2016", "Franck Cassez"), // this one ie not working
    "scala"  -> (
    HeaderPattern.cStyleBlockComment,
    """|/*
     | * This file is part of MQ-Scala-SMTLIB.
     | *
     | * Copyright (C) 2015-2017 Franck Cassez.
     | *
     | * MQ-Scala-SMTLIB  is free software: you can redistribute it and/or modify
     | * it under the terms of the GNU Lesser General Public License as published
     | * by the Free Software Foundation, either version 3 of the License, or (at
     | * your option) any later version.
     | *
     | * MQ-Scala-SMTLIB is  distributed in the hope that it will be  useful, but
     | * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
     | * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
     | *
     | * See the GNU Lesser General Public License for  more details.
     | *
     | * You should have received a copy of the GNU Lesser General Public License
     | * along with MQ-Scala-SMTLIB.  (See files COPYING and COPYING.LESSER.)  If
     | * not, see  <http://www.gnu.org/licenses/>.
     | */
     |
     |""".stripMargin
     )
)

excludes := Seq("src/generated/**")
